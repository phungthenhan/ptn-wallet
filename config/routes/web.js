/* jscs : disable */

module.exports = {

  GET: {
    '/'                                 : ['DebugController.demoPage'],
    '/demo'                             : ['DebugController.demoPage'],
  }
};
