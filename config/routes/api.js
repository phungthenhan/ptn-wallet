/* jscs : disable */

module.exports = {

  GET: {
    // UserAPIs
    '/user/me'                                                  : ['UserController.me', ['authenticate']],
    '/user/session'                                             : ['UserController.getCurrentSession', ['authenticate']],

    // Wallet APIs
    '/:coin/wallet'                                             : ['WalletController.getListWallets', ['authenticate', 'paginate']],
    '/:coin/wallet/:walletId'                                   : ['WalletController.getWalletDetails', ['authenticate']],
    '/:coin/wallet/address/:address'                            : ['WalletController.getWalletByAddress'],
    '/:coin/wallet/:walletId/transfer'                          : ['WalletController.listWalletTransactions', ['paginate']],
    '/:coin/wallet/:walletId/transfer/:txid'                    : ['WalletController.getOneTransaction'],
    '/:coin/wallet/:walletId/transfer/sequenceId/:sequenceId'   : ['WalletController.getOneWalletTransferBySequenceId'],
    '/:coin/wallet/:walletId/address/:address'                  : ['WalletController.getOneWalletAddress'],
    '/:coin/wallet/:walletId/addresses'                         : ['WalletController.getWalletAddresses', ['paginate']],
    '/:coin/wallet/:walletId/tx'                                : ['WalletController.listWalletTransactions', ['paginate']],
    '/:coin/wallet/:walletId/tx/:txid'                          : ['WalletController.getOneTransaction'],
    '/:coin/wallet/:walletId/unspents'                          : ['WalletController.getWalletUnspents'],
    '/:coin/wallet/:walletId/maximumSpendable'                  : ['WalletController.getMaximumSpendable'],
    '/:coin/wallet/:walletId/webhooks'                          : ['WebhookController.getListWalletWebhooks'],
    '/:coin/webhooks'                                           : ['WebhookController.getListUserWebhooks', ['authenticate']],
    '/:coin/wallet/:walletId/withdrawals'                       : ['WalletController.listCoinWithdrawals', ['authenticate','paginate']],
    '/:coin/wallet/:walletId/deposits'                          : ['WalletController.listCoinDeposits', ['authenticate','paginate']],

    // Others
    '/:coin/wallet/:address/tx'                                 : ['WalletController.getAddressTransactions'],
    '/:coin/tx/:txid'                                           : ['WalletController.getOneTransaction'],
    '/:coin/balance/:address'                                   : ['WalletController.getAddressBalance'],
    '/:coin/tx_trace/:txid'                                     : ['WalletController.getTransactionTrace'],
  },

  POST: {
    // User APIs
    '/user/session'                                             : ['UserController.createSession', ['authenticate']],
    '/user/lock'                                                : ['UserController.lock', ['authenticate']],
    '/user/unlock'                                              : ['UserController.unlock', ['authenticate']],
    '/user/login'                                               : ['AuthController.login'],
    '/user/logout'                                              : ['AuthController.logout', ['authenticate']],
    '/password'                                                 : ['UserController.changePassword', ['authenticate']],

    // Wallet APIs
    '/:coin/wallet/generate'                                    : ['WalletController.createWallet', ['authenticate']],
    '/:coin/wallet/:walletId/address'                           : ['WalletController.createWalletAddress', ['authenticate']],
    '/:coin/wallet/:walletId/sendcoins'                         : ['WalletController.sendCoins', ['authenticate']],
    '/:coin/wallet/:walletId/sendmany'                          : ['WalletController.sendCoinsToMany', ['authenticate']],
    '/:coin/wallet/:walletId/consolidateunspents'               : ['WalletController.consolidateWalletUnspents', ['authenticate']],
    '/:coin/wallet/:walletId/fanoutunspents'                    : ['WalletController.fanoutWalletUnspents', ['authenticate']],
    '/:coin/wallet/:walletId/tx/build'                          : ['WalletController.buildTransaction', ['authenticate']],
    '/:coin/wallet/:walletId/signtx'                            : ['WalletController.signTransaction', ['authenticate']],
    '/:coin/wallet/:walletId/tx/send'                           : ['WalletController.sendTransaction', ['authenticate']],

    // Webhook APIs
    '/:coin/webhooks'                                           : ['WebhookController.createUserWebhook', ['authenticate']],
    '/:coin/wallet/:walletId/webhooks'                          : ['WebhookController.createWalletWebhook', ['authenticate']],
    '/:coin/webhooks/:webhookId/simulate'                       : ['WebhookController.simulateWebhook', ['authenticate']],
  },

  DELETE: {
    '/:coin/wallet/:walletId/webhooks'                          : ['WebhookController.removeWalletWebhook', ['authenticate']],
    '/:coin/webhooks'                                           : ['WebhookController.removeUserWebhook', ['authenticate']],
  }

};
