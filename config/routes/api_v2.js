// APIs for mobile/web wallet
module.exports = {

  GET: {
    '/me'                                       : ['UserController.me', ['authenticate']],
    '/me/:coin/addresses/default'               : ['WalletController2.getOrCreateDefaultAddress', ['authenticate']],
    '/public/:coin/balance/:address'            : ['WalletController2.getAddressBalance'],
  },

  POST: {
    '/:coin/addresses/generate'                 : ['WalletController2.generateNewDefaultAddress', ['authenticate']],
    '/:coin/send'                               : ['WalletController2.sendCoins', ['authenticate']],
  },

  DELETE: {
    // Implement me.
  }

};
