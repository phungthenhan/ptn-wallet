const Const = require('../app/common/Const');

module.exports = {
  'mysql-master': {
    type: Const.DATA_SOURCE_TYPE.MYSQL,
    connectionLimit: 100,
    waitForConnections: true,
    queueLimit: 500,
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  'mysql-slave': {
    type: Const.DATA_SOURCE_TYPE.MYSQL,
    connectionLimit: 500,
    waitForConnections: true,
    queueLimit: 5000,
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  'mysql-master-test': {
    type: Const.DATA_SOURCE_TYPE.MYSQL,
    connectionLimit: 2,
    waitForConnections: true,
    queueLimit: 50
  },
  'mysql-slave-test': {
    type: Const.DATA_SOURCE_TYPE.MYSQL,
    connectionLimit: 50,
    waitForConnections: false
  },
  'mongo': {
    type: Const.DATA_SOURCE_TYPE.MONGODB
  }
}
