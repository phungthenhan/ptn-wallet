require('dotenv').config();

const path          = require('path');
const util          = require('util');
const _             = require('lodash');
const async         = require('async');
const assert        = require('assert');
const request       = require('superagent');
const requireDir    = require('require-directory');

require('./blockchain-gateway');
require('./end/Example.spec.js');
