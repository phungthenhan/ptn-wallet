require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

const blockNumber = 273482;
const blockHash = '00000000000029be919a08b606d2161c68924546745ab48908df00534ee38115';
const blockHex = '02000000d8ed95305ca8a017fcd28698606e7eb5c0d82d372370df78569face3000000000166c938406ecf16958da20fbc8c63b249ab6d7690c5a79aede370cef3ebceb02de3e5533698001b9af97c4a0201000000010000000000000000000000000000000000000000000000000000000000000000ffffffff3f034a2c040653e5e32d1001fabe6d6dddcaa197fcfe728f047c29963c0cdc466a9083ac2f7150aca96d8fbd4f981f24010000000000000002000000da7a0000ffffffff0100f90295000000001976a9140ca02e117a069e3bdd803fef356adf2d3fedc0a188ac000000000100000001286de553484404c85a9e08ea918e479471d13e2ce27540b45a4d1adbf93ea190000000006b483045022100ea0a578d84ebf4afb1664df689ce60063c1c616e4fbc9dd9fd0a0a71f9292318022032a8d8c65ebf14dc7533eaab4b75476f1e0abfaaad4b4de443f50ed6c66637f101210303d49b8618972fa582c9fcc410b6a10969bf4676c9f0d93ec6bc9fbc63775a1affffffff0280ba8c01000000001976a91402e0a25f06d288d8b75ecd801e7121a8cbdf1a6e88acd8f30a01000000001976a914e49dbfa28c05cca30a3d8598f611581a5a069e7188ac00000000';

describe('BtcGateway::getRawBlock', () => {

  it('1.1 Get invalid block number: height < 0', (done) => {
    BtcGateway.getRawBlock(-1, (err, block) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block number: height > max', (done) => {
    BtcGateway.getRawBlock(1000000000, (err, block) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.3 Get invalid block hash', (done) => {
    BtcGateway.getRawBlock('abc', (err, block) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number - without verbosity', (done) => {
    BtcGateway.getRawBlock(blockNumber, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('2.2 Get get valid block number - with verbosity=0', (done) => {
    BtcGateway.getRawBlock(blockNumber, 0, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('2.3 Get get valid block number - with verbosity=1', (done) => {
    BtcGateway.getRawBlock(blockNumber, 1, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

  it('3.1 Get get valid block hash - without verbosity', (done) => {
    BtcGateway.getRawBlock(blockHash, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('3.2 Get get valid block hash - with verbosity=0', (done) => {
    BtcGateway.getRawBlock(blockHash, 0, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('3.3 Get get valid block hash - with verbosity=1', (done) => {
    BtcGateway.getRawBlock(blockHash, 1, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

});
