require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

const blockNumber = 273482;
const blockHash = '00000000000029be919a08b606d2161c68924546745ab48908df00534ee38115';

describe('BtcGateway::getBlockTransactions', () => {

  it('1.1 Get invalid block number', (done) => {
    BtcGateway.getBlockTransactions(-1, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block hash', (done) => {
    BtcGateway.getBlockTransactions('abc', (err) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number', (done) => {
    BtcGateway.getBlockTransactions(blockNumber, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

  it('3.1 Get get valid block hash', (done) => {
    BtcGateway.getBlockTransactions(blockHash, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

});
