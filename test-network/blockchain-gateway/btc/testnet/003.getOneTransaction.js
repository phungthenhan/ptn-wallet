require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

describe('BtcGateway::getOneTransaction', () => {

  it('1.1 Get invalid txid', (done) => {
    BtcGateway.getOneTransaction('abc', (err, tx) => {
      // Error: txid has wrong length/format
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get non-existed txid', (done) => {
    BtcGateway.getOneTransaction('1111111111111111111111111111111111111111111111111111111111111111', (err, tx) => {
      // Error: No such mempool or blockchain transaction. Use gettransaction for wallet transactions.
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.3 Get valid txid', (done) => {
    const txid = 'cd2b8fa03c6d48b17de1313d71d6d8b07fa530ef954aaeaf4d5dcfe3278a052c';
    const rawTx = '010000000147700fea093978bb28f618d92732fb7bff22931073a1d84d55287bf8797f02ab000000006a473044022006e9d8ef5bea4ad585c8824f2552276f4043369555cbf187a26eb8d5bf6e2edf0220340d0950611fc135d40082c312188d11e98328e32ba09c62ade2df9d3760cfb1012103228f3da992c4bcd225c535f5bd0879c79d79550e67d8476849b1730764f4f241ffffffff022d390800000000001976a914d56898c8e9aca9c1017c438b1e5f7495b6c636f588ac0000000000000000536a4c50000105790001e26c0cddcfc9ddf93b149045ea62b18ad5870a484d5fb1df6153239e15c246557a9fb4167dc4f182b9859018cd455b680c6f057d08ead7550dc07d1bc4bbc7483d87103a1360530cf3a100000000';
    BtcGateway.getOneTransaction(txid, (err, tx) => {
      if (err) throw err;

      assert.equal(tx.hex, rawTx);
      assert.equal(tx.txid, txid);
      done();
    });
  });

});
