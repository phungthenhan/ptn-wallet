require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

describe('BtcGateway::getBlockCount', () => {

  it('Current block count should be greater than zero.', (done) => {
    BtcGateway.getBlockCount((err, height) => {
      if (err) throw err;

      assert(height > 0);
      done();
    });
  });

});
