require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

const options1 = {
  fromBlockNumber: 273482,
  toBlockNumber: 273482,
};

const options2 = {
  fromBlockNumber: 273482,
  toBlockNumber: 273486,
};

describe('BtcGateway::getBatchTransactions', () => {

  it('1.1 Invalid options: no block range in options', (done) => {
    BtcGateway.getBatchTransactions({}, (err) => {
      // Error: You must specify the block range
      assert(err);
      done();
    });
  });

  it('1.2 Invalid options: from > to', (done) => {
    BtcGateway.getBatchTransactions({ fromBlockNumber: 1000, toBlockNumber: 999 }, (err) => {
      // Error: Start block must be less or equal end block
      assert(err);
      done();
    });
  });

  it('1.3 Invalid options: invalid fromBlockNumber', (done) => {
    BtcGateway.getBatchTransactions({ fromBlockNumber: -1, toBlockNumber: 999 }, (err) => {
      // Error: Start and end block number must be greater than zero
      assert(err);
      done();
    });
  });

  it('1.4 Invalid options: invalid toBlockNumber', (done) => {
    BtcGateway.getBatchTransactions({ fromBlockNumber: 10000000, toBlockNumber: 10000010 }, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Valid options: get transactions of one block', (done) => {
    BtcGateway.getBatchTransactions(options1, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

  it('2.2 Valid options: get transactions of multiple blocks', (done) => {
    BtcGateway.getBatchTransactions(options2, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 34);
      done();
    });
  });

});
