require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

describe('BtcGateway::getTransactionsByIds', () => {

  it('1.1 Invalid txids', (done) => {
    BtcGateway.getTransactionsByIds(['abc', 'abc123', 'xyz'], (err, tx) => {
      // Error: txid has wrong length/format
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Non-existed txids', (done) => {
    const txids = [
      '1111111111111111111111111111111111111111111111111111111111111111',
      'd364f9078b162257d088dac56a8f24d428dc5cf9b934423954ee8b20fd5844c6',
      '1aacace487c3be81521be84f613ced93880171e6ca17868d68557c62c369d1c0',
      'ffcde456b8ac7066e9484b738f43742b4db7f49b6ff54a68483f64835ed7c674',
      '3a060f0499a14018d82025c3e9d579f662ae83fc074824a39403be4c6e0c7c11',
    ];
    BtcGateway.getTransactionsByIds(txids, (err, tx) => {
      // Error: No such mempool or blockchain transaction. Use gettransaction for wallet transactions.
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Valid txids', (done) => {
    const txids = [
      'f3e529eb2b3b71c30c8723e1c3c6d8a2676b46d410a8b76c2e8a145218d14a34',
      '7ce9929afca943a8ef761fff6aeac4c741557d6ad2a810460e6b5bed040c7546',
    ];
    BtcGateway.getTransactionsByIds(txids, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      assert.equal(_.difference(_.map(txs, 'txid'), txids).length, 0);
      done();
    });
  });

  it('2.1 Valid but duplicated txids', (done) => {
    const txids = [
      'f3e529eb2b3b71c30c8723e1c3c6d8a2676b46d410a8b76c2e8a145218d14a34',
      '7ce9929afca943a8ef761fff6aeac4c741557d6ad2a810460e6b5bed040c7546',
      '7ce9929afca943a8ef761fff6aeac4c741557d6ad2a810460e6b5bed040c7546',
      '7ce9929afca943a8ef761fff6aeac4c741557d6ad2a810460e6b5bed040c7546',
      '7ce9929afca943a8ef761fff6aeac4c741557d6ad2a810460e6b5bed040c7546',
    ];
    BtcGateway.getTransactionsByIds(txids, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      assert.equal(_.difference(_.map(txs, 'txid'), txids).length, 0);
      done();
    });
  });

});
