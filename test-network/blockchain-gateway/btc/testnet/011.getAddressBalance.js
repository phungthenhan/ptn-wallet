require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

const address = 'mn9QhsFiX2eEXtF6zrGn5N49iS8BHXFjBt';

describe('BtcGateway::getAddressBalance', () => {

  it('1.1 Invalid address', (done) => {
    BtcGateway.getAddressBalance('abc', (err) => {
      // Error: Bad request
      assert.equal(err.status, 400);
      done();
    });
  });

  it('1.2 Valid address', (done) => {
    BtcGateway.getAddressBalance(address, (err, balance) => {
      if (err) throw err;

      assert(balance >= 0);
      done();
    });
  });

});
