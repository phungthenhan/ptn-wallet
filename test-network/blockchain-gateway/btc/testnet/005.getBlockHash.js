require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

describe('BtcGateway::getBlockHash', () => {

  it('1.1 Get invalid block number', (done) => {
    BtcGateway.getBlockHash(-1, (err, hash) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Get genesis block', (done) => {
    BtcGateway.getBlockHash(0, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, '000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943');
      done();
    });
  });

  it('2.2 Get a valid block', (done) => {
    BtcGateway.getBlockHash(1381145, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, '0000000000000018d18d4426050261a4445ad579ad777bb09c9f2112009e6ef2');
      done();
    });
  });

});
