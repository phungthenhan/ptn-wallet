require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

describe('BtcGateway::getRPCNodeInfo', () => {

  it('Network config in RPC node should be same as in environment variable', (done) => {
    BtcGateway.getRPCNodeInfo((err, info) => {
      if (err) throw err;
      assert.equal(info.testnet, true);
      done();
    });
  });

});
