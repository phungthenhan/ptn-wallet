require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestBtcGateway');
const BtcGateway = require('../../../../app/blockchain').BtcGateway;

const address = 'mn9QhsFiX2eEXtF6zrGn5N49iS8BHXFjBt';

describe('BtcGateway::getAddressUtxos', () => {

  it('1.1 Invalid address', (done) => {
    BtcGateway.getAddressUtxos('abc', (err) => {
      // Error: Bad request
      assert.equal(err.status, 400);
      done();
    });
  });

  it('1.2 Valid address', (done) => {
    BtcGateway.getAddressUtxos(address, (err, utxos) => {
      if (err) throw err;

      if (utxos.length > 0) {
        assert(utxos[0].address === address);
        assert(utxos.length >= 0);
      }
      done();
    });
  });

  it('2.1 Sum of utxos should be equal to balance', (done) => {
    async.auto({
      balance: (next) => {
        BtcGateway.getAddressBalance(address, next);
      },
      utxos: (next) => {
        BtcGateway.getAddressUtxos(address, next);
      }
    }, (err, ret) => {
      if (err) throw err;

      const sumUtxo = _.reduce(ret.utxos, (sum, utxo) => sum + utxo.amountSat, 0);
      assert.equal(ret.balance, sumUtxo);
      done();
    });
  });

});
