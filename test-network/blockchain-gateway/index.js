const requireDir = require('require-directory');

const ETH_NETWORK = process.env.ETH_NETWORK;
const BTC_NETWORK = process.env.BTC_NETWORK;
const DASH_NETWORK = process.env.DASH_NETWORK;
const QTUM_NETWORK = process.env.QTUM_NETWORK;

// require('./dash/testnet/012.getAddressUtxos');

requireDir(module, `./dash/${DASH_NETWORK}`, { recurse: true });
requireDir(module, `./qtum/${QTUM_NETWORK}`, { recurse: true });
requireDir(module, `./btc/${BTC_NETWORK}`, { recurse: true });
