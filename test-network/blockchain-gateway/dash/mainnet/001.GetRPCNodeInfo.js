const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

describe('DashGateway::getRPCNodeInfo', () => {

  it('Network config in RPC node should be same as in environment variable', (done) => {
    DashGateway.getRPCNodeInfo((err, info) => {
      if (err) throw err;

      assert.equal(info.testnet, false);
      assert.equal(info.errors, '');
      done();
    });
  });

});
