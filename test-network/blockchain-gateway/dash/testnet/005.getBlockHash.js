const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

describe('DashGateway::getBlockHash', () => {

  it('1.1 Get invalid block number', (done) => {
    DashGateway.getBlockHash(-1, (err, hash) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Get genesis block', (done) => {
    DashGateway.getBlockHash(0, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, '00000bafbc94add76cb75e2ec92894837288a481e5c005f6563d91623bf8bc2c');
      done();
    });
  });

  it('2.2 Get a valid block', (done) => {
    DashGateway.getBlockHash(200000, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, '0000000010f0050fe3b367acae06810590be41c4b51127ee45b801bae48697fb');
      done();
    });
  });

});
