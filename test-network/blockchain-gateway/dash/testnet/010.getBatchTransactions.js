const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

const options1 = {
  fromBlockNumber: 200601,
  toBlockNumber: 200601,
};

const options2 = {
  fromBlockNumber: 200601,
  toBlockNumber: 200605,
};

describe('DashGateway::getBatchTransactions', () => {

  it('1.1 Invalid options: no block range in options', (done) => {
    DashGateway.getBatchTransactions({}, (err) => {
      // Error: You must specify the block range
      assert(err);
      done();
    });
  });

  it('1.2 Invalid options: from > to', (done) => {
    DashGateway.getBatchTransactions({ fromBlockNumber: 1000, toBlockNumber: 999 }, (err) => {
      // Error: Start block must be less or equal end block
      assert(err);
      done();
    });
  });

  it('1.3 Invalid options: invalid fromBlockNumber', (done) => {
    DashGateway.getBatchTransactions({ fromBlockNumber: -1, toBlockNumber: 999 }, (err) => {
      // Error: Start and end block number must be greater than zero
      assert(err);
      done();
    });
  });

  it('1.4 Invalid options: invalid toBlockNumber', (done) => {
    DashGateway.getBatchTransactions({ fromBlockNumber: 1000000, toBlockNumber: 1000001 }, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Valid options: get transactions of one block', (done) => {
    DashGateway.getBatchTransactions(options1, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

  it('2.2 Valid options: get transactions of multiple blocks', (done) => {
    DashGateway.getBatchTransactions(options2, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 6);
      done();
    });
  });

});
