const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

describe('DashGateway::getBlockCount', () => {

  it('Current block count should be greater than zero.', (done) => {
    DashGateway.getBlockCount((err, height) => {
      if (err) throw err;

      assert(height > 0);
      done();
    });
  });

});
