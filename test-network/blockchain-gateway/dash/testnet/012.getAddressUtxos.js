const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

const address = '8x6NSMqdk5ZfzCDnnSX2yJoFPSZnmZEAX9';

describe('DashGateway::getAddressUtxos', () => {

  it('1.1 Invalid address', (done) => {
    DashGateway.getAddressUtxos('abc', (err) => {
      // Error: Bad request
      assert.equal(err.status, 400);
      done();
    });
  });

  it('1.2 Valid address', (done) => {
    DashGateway.getAddressUtxos(address, (err, utxos) => {
      if (err) throw err;

      assert(utxos[0].address === address);
      assert(utxos.length >= 0);
      done();
    });
  });

  it('2.1 Sum of utxos should be equal to balance', (done) => {
    async.auto({
      balance: (next) => {
        DashGateway.getAddressBalance(address, next);
      },
      utxos: (next) => {
        DashGateway.getAddressUtxos(address, next);
      }
    }, (err, ret) => {
      if (err) throw err;

      const sumUtxo = _.reduce(ret.utxos, (sum, utxo) => sum + utxo.amountSat, 0);
      assert.equal(ret.balance, sumUtxo);
      done();
    });
  });

});
