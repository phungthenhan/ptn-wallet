const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

const blockNumber = 200515;
const blockHash = '0000000010d38df8f610c9089e0ce20102efd6d1c63c5d10e22376c4ddf1df57';

describe('DashGateway::getBlockTransactions', () => {

  it('1.1 Get invalid block number', (done) => {
    DashGateway.getBlockTransactions(-1, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block hash', (done) => {
    DashGateway.getBlockTransactions('abc', (err) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number', (done) => {
    DashGateway.getBlockTransactions(blockNumber, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 19);
      done();
    });
  });

  it('3.1 Get get valid block hash', (done) => {
    DashGateway.getBlockTransactions(blockHash, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 19);
      done();
    });
  });

});
