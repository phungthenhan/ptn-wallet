const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

const blockNumber = 200000;
const blockHash = '0000000010f0050fe3b367acae06810590be41c4b51127ee45b801bae48697fb';
const blockHex = '0000002008897e991b784653892dc51b81ac894179bf4d2f33bd27a3a227090f00000000ca893e8dd3757703fcc8a845dd934c18666cdac5718243af56b9279e55e80dadb6ac645b45cc121cb452a5760101000000010000000000000000000000000000000000000000000000000000000000000000ffffffff4c03400d0304b6ac645b08fabe6d6d4e546b6f476a58497445746d63506b62572d2d35584853467a765a67486969720100000000000000300000111b0000000d2f6e6f64655374726174756d2f00000000058074d21a000000001976a914b1582b812799c21c76527a045123ac840190533a88ac40230e43000000001976a9142601fe57011446c02d5de22bd88b5d309daea1b588ac403a690d000000001976a914c2c29ebc787954ef99d01c5f79115abf7012fb8e88ac403a690d000000001976a914d7b47d4b40a23c389f5a17754d7f60f511c7d0ec88ac403a690d000000001976a914dc3e0793134b081145ec0c67a9c72a7b297df27c88ac00000000';

describe('DashGateway::getOneBlock', () => {

  it('1.1 Get invalid block number', (done) => {
    DashGateway.getOneBlock(-1, (err, block) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block hash', (done) => {
    DashGateway.getOneBlock('abc', (err, block) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number', (done) => {
    DashGateway.getOneBlock(blockNumber, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

  it('3.1 Get get valid block hash', (done) => {
    DashGateway.getOneBlock(blockHash, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

});
