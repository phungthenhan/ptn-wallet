const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

const address = '8x6NSMqdk5ZfzCDnnSX2yJoFPSZnmZEAX9';

describe('DashGateway::getAddressBalance', () => {

  it('1.1 Invalid address', (done) => {
    DashGateway.getAddressBalance('abc', (err) => {
      // Error: Bad request
      assert.equal(err.status, 400);
      done();
    });
  });

  it('1.2 Valid address', (done) => {
    DashGateway.getAddressBalance(address, (err, balance) => {
      if (err) throw err;

      assert(balance >= 0);
      done();
    });
  });

});
