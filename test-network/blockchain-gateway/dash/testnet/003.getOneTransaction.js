const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestDashGateway');
const DashGateway = require('../../../../app/blockchain').DashGateway;

describe('DashGateway::getOneTransaction', () => {

  it('1.1 Get invalid txid', (done) => {
    DashGateway.getOneTransaction('abc', (err, tx) => {
      // Error: txid has wrong length/format
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get non-existed txid', (done) => {
    DashGateway.getOneTransaction('1111111111111111111111111111111111111111111111111111111111111111', (err, tx) => {
      // Error: No such mempool or blockchain transaction. Use gettransaction for wallet transactions.
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.3 Get valid txid', (done) => {
    const txid = 'd364f9078b162257d088dac56a8f24d428dc5cf9b934423954ee8b20fd5844c6';
    const rawTx = '020000000150876ae0a8f90ac829294c557b5713a1665fad939a953c18cf59c42273052c02000000006a473044022072e469da258ac647c71e4323208dc2e1da5ef9f78d20e0ececc632f45d59e6eb022016a71183bb0ade3f91611de0ed17bfc3a4879fd47847295c0fe503db9e95dfc10121038bde67c6fa64d8f4e16bc6b6a0d4ecd108ffb41c58ba5d32477b0b3a0a5d9c5afeffffff010e6325aa8c0000001976a9140d5bcbeeb459af40f97fcb4a98e9d1ed13e904c888ac510f0300';
    DashGateway.getOneTransaction(txid, (err, tx) => {
      if (err) throw err;

      assert.equal(tx.hex, rawTx);
      assert.equal(tx.txid, txid);
      done();
    });
  });

});
