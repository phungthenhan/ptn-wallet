require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

describe('QtumGateway::getTransactionsByIds', () => {

  it('1.1 Invalid txids', (done) => {
    QtumGateway.getTransactionsByIds(['abc', 'abc123', 'xyz'], (err, tx) => {
      // Error: txid has wrong length/format
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Non-existed txids', (done) => {
    const txids = [
      '1111111111111111111111111111111111111111111111111111111111111111',
      'd364f9078b162257d088dac56a8f24d428dc5cf9b934423954ee8b20fd5844c6',
      '1aacace487c3be81521be84f613ced93880171e6ca17868d68557c62c369d1c0',
      'ffcde456b8ac7066e9484b738f43742b4db7f49b6ff54a68483f64835ed7c674',
      '3a060f0499a14018d82025c3e9d579f662ae83fc074824a39403be4c6e0c7c11',
    ];
    QtumGateway.getTransactionsByIds(txids, (err, tx) => {
      // Error: No such mempool or blockchain transaction. Use gettransaction for wallet transactions.
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Valid txids', (done) => {
    const txids = [
      '90f6e4990a42fe4d1aa8cbf460f57e474f131bd7e566cf2231bf273a3e774c78',
      '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4',
    ];
    QtumGateway.getTransactionsByIds(txids, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      assert.equal(_.difference(_.map(txs, 'txid'), txids).length, 0);
      done();
    });
  });

  it('2.1 Valid but duplicated txids', (done) => {
    const txids = [
      '90f6e4990a42fe4d1aa8cbf460f57e474f131bd7e566cf2231bf273a3e774c78',
      '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4',
      '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4',
      '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4',
      '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4',
    ];
    QtumGateway.getTransactionsByIds(txids, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      assert.equal(_.difference(_.map(txs, 'txid'), txids).length, 0);
      done();
    });
  });

});
