require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

describe('QtumGateway::getRawTransaction', () => {

  it('1.1 Get invalid txid', (done) => {
    QtumGateway.getRawTransaction('abc', (err, tx) => {
      // Error: txid has wrong length/format
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get non-existed txid', (done) => {
    QtumGateway.getRawTransaction('1111111111111111111111111111111111111111111111111111111111111111', (err, tx) => {
      // Error: No such mempool or blockchain transaction. Use gettransaction for wallet transactions.
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get valid txid - without verbosity', (done) => {
    const txid = '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4';
    const rawTx = '01000000016128c5a4b93f28db43a3a29752076ec47b0bec6e9692bf16972617c0af55ee96010000006a473044022052dfaf8e5f3a22fb85e44817866299e68590d57738e516dd7ca37005d0e1a4f402201865c2964b778faea65971dff2ba51922ef3b3b79bfc480a5d245fcce0c1d7ba012103ced12a0464a95c1f8933b9117a8a04087249488a9d524d5aa93227a1e1b6f03cffffffff0200c2eb0b000000001976a9144f7e9b83dbefe41a3ccaf592d2be691c53d1818b88ac6865597d010000001976a9142d94b039402d4757c3481327577d6d1acc22b48088ac00000000';
    QtumGateway.getRawTransaction(txid, (err, tx) => {
      if (err) throw err;

      assert.equal(tx, rawTx);
      done();
    });
  });

  it('2.1 Get valid txid - with verbosity=0', (done) => {
    const txid = '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4';
    const rawTx = '01000000016128c5a4b93f28db43a3a29752076ec47b0bec6e9692bf16972617c0af55ee96010000006a473044022052dfaf8e5f3a22fb85e44817866299e68590d57738e516dd7ca37005d0e1a4f402201865c2964b778faea65971dff2ba51922ef3b3b79bfc480a5d245fcce0c1d7ba012103ced12a0464a95c1f8933b9117a8a04087249488a9d524d5aa93227a1e1b6f03cffffffff0200c2eb0b000000001976a9144f7e9b83dbefe41a3ccaf592d2be691c53d1818b88ac6865597d010000001976a9142d94b039402d4757c3481327577d6d1acc22b48088ac00000000';
    QtumGateway.getRawTransaction(txid, 0, (err, tx) => {
      if (err) throw err;

      assert.equal(tx, rawTx);
      done();
    });
  });

  it('2.3 Get valid txid - with verbosity=1', (done) => {
    const txid = '8743397bf5dcfe46cea27ce75b0bfedcc9f33807ee810ddfa12d458d45a834d4';
    const rawTx = '01000000016128c5a4b93f28db43a3a29752076ec47b0bec6e9692bf16972617c0af55ee96010000006a473044022052dfaf8e5f3a22fb85e44817866299e68590d57738e516dd7ca37005d0e1a4f402201865c2964b778faea65971dff2ba51922ef3b3b79bfc480a5d245fcce0c1d7ba012103ced12a0464a95c1f8933b9117a8a04087249488a9d524d5aa93227a1e1b6f03cffffffff0200c2eb0b000000001976a9144f7e9b83dbefe41a3ccaf592d2be691c53d1818b88ac6865597d010000001976a9142d94b039402d4757c3481327577d6d1acc22b48088ac00000000';
    QtumGateway.getRawTransaction(txid, 1, (err, tx) => {
      if (err) throw err;

      assert.equal(tx.hex, rawTx);
      assert.equal(tx.txid, txid);
      done();
    });
  });

});
