require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

describe('QtumGateway::getBlockCount', () => {

  it('Current block count should be greater than zero.', (done) => {
    QtumGateway.getBlockCount((err, height) => {
      if (err) throw err;

      assert(height > 0);
      done();
    });
  });

});
