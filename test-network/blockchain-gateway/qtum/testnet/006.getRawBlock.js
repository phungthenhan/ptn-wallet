require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

const blockNumber = 189869;
const blockHash = 'b77113f92f415412496e21ec778d09b22834d5247cb1feebb29bea9eb48255af';
const blockHex = '00000020d4a053f3a4a386e8f538843028b8782bf1fe08dffef56045f801a17376031483ff51e1561f712a32160caa072fa94a4f36f5e46fad0072656112e14bd3a072f06006685bfc95171a00000000dbf2c24fc48c4075f4eadefde0b0b1602afb415f374cf068bca96ffa100f633a37377ab05ba3abca08abdb26835988fbf19bae286d0fd032e0a28a784b7cd90c7bf0c134d75937973d004d234a1efae069619cfed78f22927377406c9f007bff010000004730450221009b9dbc3332e98eaa66dca828626c0dc92b79a18f896a1fd9dcf69638166f25850220466d90793753dfe07e983ca89cea2da77796d86b2382c3805104831d5788352d02020000000001010000000000000000000000000000000000000000000000000000000000000000ffffffff0503ade50200ffffffff020000000000000000000000000000000000266a24aa21a9ed598bdca4eab6001d5753ba33e9a50b1986649f4b477948b5f761decd6b742b4e012000000000000000000000000000000000000000000000000000000000000000000000000002000000017bf0c134d75937973d004d234a1efae069619cfed78f22927377406c9f007bff010000004847304402205dd90b292cc85d8e6940bfbfb005f5e39d662192d42a4c9b386e26e6bb7a99fe02201b69cad7ddb9a2a624eb6e910c478f38a47002ec89bf64656964435488467fea01ffffffff0b00000000000000000064c8d9a602000000232102d2c726c196402d533a961a9a1b61b7cc4910a99a4f466ac8ed8780214fb8793bac005a6202000000001976a9140ec1afc27860e1cd16d0283babbb523d1de04ace88ac005a6202000000001976a9142bbd1e4796443e108156cb3c4c6eb7ee81e141dc88ac005a6202000000001976a914cf1537ab9e46ab673dbef21c145a48cdaf7a4d8b88ac005a6202000000001976a914d188432c0b44cc6c7557d70a70fff64ee3812d3788ac005a6202000000001976a9149739ff641d773d3af8e2edaf638b68c16f02816d88ac005a6202000000001976a914b29ec1e265ccb28ad6da4451eb9f275dcebc022688ac005a6202000000001976a914728a9f6680c44a285e126e8e85069d849af4ee1488ac005a6202000000001976a9142e10fe88d6e075ad44a42f5c941599240aeba5fb88ac005a6202000000001976a9142820129a61e503e1fdecacd1133295ef51d2379a88ac00000000';

describe('QtumGateway::getRawBlock', () => {

  it('1.1 Get invalid block number: height < 0', (done) => {
    QtumGateway.getRawBlock(-1, (err, block) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block number: height > max', (done) => {
    QtumGateway.getRawBlock(1000000000, (err, block) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.3 Get invalid block hash', (done) => {
    QtumGateway.getRawBlock('abc', (err, block) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number - without verbosity', (done) => {
    QtumGateway.getRawBlock(blockNumber, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('2.2 Get get valid block number - with verbosity=0', (done) => {
    QtumGateway.getRawBlock(blockNumber, 0, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('2.3 Get get valid block number - with verbosity=1', (done) => {
    QtumGateway.getRawBlock(blockNumber, 1, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

  it('3.1 Get get valid block hash - without verbosity', (done) => {
    QtumGateway.getRawBlock(blockHash, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('3.2 Get get valid block hash - with verbosity=0', (done) => {
    QtumGateway.getRawBlock(blockHash, 0, (err, block) => {
      if (err) throw err;

      assert.equal(block, blockHex);
      done();
    });
  });

  it('3.3 Get get valid block hash - with verbosity=1', (done) => {
    QtumGateway.getRawBlock(blockHash, 1, (err, block) => {
      if (err) throw err;

      assert.equal(block.height, blockNumber);
      assert.equal(block.hash, blockHash);
      done();
    });
  });

});
