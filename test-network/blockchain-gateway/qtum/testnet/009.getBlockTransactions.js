require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

const blockNumber = 189869;
const blockHash = 'b77113f92f415412496e21ec778d09b22834d5247cb1feebb29bea9eb48255af';

describe('QtumGateway::getBlockTransactions', () => {

  it('1.1 Get invalid block number', (done) => {
    QtumGateway.getBlockTransactions(-1, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('1.2 Get invalid block hash', (done) => {
    QtumGateway.getBlockTransactions('abc', (err) => {
      // Error: Block not found
      assert.equal(err.code, -5);
      done();
    });
  });

  it('2.1 Get get valid block number', (done) => {
    QtumGateway.getBlockTransactions(blockNumber, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

  it('3.1 Get get valid block hash', (done) => {
    QtumGateway.getBlockTransactions(blockHash, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

});
