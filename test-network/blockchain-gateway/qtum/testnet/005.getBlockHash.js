require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

describe('QtumGateway::getBlockHash', () => {

  it('1.1 Get invalid block number', (done) => {
    QtumGateway.getBlockHash(-1, (err, hash) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Get genesis block', (done) => {
    QtumGateway.getBlockHash(0, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, '0000e803ee215c0684ca0d2f9220594d3f828617972aad66feb2ba51f5e14222');
      done();
    });
  });

  it('2.2 Get a valid block', (done) => {
    QtumGateway.getBlockHash(189869, (err, hash) => {
      if (err) throw err;

      assert.equal(hash, 'b77113f92f415412496e21ec778d09b22834d5247cb1feebb29bea9eb48255af');
      done();
    });
  });

});
