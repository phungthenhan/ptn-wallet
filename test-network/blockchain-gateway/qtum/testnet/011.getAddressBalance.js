require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

const address = 'qMiPgiJkEPj3t2GJ5Nw6VRGfcZsMcufwu3';

describe('QtumGateway::getAddressBalance', () => {

  it('1.1 Invalid address', (done) => {
    QtumGateway.getAddressBalance('abc', (err) => {
      // Error: Bad request
      assert.equal(err.status, 400);
      done();
    });
  });

  it('1.2 Valid address', (done) => {
    QtumGateway.getAddressBalance(address, (err, balance) => {
      if (err) throw err;

      assert(balance >= 0);
      done();
    });
  });

});
