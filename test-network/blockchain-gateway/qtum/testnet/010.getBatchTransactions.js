require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const assert = require('assert');
const logger = require('sota-core').getLogger('TestQtumGateway');
const QtumGateway = require('../../../../app/blockchain').QtumGateway;

const options1 = {
  fromBlockNumber: 189869,
  toBlockNumber: 189869,
};

const options2 = {
  fromBlockNumber: 189869,
  toBlockNumber: 189873,
};

describe('QtumGateway::getBatchTransactions', () => {

  it('1.1 Invalid options: no block range in options', (done) => {
    QtumGateway.getBatchTransactions({}, (err) => {
      // Error: You must specify the block range
      assert(err);
      done();
    });
  });

  it('1.2 Invalid options: from > to', (done) => {
    QtumGateway.getBatchTransactions({ fromBlockNumber: 1000, toBlockNumber: 999 }, (err) => {
      // Error: Start block must be less or equal end block
      assert(err);
      done();
    });
  });

  it('1.3 Invalid options: invalid fromBlockNumber', (done) => {
    QtumGateway.getBatchTransactions({ fromBlockNumber: -1, toBlockNumber: 999 }, (err) => {
      // Error: Start and end block number must be greater than zero
      assert(err);
      done();
    });
  });

  it('1.4 Invalid options: invalid toBlockNumber', (done) => {
    QtumGateway.getBatchTransactions({ fromBlockNumber: 1000000, toBlockNumber: 1000001 }, (err) => {
      // Error: Block height out of range
      assert.equal(err.code, -8);
      done();
    });
  });

  it('2.1 Valid options: get transactions of one block', (done) => {
    QtumGateway.getBatchTransactions(options1, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 2);
      done();
    });
  });

  it('2.2 Valid options: get transactions of multiple blocks', (done) => {
    QtumGateway.getBatchTransactions(options2, (err, txs) => {
      if (err) throw err;

      assert.equal(txs.length, 10);
      done();
    });
  });

});
