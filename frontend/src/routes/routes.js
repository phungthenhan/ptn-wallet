import DashboardLayout from '@/pages/Layout/DashboardLayout.vue';
import Login from '@/pages/Login.vue';
import WalletList from '@/pages/WalletList.vue';
import CoinDeposits from '@/pages/CoinDeposits.vue';
import CoinWithdrawls from '@/pages/CoinWithdrawls.vue';

const routes = [
  {
    path: '/login',
    component: Login,
    name: 'Login'
  },
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/wallets',
    children: [
      {
        path: 'wallets',
        name: 'Wallet management',
        component: WalletList
      },
      {
        path: 'coin-deposits',
        name: 'Coin deposits management',
        component: CoinDeposits
      },
      {
        path: 'coin-withdrawals',
        name: 'Coin withdrawls management',
        component: CoinWithdrawls
      }
    ]
  }
]

export default routes;
