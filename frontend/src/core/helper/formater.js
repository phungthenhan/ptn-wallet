/* eslint-disable */
import _ from 'lodash';
import BN from 'bignumber.js';
import Const from '../common/Const';

const keyedERC20Tokens = _.keyBy(Const.ETH_CONFIG.tokens, 'symbol');

export const formatCoinAmount = (coin, amount) => {
  let decimal = 0;
  const tokenDef = keyedERC20Tokens[coin];
  if (tokenDef) {
    decimal = tokenDef.decimal;
  } else {
    switch (coin) {
      case Const.COIN.ETH:
        decimal = 18;
        break;
      case Const.COIN.DASH:
      case Const.COIN.QTUM:
        decimal = 8;
        break;
      default:
        break;
    }
  }
  return (new BN(amount)).div(Math.pow(10, decimal)).toFormat();
};

export const unFormatCoinAmount = (coin, amount) => {
  let decimal = 0;
  const tokenDef = keyedERC20Tokens[coin];
  if (tokenDef) {
    decimal = tokenDef.decimal;
  } else {
    switch (coin) {
      case Const.COIN.ETH:
        decimal = 18;
        break;
      case Const.COIN.DASH:
      case Const.COIN.QTUM:
        decimal = 8;
        break;
      default:
        break;
    }
  }
  return (new BN(amount)).multipliedBy(Math.pow(10, decimal)).toString();
};

export const stringToTimeNumber = (timeString) => {
  let hour = 0;
  let minut = 0;
  try {
    hour = parseInt(timeString.substr(0, timeString.indexOf(":")));
    minut = parseInt(timeString.substr(timeString.indexOf(":") + 1));
    return (hour * 60 + minut) * 60 * 1000 || 0;
  }
  catch (e) {
    return 0;
  }
};

export const timeNumberToString = (timeNumber) => {
  timeNumber = parseInt(timeNumber);
  let hour = 0;
  let minut = 0;
  try {
    hour = parseInt(timeNumber / (60 * 60 * 1000));
    minut = parseInt((timeNumber - hour * 60 * 60 * 1000) / (60 * 1000));

    let hourString = hour > 9 ? hour.toString() : "0" + hour.toString();
    let minutString = minut > 9 ? minut.toString() : "0" + minut.toString();
    return `${hourString}:${minutString}`;
  }
  catch (e) {
    return "00:00";
  }
};

export const formatCoinGasPrice = (coin, amount) => {
  let decimal = 0;
  const tokenDef = keyedERC20Tokens[coin];
  if (tokenDef) {
    decimal = 9;
  } else {
    switch (coin) {
      case Const.COIN.ETH:
        decimal = 9;
        break;
      case Const.COIN.DASH:
        break;
      case Const.COIN.QTUM:
        break;
      default:
        break;
    }
  }

  return (new BN(amount)).dividedBy(Math.pow(10, decimal)).toString();
};

export const unFormatCoinGasPrice = (coin, amount) => {
  let decimal = 0;
  const tokenDef = keyedERC20Tokens[coin];
  if (tokenDef) {
    decimal = 9;
  } else {
    switch (coin) {
      case Const.COIN.ETH:
        decimal = 9;
        break;
      case Const.COIN.DASH:
        break;
      case Const.COIN.QTUM:
        break;
      default:
        break;
    }
  }

  return (new BN(amount)).multipliedBy(Math.pow(10, decimal)).toString();
};