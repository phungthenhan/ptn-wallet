/* eslint-disable */
import jQuery from 'jquery';
import Const from '../common/Const';
import ethNetworkConfig from '../../config/eth';
import dashNetworkConfig from '../../config/dash';
import neoNetworkConfig from '../../config/neo';

const urlPattern = new RegExp('^(https?:\\/\\/)?' + // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
  '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator

const Utils = {

  qs: function (key) {
    key = key.replace(/[*+?^$.[]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    let match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
  },

  numberWithCommas: function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  },

  isValidUrl: function (url) {
    return urlPattern.test(url);
  },

  growl: function (text, options) {
    jQuery.bootstrapGrowl(text, options);
  },

  vueRedirect: function (path) {
    if (window.vueRouter) {
      window.vueRouter.replace(path);
    } else {
      console.warn(`[ERROR] Utils.vueRedirect: window.vueRouter does not initialized...`);
    }
  },

  getBooleanDisplayName: function (val) {
    if (val === Const.BOOLEAN.FALSE) {
      return 'False';
    }

    if (val === Const.BOOLEAN.TRUE) {
      return 'True';
    }

    return `Invalid boolean (${val})`;
  },

  getDashExplorerLink_Address: function (address) {
    return dashNetworkConfig.explorerEndpoint + '/address/' + address;
  },

  getDashExplorerLink_Block: function (blockHash) {
    return dashNetworkConfig.explorerEndpoint + '/block/' + blockHash;
  },

  getDashExplorerLink_Transaction: function (txid) {
    return dashNetworkConfig.explorerEndpoint + '/tx/' + txid;
  },

  getQtumExplorerLink_Address: function (address) {
    return ethNetworkConfig.explorerEndpoint + '/address/' + address;
  },

  getQtumExplorerLink_Transaction: function (txid) {
    return ethNetworkConfig.explorerEndpoint + '/tx/' + txid;
  },

  getETHExplorerLink_Address: function (address) {
    return ethNetworkConfig.explorerEndpoint + '/address/' + address;
  },

  getETHExplorerLink_Transaction: function (txid) {
    return ethNetworkConfig.explorerEndpoint + '/tx/' + txid;
  },

  getETCExplorerLink_Address: function (address) {
    return ethNetworkConfig.explorerEndpoint + '/address/' + address;
  },

  getETCExplorerLink_Transaction: function (txid) {
    return ethNetworkConfig.explorerEndpoint + '/tx/' + txid;
  },

  getNeoExplorerLink_Address: function (address) {
    return neoNetworkConfig.explorerEndpoint + '/address/' + address;
  },

  getNeoExplorerLink_Transaction: function (txid) {
    return neoNetworkConfig.explorerEndpoint + '/transaction/' + txid;
  },

};

window.Utils = Utils;
export default Utils;