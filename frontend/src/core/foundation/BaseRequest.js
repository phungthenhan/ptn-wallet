import _ from 'lodash';
import axios from 'axios';
import auth from './Authenticator';

// Promised-based request
export default class BaseRequest {
  get (url, params = {}) {
    return this._doSelfRequest('GET', url, { params });
  }

  put (url, data = {}) {
    return this._doSelfRequest('put', url, { data });
  }

  post (url, data = {}) {
    return this._doSelfRequest('post', url, { data });
  }

  _doSelfRequest (method, url, paramsConfig) {
    const realUrl = `${process.env.VUE_APP_API_URL}/api${url}`;
    return this._doRequest(method, realUrl, paramsConfig)
  }

  _doRequest (method, url, paramsConfig) {
    const headers = {
      'Accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': this._getAuthToken()
    };

    const config = _.assign({
      method,
      url,
      headers
    }, paramsConfig);

    return new Promise((resolve, reject) => {
      axios(config)
        .then(response => {
          if (!response.data) {
            window.EventBus.$emit('EVENT_COMMON_ERROR', 'Invalid response format: ' + response);
            return;
          }

          if (!response.data.data) {
            resolve(response.data);
            return;
          }

          resolve(response.data.data);
        })
        .catch(err => {
          window.EventBus.$emit('EVENT_COMMON_ERROR', err);
        });
    });
  }

  _getAuthToken () {
    if (!auth._user) {
      return '';
    }

    return auth._user.token;
  }
};
