import _ from 'lodash';
import axios from 'axios';
import Utils from '../helper/util';
import Vue from 'vue';

const LOGIN_URL = '/login';

class Authenticator {
  constructor () {
    this._init();
  }

  _init () {
    this._user = null;

    // If there's auth token in query string
    const authToken = Utils.qs('access_token');
    if (authToken && authToken.length > 0) {
      if (window.G_USER) {
        this._user = window.G_USER;
      } else {
        this._user = {
          token: authToken
        };

        this.refreshUser(authToken);
      }

      return this;
    }

    // Otherwise get stored user in local storage
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      try {
        this._user = JSON.parse(storedUser);
      } catch (e) {
        console.log('Invalid stored user: ' + storedUser);
        localStorage.removeItem('user');
        this._user = null;
      }
    }

    return this;
  }

  login (credentials) {
    return new Promise((resolve, reject) => {
      axios.post(`${process.env.VUE_APP_API_URL}/api/login`, credentials)
        .then(response => {
          const user = response.data;
          const userData = JSON.stringify(user);
          this._user = JSON.parse(userData);
          localStorage.setItem('user', userData);
          resolve(user);
        })
        .catch(err => {
          window.EventBus.$emit('EVENT_COMMON_ERROR', err);
        });
    });
  }

  logout () {
    localStorage.removeItem('user');
    Utils.vueRedirect(LOGIN_URL);
    location.reload();
  }

  getUser () {
    if (!this._user) {
      Utils.vueRedirect(LOGIN_URL);
    }

    return this._user;
  }

  refreshUser (authToken) {
    const headers = {
      'x-auth-token': authToken || this._user.token,
      'accept': 'application/json'
    };
    const config = Object.assign({}, {
      headers,
      method: 'GET',
      url: `${process.env.VUE_APP_API_URL}/user/me`
    });
    axios(config)
      .then(res => {
        this._user = _.assign(this._user, res.body.data);
      })
      .catch(err => {
        window.EventBus.$emit('EVENT_COMMON_ERROR', err);
      });
  }
};

const auth = new Authenticator();
Vue.prototype.$auth = auth;
export default auth;
