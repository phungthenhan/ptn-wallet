import _ from 'lodash';
import BaseRequest from '../foundation/BaseRequest';

class CollectRequest extends BaseRequest {


  getCollectTransaction (coin, page, perPage) {
    let offSet = ((page-1) * perPage) < 0 ? 0 : ((page-1) * perPage);

    const url = `/${coin}/collect/transaction?p_offset=${offSet}&p_limit=${perPage}`;
    return this.get(url);
  }

  getListAddress (coin, page, perPage) {
    let offSet = ((page-1) * perPage) < 0 ? 0 : ((page-1) * perPage);
    const url = `/${coin}/address?p_offset=${offSet}&p_limit=${perPage}`;
    return this.get(url);
  }
  getCollectSetting (coin) {
    const url = `/${coin}/collect/setting`;
    return this.get(url);
  }


  updateCollectSetting (coin, params) {
    const url = `/${coin}/collect/setting`;
    return this.post(url, params);
  }


  collectCoin (coin, params) {
    const url = `/${coin}/collect/collectcoins`;
    return this.post(url, params);
  }

  manyCollectCoin (coin, params) {
    const url = `/${coin}/collect/manycollectcoins`;
    return this.post(url, params);
  }
}

const instance = new CollectRequest();
export default instance;
