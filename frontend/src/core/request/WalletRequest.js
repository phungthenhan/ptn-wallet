import _ from 'lodash';
import BaseRequest from '../foundation/BaseRequest';

class WalletRequest extends BaseRequest {
  getWalletAddresses (coin, walletId, params = {}) {
    const url = `/${coin}/wallet/${walletId}/addresses`;
    params = _.assign({}, params);
    return this.get(url, params);
  }

  getWallets (coin, params = {}) {
    const url = `/${coin}/wallet`;
    params = _.assign({}, params);
    return this.get(url, params);
  }

  generateWallet (coin, params) {
    const url = `/${coin}/wallet/generate`;
    return this.post(url, params);
  }

  createWalletAddress (coin, walletId, params) {
    const url = `/${coin}/wallet/${walletId}/address`;
    return this.post(url, params);
  }

  getBalance (address, params) {
    const url = `http://localhost:3001/insight-api-dash/addr/${address}`
    return this._doRequest('GET', url, params);
  }

  sendCoins (coin, walletId, params) {
    const url = `/${coin}/wallet/${walletId}/sendcoins`;
    return this.post(url, params);
  }

  getWalletTransactions (coin, walletId, params = {}) {
    const url = `/${coin}/wallet/${walletId}/transfer`;
    const finalParams = _.assign({}, params);

    return this.get(url, finalParams);
  }

  getCoinWithdrawals (coin, walletId, params = {}) {
    const url = `/${coin}/wallet/${walletId}/withdrawals`;
    return this.get(url, params);
  }

  getCoinDeposits (coin, walletId, params = {}) {
    const url = `/${coin}/wallet/${walletId}/deposits`;
    return this.get(url, params);
  }
}

const instance = new WalletRequest();
export default instance;
