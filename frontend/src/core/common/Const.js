/* eslint-disable */

const _           = require('lodash');
const CoreConst   = require('sota-core/common/Const');
const ETHConfig   = require('../../config/eth');
const DashConfig  = require('../../config/dash');

const AppConst = {
  WALLET_ID_LENGTH: 32,

  COIN: {
    DASH  : 'dash',
    ETH   : 'eth',
    QTUM  : 'qtum',
    USDT  : 'usdt',
    NEO   : 'neo',
    BTC   : 'btc'
  },

  UTXO_COIN: [
    'dash',
    'qtum',
    'btc'
  ],

  ERC20: _.keyBy(ETHConfig.tokens, 'symbol'),

  ETH_CONFIG: ETHConfig,

  DASH_CONFIG: DashConfig,

  WITHDRAW_STATUS: {
    UNSIGNED: 'unsigned',
    SIGNED: 'signed',
    SENT: 'sent',
    FAILED: 'failed',
    COMPLETED: 'completed',
  },

  COLLECT_STATUS: {
    SENT: 'sent',
    FAILED: 'failed',
    COMPLETED: 'completed',
  },

  TX_STATUS: {
    SUCCESS: '0x1',
    FAILED: '0x0',
  },

  PROCESSED_BLOCK_TYPE: {
    CRAWL: 'crawl',
    DEPOSIT: 'deposit'
  },

  WALLET_EVENT: {
    CREATED           : 'created',
    DEPOSIT           : 'deposit',
    WITHDRAW_REQUEST  : 'withdraw_request',
    WITHDRAW_VERIFIED : 'withdraw_verified',
    WITHDRAW_FAILED   : 'withdraw_failed',
    WITHDRAW_FEE      : 'withdraw_fee',
    ERC20_WITHDRAW_FEE: 'erc20_withdraw_fee',
    WITHDRAW_ACCEPTED : 'withdraw_accepted',
    WITHDRAW_DECLINED : 'withdraw_declined',
    COLLECT_FEE       : 'collect_fee'
  },

  ASYNC_BREAKER: 'KQhYpqLAM1QAf17khUGx',

  PUBSUB_CHANNEL: {
    APP: 'APP_CHANNEL_Z2G6S30WYY'
  },

  DEFAULT_PAGINATION: {
    fields: [
      {
        name: 'id',
        defaultOrder: CoreConst.PAGINATION.ORDER.DESC,
        canBeEqual: false,
      }
    ]
  },

  SATOSHI_FACTOR: 100000000,
  FEE_PER_KB: 10000,
  MIN_CONFIRMATIONS: 0,
  MAX_CONFIRMATIONS: 999999,
  /**
   * The longest that a raw utxo-based transaction can be saved in database.
   * - Bitcoin
   * - Dash
   * - Qtum
   */
  MAX_UTXO_BASED_TRANSACTION_SIZE: 2000,

  /**
   * The asset Neo.
   * - Neo
   * - Gas
   */
  NEO_ASSET: {
    NEO: 'NEO',
    GAS: 'GAS',
    NEO_HASH:'c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b',
    GAS_HASH:'602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7',
  },

  WEBHOOK_TYPE: {
    TRANSFER: 'transfer',
    TXCHANGED: 'tx_changed',
  },

};

module.exports = _.assign(CoreConst, AppConst);
