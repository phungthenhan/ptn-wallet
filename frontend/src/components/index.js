// Tables
import WalletTable from './Tables/WalletTable.vue'
import Transactions from './Tables/Transactions.vue'

export {
  Transactions,
  WalletTable
}
