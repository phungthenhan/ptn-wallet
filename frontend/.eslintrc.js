// http://eslint.org/docs/user-guide/configuring

module.exports = {
  "extends": [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  "rules": {
    "semi": 0
  },
  "parserOptions": {
    "parser": "babel-eslint"
  },
  "root": true,
  "env": {
    "node": true
  },
};
