/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'LatestBlockModel',

  $tableName: 'latest_block',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  /**
   * Find latest block with `type` that has been processed and recorded
   * If it's not existed, insert a new record with passed inital value
   */
  findOneOrInsert: function (coin, type, initialBlockNumber,  callback) {
    this.findOne({
      where: 'coin = ? AND type = ?',
      params: [coin, type]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      // Just return if record is existed
      if (ret) {
        return callback(null, ret);
      }

      // Otherwise create a new one
      this.add({
        coin,
        type,
        block_number: initialBlockNumber
      }, callback);
    });
  }
});
