/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                          = require('lodash');
const async                      = require('async');
const util                       = require('util');
const EthWithdrawalModel         = require('./EthWithdrawalModel');

module.exports = EthWithdrawalModel.extends({
  classname: 'Erc20WithdrawalModel',

  $tableName: 'erc20_withdrawal',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
