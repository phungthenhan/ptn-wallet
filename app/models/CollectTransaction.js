const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'CollectTransactionModel',

  $tableName: 'collect_transaction',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
