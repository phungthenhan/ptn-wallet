/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const Const             = require('../../app/common/Const');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const IWithdrawalModel  = require('../interfaces/IWithdrawalModel');

module.exports = BaseModel.extends({
  classname: 'EthWithdrawalModel',

  $tableName: 'eth_withdrawal',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
}).implements([IWithdrawalModel]);
