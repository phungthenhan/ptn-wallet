/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const Const             = require('../common/Const');
const BaseModel         = require('sota-core').load('model/BaseModel');
const ErrorFactory      = require('sota-core').load('error/ErrorFactory');

module.exports = BaseModel.extends({
  classname: 'WalletBalanceModel',

  $tableName: 'wallet_balance',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  findOneOrCreate: function (walletId, coin, callback) {
    async.waterfall([
      (next) => {
        this.findOne({
          where: 'wallet_id = ? AND coin = ?',
          params: [walletId, coin]
        }, next);
      },
      (ret, next) => {
        if (ret) {
          return next(null, ret);
        }

        this.add({
          wallet_id: walletId,
          coin: coin,
          balance: 0
        }, next);
      }
    ], callback);
  },

  findOneOrFail: function (walletId, coin, callback) {
    this.findOne({
      where: 'wallet_id = ? AND coin = ?',
      params: [walletId, coin]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (!ret) {
        return callback(`Wallet ${walletId} doesn't have balance record for ${coin} yet.`);
      }

      return callback(null, ret);
    });
  },

  addDeposit: function (depositRecord, walletId, coin, amount, callback) {
    const WalletLogModel = this.getModel('WalletLogModel');
    async.parallel([
      (next) => {
        async.waterfall([
          (_next) => {
            this.findOne({
              where: 'wallet_id = ? AND coin = ?',
              params: [walletId, coin],
            }, _next);
          },
          (ret, _next) => {
            if (ret) {
              this.update({
                set: '`balance` = `balance` + ?',
                where: '`wallet_id` = ? AND `coin` = ?',
                params: [amount, walletId, coin]
              }, _next);
            } else {
              const options = {};
              this.add({
                wallet_id: walletId,
                coin: coin,
                balance: amount
              }, options, _next);
            }
          }
        ], next);
      },
      (next) => {
        WalletLogModel.add({
          wallet_id: walletId,
          coin: coin,
          event: Const.WALLET_EVENT.DEPOSIT,
          balance_change: amount,
          ref_id: depositRecord.id,
        }, next);
      }
    ], callback);
  },

  addWithdrawal: function (withdrawalRecord, walletId, coin, amount, callback) {
    const WalletLogModel = this.getModel('WalletLogModel');
    async.parallel([
      (next) => {
        this.update({
          set: '`balance` = `balance` - ?, `withdrawal_pending` = `withdrawal_pending` + ?',
          where: 'wallet_id = ? AND coin = ?',
          params: [amount, amount, walletId, coin],
        }, next);
      },
      (next) => {
        WalletLogModel.add({
          wallet_id: walletId,
          coin: coin,
          event: Const.WALLET_EVENT.WITHDRAW_REQUEST,
          balance_change: -amount,
          ref_id: withdrawalRecord.id,
        }, next);
      }
    ], callback);
  },

  verifyWithdrawal: function (withdrawalRecord, walletId, coin, amount, callback) {
    const WalletLogModel = this.getModel('WalletLogModel');
    const feeCoin = !!Const.ERC20[coin] ? Const.COIN.ETH : coin;
    const fee = withdrawalRecord.fee;

    async.parallel([
      (next) => {
        this.update({
          set: '`withdrawal_pending` = `withdrawal_pending` - ?',
          where: 'wallet_id = ? AND coin = ?',
          params: [amount, walletId, coin]
        }, next);
      },
      (next) => {
        WalletLogModel.add({
          wallet_id: walletId,
          coin: coin,
          event: Const.WALLET_EVENT.WITHDRAW_VERIFIED,
          balance_change: -amount,
          ref_id: withdrawalRecord.id,
        }, next);
      },
      (next) => {
        this.update({
          set: '`balance` = `balance` - ?',
          where: 'wallet_id = ? AND coin = ?',
          params: [fee, walletId, feeCoin]
        }, next);
      },
      (next) => {
        WalletLogModel.add({
          wallet_id: walletId,
          coin: feeCoin,
          event: !!Const.ERC20[coin] ? Const.WALLET_EVENT.ERC20_WITHDRAW_FEE : Const.WALLET_EVENT.WITHDRAW_FEE,
          balance_change: -fee,
          ref_id: withdrawalRecord.id,
        }, next);
      }
    ], callback);
  },

  invalidateWithdrawal: function (withdrawalRecord, walletId, coin, amount, callback) {
    const WalletLogModel = this.getModel('WalletLogModel');
    async.parallel([
      (next) => {
        this.update({
          set: '`balance` = `balance` + ?, `withdrawal_pending` = `withdrawal_pending` - ?',
          where: 'wallet_id = ? AND coin = ?',
          params: [amount, amount, walletId, coin]
        }, next);
      },
      (next) => {
        WalletLogModel.add({
          wallet_id: walletId,
          coin: coin,
          event: Const.WALLET_EVENT.WITHDRAW_FAILED,
          balance_change: amount,
          ref_id: withdrawalRecord.id,
        }, next);
      }
    ], callback);
  },

});