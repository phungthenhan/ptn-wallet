/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                     = require('lodash');
const async                 = require('async');
const util                  = require('util');
const BaseModel             = require('sota-core').load('model/BaseModel');
const WalletWebhookEntity   = require('../entities/WalletWebhookEntity');

module.exports = BaseModel.extends({
  classname: 'WalletWebhookModel',

  $Entity: WalletWebhookEntity,

  $tableName: 'wallet_webhook',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
