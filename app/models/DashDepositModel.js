/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                   = require('lodash');
const async               = require('async');
const util                = require('util');
const DashDepositEntity   = require('../entities/DashDepositEntity');
const BaseModel           = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'DashDepositModel',

  $Entity: DashDepositEntity,

  $tableName: 'dash_deposit',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
