/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                   = require('lodash');
const async               = require('async');
const util                = require('util');
const NeoDepositEntity   = require('../entities/NeoDepositEntity');
const BaseModel           = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'NeoDepositModel',
  
  $Entity: NeoDepositEntity,
  
  $tableName: 'neo_deposit',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
  
});
