/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const IWithdrawalModel  = require('../interfaces/IWithdrawalModel');

module.exports = BaseModel.extends({
  classname: 'DashWithdrawalModel',

  $tableName: 'dash_withdrawal',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
}).implements([IWithdrawalModel])
