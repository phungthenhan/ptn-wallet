/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const IAppModel         = require('../interfaces/IAppModel');
const IHotWalletModel   = require('../interfaces/IHotWalletModel');
const Const             = require('../../app/common/Const');
const BaseModel         = require('sota-core').load('model/BaseModel');
const Utils             = require('sota-core').load('util/Utils');
const HotWalletEntity   = require('../entities/HotWalletEntity');

module.exports = BaseModel.extends({
  classname: 'BtcHotWalletModel',

  $Entity: HotWalletEntity,
  $tableName: 'btc_hot_wallet',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
  $excludedCols: ['private_key', 'created_at', 'updated_at', 'created_by', 'updated_by'],

  getNonSendableWallets: function (callback) {
    const BtcWithdrawalModel = this.getModel('BtcWithdrawalModel');

    async.waterfall([
      (next) => {
        BtcWithdrawalModel.find({
          columns: ['from_address'],
          where: "status = ? OR status = ?",
          params: [Const.WITHDRAW_STATUS.SIGNED, Const.WITHDRAW_STATUS.SENT],
          groupBy: ['from_address'],
        }, next);
      },

      (ret, next) => {
        if (!ret.length){
          return next(null, []);
        }
        const addresses = _.map(ret, 'fromAddress');

        this.find({
          where: this.whereIn('address', addresses.length),
          params: addresses
        }, next);
      }
    ], callback)
  }

}).implements([IHotWalletModel, IAppModel]);