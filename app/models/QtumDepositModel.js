/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const DashDepositEntity = require('../entities/QtumDepositEntity');
const BaseModel         = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'QtumDepositModel',

  $Entity: DashDepositEntity,

  $tableName: 'qtum_deposit',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
