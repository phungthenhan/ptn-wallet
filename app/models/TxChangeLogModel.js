/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'TxChangeLogModel',

  $tableName: 'tx_change_log',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  appendLog: function (changeInfo, callback) {
    this.add({
      tx_old: changeInfo.from,
      tx_new: changeInfo.to,
      coin: changeInfo.coin
    }, callback);
  }
});