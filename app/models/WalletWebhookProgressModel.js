/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                           = require('lodash');
const async                       = require('async');
const util                        = require('util');
const BaseModel                   = require('sota-core').load('model/BaseModel');
const WalletWebhookProgressEntity = require('../entities/WalletWebhookProgressEntity');

module.exports = BaseModel.extends({
  classname: 'WalletWebhookProgressModel',

  $Entity: WalletWebhookProgressEntity,

  $tableName: 'wallet_webhook_progress',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  appendTxChangeProgress: function (webHookInfo, changeInfo, callback) {
    const webHookProgressInfo = webHookInfo;
    this.findOne({
      where: 'webhook_id = ? AND txid = ?',
      params: [webHookProgressInfo.webhook_id, changeInfo.from],
    }, (err, ret) => {
      if (err){
        return callback(err);
      }

      async.waterfall([
        (next) => {
          if (!ret){
            webHookProgressInfo.txid = changeInfo.from;
            this.add(webHookProgressInfo, { isInsertIgnore: true }, next);
          } else return next(null, null)
        },
        (_ret, next) => {
          webHookProgressInfo.txid = changeInfo.to;
          this.add(webHookProgressInfo, { isInsertIgnore: true }, next);
        },
        (_ret, next) => {
          this.update({
            set: "data = ?",
            where: "webhook_id = ? AND txid = ?",
            params: [JSON.stringify(changeInfo), webHookProgressInfo.webhook_id, changeInfo.from],
          }, next)
        },

      ], callback)
    });
  }

});
