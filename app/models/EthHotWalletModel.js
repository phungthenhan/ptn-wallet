/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const Const             = require('../../app/common/Const');
const BaseModel         = require('sota-core').load('model/BaseModel');
const IAppModel         = require('../interfaces/IAppModel');
const IHotWalletModel   = require('../interfaces/IHotWalletModel');
const Utils             = require('sota-core').load('util/Utils');
const HotWalletEntity   = require('../entities/HotWalletEntity');
const ErrorFactory      = require('sota-core').load('error/ErrorFactory');

module.exports = BaseModel.extends({
  classname: 'EthHotWalletModel',

  $Entity: HotWalletEntity,
  $tableName: 'eth_hot_wallet',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
  $excludedCols: ['private_key', 'created_at', 'updated_at', 'created_by', 'updated_by'],

  getNonSendableWallets: function (callback) {
    const EthWithdrawalModel = this.getModel('EthWithdrawalModel');
    const Erc20WithdrawalModel = this.getModel('Erc20WithdrawalModel');

    async.waterfall([
      (next) => {
        EthWithdrawalModel.find({
          columns: ['from_address'],
          where: "status = ? OR status = ?",
          params: [Const.WITHDRAW_STATUS.SIGNED, Const.WITHDRAW_STATUS.SENT],
          groupBy: ['from_address'],
        }, next);
      },

      (ethHotWallet, next) => {
        Erc20WithdrawalModel.find({
          columns: ['from_address'],
          where: "status = ? OR status = ?",
          params: [Const.WITHDRAW_STATUS.SIGNED, Const.WITHDRAW_STATUS.SENT],
          groupBy: ['from_address'],
        }, (err, erc20HotWallet) => {
          if (err) return next(err);
          return next(null, erc20HotWallet.concat(ethHotWallet))
        });
      },

      (ret, next) => {
        if (!ret.length){
          return next(null, []);
        }
        const addresses = _.map(ret, 'fromAddress');

        this.find({
          where: this.whereIn('address', addresses.length),
          params: addresses
        }, next);
      }
    ], callback)
  }
}).implements([IHotWalletModel, IAppModel]);