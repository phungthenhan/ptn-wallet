/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const UserEntity        = require('../entities/UserEntity');

module.exports = BaseModel.extends({
  classname: 'QtumAddressModel',

  $tableName: 'qtum_address',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  $excludedCols: ['private_key', 'wif', 'created_at', 'updated_at', 'created_by', 'updated_by'],

});
