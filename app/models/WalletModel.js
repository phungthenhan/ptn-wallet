/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const ErrorFactory      = require('sota-core').load('error/ErrorFactory');
const WalletEntity      = require('../entities/WalletEntity');

module.exports = BaseModel.extends({
  classname: 'WalletModel',

  $Entity: WalletEntity,

  $excludedCols: ['change_private_key', 'created_at', 'updated_at', 'created_by', 'updated_by'],

  $tableName: 'wallet',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  findOneOrFail: function (id, callback) {
    this.findOne(id, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (!ret) {
        if (!ret) return callback(ErrorFactory.notFound(`Cannot find wallet by id: ${id}`));
      }

      return callback(null, ret);
    });
  },

});
