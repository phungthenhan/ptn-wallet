/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const Utils             = require('sota-core').load('util/Utils');
const UserEntity        = require('../entities/UserEntity');

function isPrimitive(test) {
  return (test !== Object(test));
};

module.exports = BaseModel.extends({
  classname: 'EthAddressModel',

  $tableName: 'eth_address',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },
  $excludedCols: ['private_key', 'created_at', 'updated_at', 'created_by', 'updated_by'],

  add: function($super, data, options, callback) {
    if (typeof options === 'function') {
      callback = options;
      options = null;
    }

    const WalletModel = this.getModel('WalletModel');

    async.waterfall([
      (next) => {
        WalletModel.findOneOrFail(data.walletId, next);
      },
      (wallet, next) => {
        $super(data, options, next);
      }
    ], callback);
  },

  _parseQueryParams: function (projection, options, callback) {
    if (typeof projection === 'function') {
      callback = projection;
      projection = null;
    }

    if (typeof options === 'function') {
      callback = options;
      options = {};
    }

    // Select all fields/columns
    if (projection === '*') {
      projection = null;
    }

    return { projection, options, callback };
  },

  _parseMongooseConditions: function (conditions) {
    // Mongoose-style
    let whereConditions = [];
    let params = [];
    const fields = _.keys(conditions);
    _.forEach(fields, (field) => {
      const columnName = Utils.convertToSnakeCase(field);
      const condition = conditions[field];
      if (isPrimitive(condition)) {
        whereConditions.push(`\`${columnName}\` = ?`);
        params.push(condition);
        return;
      }

      if (condition['$in']) {
        const vals = condition['$in'];
        whereConditions.push(this.whereIn(columnName, vals.length));
        params = params.concat(vals);
        return;
      }

      throw new Error(`Unsupport query condition: ${conditions}`);
    });
    const where = whereConditions.join(' AND ');

    return { where, params };
  },

  // Just experiment before implementing in core module
  // Try to make mongoose-like query for BaseModel
  find: function ($super, conditions, _projection = null, _options = {}, _callback) {
    const { projection, options, callback } = this._parseQueryParams(_projection, _options, _callback);
    // Old style
    if (conditions.where || conditions.params || conditions.orderBy || conditions.limit || conditions.offset)  {
      $super(conditions, callback);
      return;
    }

    $super(this._parseMongooseConditions(conditions), callback);
  },

  findOne: function ($super, conditions, _projection = null, _options = {}, _callback) {
    const { projection, options, callback } = this._parseQueryParams(_projection, _options, _callback);
    // Old style
    if (conditions.where || conditions.params || conditions.orderBy || conditions.limit || conditions.offset)  {
      $super(conditions, callback);
      return;
    }

    $super(this._parseMongooseConditions(conditions), callback);
  },

});
