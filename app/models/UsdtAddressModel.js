/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');
const UserEntity        = require('../entities/UserEntity');

module.exports = BaseModel.extends({
  classname: 'UsdtAddressModel',

  $tableName: 'usdt_address',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

  $excludedCols: ['private_key', 'wallet_id', 'created_at', 'updated_at', 'created_by', 'updated_by'],

  add: function($super, data, options, callback) {
    if (typeof options === 'function') {
      callback = options;
      options = null;
    }

    const WalletModel = this.getModel('WalletModel');

    async.waterfall([
      (next) => {
        WalletModel.findOneOrFail(data.walletId, next);
      },
      (wallet, next) => {
        $super(data, options, next);
      }
    ], callback);
  },

});