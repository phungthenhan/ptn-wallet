/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                   = require('lodash');
const async               = require('async');
const util                = require('util');
const BtcDepositEntity   = require('../entities/BtcDepositEntity');
const BaseModel           = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'BtcDepositModel',

  $Entity: BtcDepositEntity,

  $tableName: 'btc_deposit',
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
