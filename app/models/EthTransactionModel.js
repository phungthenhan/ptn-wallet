/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
const _                 = require('lodash');
const async             = require('async');
const util              = require('util');
const BaseModel         = require('sota-core').load('model/BaseModel');

module.exports = BaseModel.extends({
  classname: 'EthTransactionModel',

  $tableName: process.env.ETH_NETWORK === 'mainnet' ? 'eth_transaction' : `eth_transaction_${process.env.ETH_NETWORK}`,
  $dsConfig: {
    read: 'mysql-slave',
    write: 'mysql-master'
  },

});
