const _                       = require('lodash');
const async                   = require('async');
const moment                  = require('moment');
const Const                   = require('../common/Const');
const AppService              = require('./AppService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const Paginator2              = require('sota-core').load('util/Paginator2');
const logger                  = require('sota-core').getLogger('BaseCoinWalletService');
const BigNumber               = require('bignumber.js')

module.exports = AppService.extends({
  classname: 'BaseCoinWalletService',

  $_coinSymbol: null, // Must be defined in derived class

  /**
   * GET WALLET DETAILS
   * This API call retrieves wallet object information by the wallet ID.
   * This is useful to get the balance of the wallet, or identify the keys
   * used to sign with the wallet using the Get Keychain API.
   */
  getOneWallet: function (coin, walletId, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');

    async.auto({
      wallet: (next) => {
        WalletModel.findOneOrFail(walletId, next);
      },
      balance: (next) => {
        WalletBalanceModel.findOneOrCreate(walletId, this._coinSymbol, next);
      },
    }, (err, ret) => {
      if (err) return callback(err);

      if (!ret.wallet) {
        return callback(ErrorFactory.notFound(`Cannot find wallet: ${walletId}`));
      }

      const wallet = ret.wallet.toJSON();
      if (wallet.coin !== this._coinSymbol) {
        return callback(ErrorFactory.badRequest(`Wallet ${walletId} is not for coin ${this._coinSymbol}`));
      }

      const balance = ret.balance.toJSON();
      wallet.balance = parseInt(balance.balance);
      wallet.balanceString = balance.balance.toString();

      return callback(null, wallet);
    });
  },

  /**
   * CREATE WALLET ADDRESS
   * This API call is used to create a new receive address for your wallet, which enhances your privacy.
   * You may choose to call this API whenever a deposit is made.
   * The Bitkoex-Wallet API supports millions of addresses.
   * Please check the “Coin-Specific Implementation” with regards to fee address management for ethereum.
   */
  createWalletAddress: function (walletId, callback) {
    const WalletModel = this.getModel('WalletModel');

    async.auto({
      wallet: (next) => {
        WalletModel.findOneOrFail(walletId, next);
      },
      record: ['wallet', (ret, next) => {
        if (ret.wallet.coin !== this._coinSymbol) {
          return next(ErrorFactory.badRequest('Mismatch wallet\'s coin type.'));
        }

        this._createWalletAddress(walletId, callback);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, {
        address: ret.record.address,
        coin: this._coinSymbol,
        walletId: ret.record.walletId,
      });
    });
  },

  /**
   * GET WALLET ADDRESS
   * This API call is used to get information about a single wallet address.
   * The address object can be fetched by using either the id or the address itself.
   */
  getOneWalletAddress: function (address, walletId, callback) {
    const modelPrefix = this._getModelPrefix();
    const CoinAddressModel = this.getModel(`${modelPrefix}AddressModel`);
    const WalletModel = this.getModel('WalletModel');

    async.waterfall([
      (next) => {
        CoinAddressModel.findOne({
          where: 'address = ?',
          params: [address]
        }, next);
      },
      (add, next) => {
        if (!add) {
          return next(ErrorFactory.notFound(`Cannot find address: ${address}`));
        }

        if (add.walletId !== walletId) {
          return next(ErrorFactory.badRequest(`Address [${address}] does not belong to wallet [${walletId}]`));
        }

        return next(null, add);
      }
    ], callback);
  },

  getWalletAddresses: function (walletId, pagination, callback) {
    const modelPrefix = this._getModelPrefix();
    const CoinAddressModel = this.getModel(`${modelPrefix}AddressModel`);
    const WalletModel = this.getModel('WalletModel');

    async.auto({
      addresses: (next) => {
        CoinAddressModel.find({
          where:  'wallet_id = ?',
          params: [walletId],
          orderBy: 'created_at DESC',
          limit: pagination.limit,
          offset: pagination.offset
        }, next);
      },
      count: (next) => {
        CoinAddressModel.count({
          where:  'wallet_id = ?',
          params: [walletId]
        }, next);
      }
    }, callback);
  },

  listCoinWithdrawals: function(tokenSymbol, walletId, pagination, callback) {
    const modelPrefix = this._getModelPrefix();
    const CoinWithdrawalModel = this.getModel(`${modelPrefix}WithdrawalModel`);

    async.auto({
      withdrawals: (next) => {
        CoinWithdrawalModel.find({
          where: 'wallet_id = ?',
          params: [walletId],
          orderBy: 'created_at DESC',
          limit: pagination.limit,
          offset: pagination.offset,
        }, next);
      },
      count: (next) => {
        CoinWithdrawalModel.count({
          where: 'wallet_id = ?',
          params: [walletId],
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret);
    });
  },

  listCoinDeposits: function(tokenSymbol, walletId, pagination, callback) {
    const modelPrefix = this._getModelPrefix();
    const CoinDepositModel = this.getModel(`${modelPrefix}DepositModel`);

    async.auto({
      deposits: (next) => {
        CoinDepositModel.find({
          where: 'wallet_id = ?',
          params: [walletId],
          orderBy: 'block_timestamp DESC',
          limit: pagination.limit,
          offset: pagination.offset,
        }, next);
      },
      count: (next) => {
        CoinDepositModel.count({
          where: 'wallet_id = ?',
          params: [walletId],
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret);
    });
  },

  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  sendCoins: function (userId, coin, params, callback) {
    const walletId = params.walletId;
    const address = params.address;
    const amount = params.amount;
    let withdrawal = null;

    async.waterfall([
      (next) => {
        this._createWithdrawalRecord(walletId, coin, address, amount, next);
      },
      (record, next) => {
        withdrawal = record;
        logger.info(`Created withdrawal: ${JSON.stringify(record.toJSON())}`);
        this._subtractBalance(withdrawal, walletId, params.coin, amount, next);
      },
    ], (err) => {
      if (err) {
        return callback(err);
      }

      return callback(null, {
        txid: withdrawal.txid,
        status: Const.WITHDRAW_STATUS.SIGNED,
      });
    });
  },

  _subtractBalance: function (withdrawalRecord, walletId, coin, amount, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');

    amount = new BigNumber(amount);

    async.waterfall([
      (next) => {
        WalletBalanceModel.findOneOrFail(walletId, coin, next);
      },
      (wallet, next) => {
        const balance = new BigNumber(wallet.balance)
        if (amount.isGreaterThan(balance)) {
          return next(ErrorFactory.badRequest('Insufficient wallet\'s balance.'));
        }

        WalletBalanceModel.addWithdrawal(withdrawalRecord, walletId, coin, amount.toNumber(), next);
      }
    ], callback);
  },

  /**
   * GET ONE WALLET TRANSACTION
   * Wallet transfers represent digital currency sends and receives on your wallet.
   * This is the recommended method to track funding withdrawals and deposits.
   */
  getOneTransaction: function (txid, callback) {
    throw new Error(`Must be implemented in derived class: ${this.classname}`);
  },

  _getModelPrefix: function () {
    return this._coinSymbol.charAt(0).toUpperCase() + this._coinSymbol.slice(1);
  },

  getAddressBalance : function (coin, address, callback) {
    async.waterfall([
      (next) => {
        this.getCoinAddressBalance(coin, address, next);
      }
    ], (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(err, {
        coin: coin,
        address: address,
        balance: ret,
      });
    });
  },

  getTransactionTrace: function (coin, txid, callback) {
    const TxChangeLogModel = this.getModel('TxChangeLogModel');
    let checkNextData = true;
    let toTxid = txid;

    async.whilst(
      function() { return checkNextData; },
      function(next) {
        TxChangeLogModel.findOne({
          where: `tx_old = ?`,
          params: [toTxid]
        }, (err, ret) => {
          if (!ret){
            checkNextData = false;
          } else {
            toTxid = ret.txNew;
          }
          return next(null, {
            from: txid,
            to: toTxid
          })
        });
      }, callback
    );
  }
});