const _                       = require('lodash');
const async                   = require('async');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const bitcore                 = require('bitcore-lib');
const ethConfig               = require('../../config/eth');
const WalletService           = require('./WalletService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const logger                  = require('sota-core').getLogger('UTXOBasedWalletService');

module.exports = WalletService.extends({
  classname: 'UTXOBasedWalletService',

  /**
   * GENERATE WALLET
   * This API call creates a new wallet. Under the hood, the SDK (or Bitkoex-Wallet Express) does the following:
   * 1. Creates the user keychain locally on the machine, and encrypts it with the provided passphrase
   (skipped if userKey is provided).
   * 2. Ceates the backup keychain locally on the machine, and encrypts it with the provided passphrase
   (skipped if backupXpub or backupXpubProvider are provided).
   * 3. Uploads the encrypted user and backup keychains to Bitkoex-Wallet.
   * 4. Creates the Bitkoex-Wallet key (and the backup key if backupXpubProvider is set) on the service.
   * 5. Creates the wallet on Bitkoex-Wallet with the 3 public keys above.
   */
  createWallet: function (userId, coin, label, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');

    async.auto({
      wallet: (next) => {
        WalletModel.add({
          coin: coin,
          label: label,
          user_id: userId,
        }, next);
      },

      generateChangeAddress: ['wallet', (ret, next) => {
        this.createUnsaveWalletAddress(ret.wallet.id, coin, next);
      }],

      saveChangeAddress: ['generateChangeAddress', (ret, next) => {
        WalletModel.update({
          set: 'change_address = ?, change_private_key =? ',
          where: 'id = ?' ,
          params: [ret.generateChangeAddress.address, ret.generateChangeAddress.privateKey, ret.wallet.id],
        }, next);
      }],

      generateDelegateAddress: ['wallet', (ret, next) => {
        this._modelPrefix = coin.charAt(0).toUpperCase() + coin.slice(1);
        const CoinWalletService = this.getService(`${this._modelPrefix}WalletService`);
        CoinWalletService.createWalletAddress(ret.wallet.id, next);
      }],

      saveDelegateAddress: ['generateDelegateAddress', (ret, next) => {
        WalletModel.update({
          set: 'delegate_address = ?',
          where: 'id = ?' ,
          params: [ret.generateDelegateAddress.address, ret.wallet.id],
        }, next);
      }],

      balances: ['wallet', (ret, next) => {
        const data = [];
        data.push({
          wallet_id: ret.wallet.id,
          coin: coin,
        });

        WalletBalanceModel.add(data, next);
      }],

    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret.wallet);
    });
  },


});