const async                   = require('async');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const BaseUserService         = require('sota-core').load('service/UserService');
const logger                  = require('sota-core').getLogger('UserService');

module.exports = BaseUserService.extends({
  classname: 'UserService',

  /*
   * GET CURRENT USER PROFILE
   * This API call retrieves information about the current authenticated user.
   * You can use this API call as a sort of sanity test or if you use multiple users in your code.
   */
  getUserDetails: function (userId, callback) {
    const UserModel = this.getModel('UserModel');

    UserModel.findOne(userId, callback);
  },

  /*
   * GET CURRENT SESSION INFORMATION
   * This API call retrieves information about the current session access token.
   * You can use this API call if you’d like to verify the scope of your access token,
   * which will include allowed scopes as well as the expiration date of the token and any associated unlocks.
   */
  getCurrentSession: function (userId, callback) {
    // TBD: implement me.
    return callback(`TBD: implement me.`);
  },

});
