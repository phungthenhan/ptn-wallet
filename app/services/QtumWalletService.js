const _                           = require('lodash');
const async                       = require('async');
const request                     = require('superagent');
const moment                      = require('moment');
const Const                       = require('../common/Const');
const QtumGateway                 = require('../blockchain').QtumGateway;
const UTXOBasedCoinWalletService  = require('./UTXOBasedCoinWalletService');
const ErrorFactory                = require('sota-core').load('error/ErrorFactory');
const logger                      = require('sota-core').getLogger('QtumWalletService');

module.exports = UTXOBasedCoinWalletService.extends({
  classname: 'QtumWalletService',

  $_coinSymbol: Const.COIN.QTUM,

  getGateway: function () {
    return QtumGateway;
  },

  getWalletRPCHost: function () {
    return process.env.QTUM_WALLET_RPC_HOST;
  },

  getWalletNetwork: function () {
    return process.env.QTUM_NETWORK;
  },

  getOneTransaction: function (txid, callback) {
    const WalletModel = this.getModel('WalletModel');
    const QtumAddressModel = this.getModel('QtumAddressModel');

    if (txid.match(/^TMP_WITHDRAWAL_TX_(.+)/)){
      return callback(null, {
        id: txid,
        date: '0000-00-00',
        time: 0,
        blocktime: 0,
        blockHeight: -1,
        confirmations: -1,
        entries: [],
      });
    }

    if (!txid.match(/^([A-Fa-f0-9]{64})$/)) {
      return callback(ErrorFactory.badRequest(`Invalid txid format: ${txid}`));
    }

    async.auto({
      tx: (next) => {
        QtumGateway.getRawTransaction(txid, 1, next);
      },
      vins: ['tx', (ret, next) => {
        this._getVins(ret.tx.vin, next);
      }],
      addresses: ['tx', 'vins', (ret, next) => {
        const addresses = [];
        const tx = ret.tx;
        let stopScriptPubKeyType = '';

        _.each(ret.vins, (vinEntry) => {
          addresses.push(vinEntry.address);
        });

        _.each(tx.vout, (voutEntry) => {
          if (voutEntry.scriptPubKey.type === 'pubkeyhash') {
            addresses.push(voutEntry.scriptPubKey.addresses[0]);
          }
          else if(!voutEntry.scriptPubKey.type || voutEntry.scriptPubKey.type === 'nulldata'){

          } else {
            stopScriptPubKeyType = `Unknown scriptPubKey type: ${voutEntry.scriptPubKey.type}`;
            return false;
            
          }
        });
        
        if(stopScriptPubKeyType){
          return next(stopScriptPubKeyType);
        }

        QtumAddressModel.find({
          where: QtumAddressModel.whereIn('address', addresses.length),
          params: addresses,
        }, next);
      }],
    }, (err, ret) => {
      if (err) {
        if (err.message && typeof err.message === 'string') {
          return callback(err.message);
        }

        return callback(err);
      }

      if (!ret.tx) {
        return callback(ErrorFactory.notFound(`Cannot find transaction: ${txid}`));
      }

      const tx = ret.tx;
      const entries = [];
      const addresses = _.keyBy(ret.addresses, 'address');

      _.each(ret.vins, (vinEntry) => {
        let valueSat = vinEntry.valueSat;
        if (valueSat === undefined || valueSat === null) {
          valueSat = vinEntry.value * Const.SATOSHI_FACTOR;
        }

        entries.push({
          address: vinEntry.address,
          value: -valueSat,
          valueString: (-valueSat).toString(),
          wallet: addresses[vinEntry.address] ? addresses[vinEntry.address].walletId : null
        });
      });

      _.each(tx.vout, (voutEntry) => {
        if (voutEntry.scriptPubKey.type === 'pubkeyhash') {
          let valueSat = voutEntry.valueSat;
          if (valueSat === undefined || valueSat === null) {
            valueSat = voutEntry.value * Const.SATOSHI_FACTOR;
          }

          entries.push({
            address: voutEntry.scriptPubKey.addresses[0],
            value: valueSat,
            valueString: valueSat.toString(),
            wallet: addresses[voutEntry.scriptPubKey.addresses[0]] ? addresses[voutEntry.scriptPubKey.addresses[0]].walletId : null
          })
        }
      });

      const response = {
        id: txid,
        date: moment.unix(tx.time).format(),
        time: tx.time,
        blocktime: tx.blocktime,
        blockHeight: tx.height,
        confirmations: tx.confirmations,
        entries: entries,
      };

      callback(null, response);
    });
  },

  _getVins: function (vins, callback) {
    // TODO: get the address and value from scriptSig, without requery qtumd here
    const result = [];
    const txs = {};

    function pushToResult (voutEntry) {
      if (voutEntry.scriptPubKey.type !== 'pubkeyhash') {
        throw new Error(`Unknown scriptPubKey type: ${voutEntry.scriptPubKey.type}`);
      }
      result.push({
        address: voutEntry.scriptPubKey.addresses[0],
        value: voutEntry.value,
      });
    }

    async.forEach(vins, (vinEntry, next) => {
      const txid = vinEntry.txid;
      const voutIndex = vinEntry.vout;
      if (txs[txid]) {
        pushToResult(txs[txid].vout[voutIndex]);
        return next(null, null);
      }

      QtumGateway.getRawTransaction(txid, 1, (err, tx) => {
        if (err) {
          return next(err);
        }

        txs[txid] = tx;
        pushToResult(txs[txid].vout[voutIndex]);
        return next(null, null);
      });
    }, (err) => {
      if (err) {
        return callback(err);
      }

      return callback(null, result);
    });
  },
});
