const _                       = require('lodash');
const async                   = require('async');
const moment                  = require('moment');
const Const                   = require('../common/Const');
const networkConfig           = require('../../config/eth');
const BigNumber               = require('bignumber.js');
const BaseCoinWalletService   = require('./BaseCoinWalletService');
const EthGateway              = require('../blockchain').EthGateway;
const ERC20ABI                  = require('../../config/eth/abi/erc20.json');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const Utils                   = require('sota-core').load('util/Utils');
const logger                  = require('sota-core').getLogger('EthWalletService');

const web3 = EthGateway.getLib();

module.exports = BaseCoinWalletService.extends({
  classname: 'EthWalletService',

  $_coinSymbol: Const.COIN.ETH,

  getGateway: function () {
    return EthGateway;
  },

  getWalletNetwork: function () {
    return process.env.ETH_NETWORK;
  },

  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  _createWithdrawalRecord: function (walletId, coin, address, amount, callback) {
    if (!web3.utils.isAddress(address)) {
      return callback(ErrorFactory.badRequest(`Invalid recipient address: ${address}`));
    }

    const EthWithdrawalModel = this.getModel('EthWithdrawalModel');
    const EthHotWalletModel = this.getModel('EthHotWalletModel');
    let reserve = null;
    let gasPrice = networkConfig.defaultGasPrice * 1e9; // Gas price is defined in gwei
    const gasLimit = networkConfig.defaultGasLimit;

    async.auto({
      hotWallet: (next) => {
        EthHotWalletModel.findOneOrFail(next)
      },

      gasPrice: (next) => {
        web3.eth.getGasPrice(next);
      },

      withdrawInfo: ["hotWallet", "gasPrice", (ret, next) => {
        reserve = web3.eth.accounts.privateKeyToAccount(ret.hotWallet.privateKey);
        if (reserve.address.toString().toLowerCase() === address.toLowerCase()) {
          return next(ErrorFactory.badRequest(`Non-sense withdraw: target is same as reserve address (${address})`));
        }
        gasPrice = ret.gasPrice;
        const withdrawInfo = {
          fromAddress: reserve.address.toString(),
          toAddress: address,
          amount: amount,
          tokenAddress: null,
          tokenSymbol: null,
          data: '',
          gasPrice: gasPrice,
          gasLimit: gasLimit,
        }
        return next(null, withdrawInfo);
      }],

      checkBalance: ['withdrawInfo', (ret, next) => {
        this.checkWithdrawBalanceInfo(ret.withdrawInfo, next);
      }],

      record: ['hotWallet', 'checkBalance', (ret, next) => {
        ret.withdrawInfo.txid = `TMP_WITHDRAWAL_TX_${reserve.address.toString()}_${Date.now().toString()}`;
        ret.withdrawInfo.walletId = walletId;
        ret.withdrawInfo.status = Const.WITHDRAW_STATUS.UNSIGNED;
        ret.withdrawInfo.txRaw = '';
        EthWithdrawalModel.add(ret.withdrawInfo, {
          isInsertIgnore: true
        }, next);
      }],
    }, (err, ret) => {
      if (err){
        return callback(err);
      }
      return callback(null, ret.record);
    });
  },

  _createWalletAddress: function (walletId, callback) {
    const EthAddressModel = this.getModel('EthAddressModel');
    async.auto({
      address: (next) => {
        let address = web3.eth.accounts.create();
        next(null, address);
      },
      saveaddress: ['address', (ret, next) => {
        EthAddressModel.add({
          walletId: walletId,
          privateKey: ret.address.privateKey.toString(),
          address: ret.address.address.toString()
        }, next);
      }],

    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, ret.saveaddress);
    });
  },

  getOneTransaction: function (txid, callback) {
    const WalletModel = this.getModel('WalletModel');
    const EthAddressModel = this.getModel('EthAddressModel');

    if (txid.match(/^TMP_WITHDRAWAL_TX_(.+)/)){
      return callback(null, {
        id: txid,
        date: '0000-00-00',
        time: 0,
        blocktime: 0,
        blockHeight: -1,
        confirmations: -1,
        entries: [],
      });
    }

    if (!txid.match(/^0x([A-Fa-f0-9]{64})$/)) {
      return callback(ErrorFactory.badRequest(`Invalid txid format: ${txid}`));
    }

    async.auto({
      tx: (next) => {
        web3.eth.getTransaction(txid, next);
      },
      receipt: (next) => {
        web3.eth.getTransactionReceipt(txid, next);
      },
      currentHeight: (next) => {
        web3.eth.getBlockNumber(next);
      },
      block: ['tx', (ret, next) => {
        if (!ret.tx) {
          return next(null, null);
        }

        const blockNumber = ret.tx.blockNumber;
        web3.eth.getBlock(blockNumber, next);
      }],
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (!ret.tx || !ret.block) {
        return callback(ErrorFactory.notFound(`Cannot find data of transaction ${txid}`));
      }

      const tx = ret.tx;
      const receipt = ret.receipt;
      const block = ret.block;
      const currentHeight = ret.currentHeight;

      if (receipt.status !== Const.TX_STATUS.SUCCESS) {
        return callback(null, {
          id: txid,
          status: 'failed'
        });
      }

      const senderAddr = tx.from;
      const receiverAddr = tx.to;
      const amount = parseInt(tx.value);

      if (isNaN(amount) || !amount) {
        return callback(ErrorFactory.badRequest(`Transaction ${txid} is not Ethereum transfer.`));
      }

      const conditions = {
        address: {
          $in: [senderAddr, receiverAddr]
        }
      };

      EthAddressModel.find(conditions, (err, addresses) => {
        if (err) {
          return callback(err);
        }

        const keyedAddresses = _.keyBy(addresses, 'address');
        const entries = [];
        entries.push({
          address: senderAddr,
          value: -amount,
          valueString: (-amount).toString(),
          wallet: keyedAddresses[senderAddr] ? keyedAddresses[senderAddr].walletId : null,
        });

        entries.push({
          address: receiverAddr,
          value: amount,
          valueString: (amount).toString(),
          wallet: keyedAddresses[receiverAddr] ? keyedAddresses[receiverAddr].walletId : null,
        });

        const response = {
          id: txid,
          date: moment.unix(block.timestamp).format(),
          blockHash: block.hash,
          blockHeight: block.number,
          confirmations: currentHeight - block.number,
          entries: entries,
        };

        return callback(null, response);
      });
    });
  },

  getCoinAddressBalance:  function (coin, address, callback) {
    async.auto({
      balance: (next) => {
        web3.eth.getBalance(address, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret.balance);
    });
  },

  checkWithdrawBalanceInfo: function (withdrawal, callback) {
    let balance = 0;
    async.auto({
      gasUsed: (next) => {
        web3.eth.estimateGas({
          from: withdrawal.fromAddress,
          to: withdrawal.toAddress,
          data: withdrawal.data,
        }).then((ret) => {
          return next(null, ret)
        }).catch((err) => {
          if (err) return next(err)
        });
      },

      checkEthToken: ["gasUsed", (ret, next) => {
        if (!withdrawal.tokenSymbol){
          return next(null, false);
        }
        const tokenConfig = Const.ERC20[withdrawal.tokenSymbol];
        const token = new web3.eth.Contract(ERC20ABI, tokenConfig.address);

        token.methods.balanceOf(withdrawal.fromAddress).call().then((tokenBalance) => {
          const contractDecimal = new BigNumber(10).exponentiatedBy(tokenConfig.decimal);
          const realBalance = contractDecimal.multipliedBy(tokenBalance.toString());
          if (!realBalance.isGreaterThanOrEqualTo(withdrawal.amount)) {
            logger.warn(`Reserve ${withdrawal.fromAddress} has insufficient balance: requested amount=${withdrawal.amount}, balance=${realBalance}`);
            return next(`Reserve ${withdrawal.fromAddress} has insufficient balance`);
          }
          logger.info("PASS CHECK token ----------> " + tokenBalance + " > " + withdrawal.amount)
          return next(null, true);
        }).catch((err) => next(err));
      }],

      check: ["checkEthToken", (ret, next) => {
        const totalCost = (new BigNumber(withdrawal.gasPrice)).multipliedBy(ret.gasUsed).plus(new BigNumber(ret.checkEthToken==false?withdrawal.amount:0));
        web3.eth.getBalance(withdrawal.fromAddress, (err, balanceRet) => {
          balance = new BigNumber(balanceRet.toString());
          if (totalCost.isGreaterThan(balance)) {
            return callback(ErrorFactory.badRequest(`Address ${withdrawal.fromAddress} has insufficient balance (${totalCost} > ${balance}).`));
          }
          logger.info("PASS CHECK coin  ----------> " + balanceRet + " > " +totalCost)
          return next(null, null);
        });
      }],
    }, (err, ret) => {
      if (err) return callback(err)
      return callback(null, true);
    })
  }

});
