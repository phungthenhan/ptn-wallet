const _                       = require('lodash');
const async                   = require('async');
const moment                  = require('moment');
const request                 = require('superagent');
const BigNumber               = require('bignumber.js');
const Const                   = require('../common/Const');
const ERC20ABI                = require('../../config/eth/abi/erc20.json');
const networkConfig           = require('../../config/eth');
const EthWalletService   = require('./EthWalletService');
const EthGateway              = require('../blockchain').EthGateway;
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const Utils                   = require('sota-core').load('util/Utils');
const logger                  = require('sota-core').getLogger('Erc20WalletService');

const web3 = EthGateway.getLib();

module.exports = EthWalletService.extends({
  classname: 'Erc20WalletService',
  _coinSymbol : Const.COIN.ETH,
  
  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  _createWithdrawalRecord: function (walletId, tokenName, address, amount, callback) {
    if (!web3.utils.isAddress(address)) {
      return callback(ErrorFactory.badRequest(`Invalid recipient address: ${address}`));
    }

    const Erc20WithdrawalModel = this.getModel('Erc20WithdrawalModel');
    const EthHotWalletModel = this.getModel('EthHotWalletModel');
    let reserve = null;
    let gasPrice = networkConfig.defaultGasPrice * 1e9; // Gas price is defined in gwei
    const gasLimit = networkConfig.defaultGasLimit;

    async.auto({
      hotWallet: (next) => {
        EthHotWalletModel.findOneOrFail(next)
      },

      gasPrice: (next) => {
        web3.eth.getGasPrice(next);
      },

      withdrawInfo: ["gasPrice", (ret, next) => {
        reserve = web3.eth.accounts.privateKeyToAccount(ret.hotWallet.privateKey);
        if (reserve.address.toString().toLowerCase() === address.toLowerCase()) {
          return next(ErrorFactory.badRequest(`Non-sense withdraw: target is same as reserve address (${address})`));
        }
        gasPrice = ret.gasPrice;
        const withdrawInfo = {
          fromAddress: reserve.address.toString(),
          toAddress: address,
          amount: amount,
          tokenAddress: Const.ERC20[tokenName].address,
          tokenSymbol: tokenName,
          data: '',
          gasPrice: gasPrice,
          gasLimit: gasLimit,
        }
        return next(null, withdrawInfo);
      }],

      checkBalance: ['withdrawInfo', (ret, next) => {
        this.checkWithdrawBalanceInfo(ret.withdrawInfo, next);
      }],

      record: ['hotWallet', 'checkBalance', (ret, next) => {
        ret.withdrawInfo.txid = 'ERC20Withdrawal' + reserve.address.toString() + Date.now().toString();
        ret.withdrawInfo.walletId = walletId;
        ret.withdrawInfo.status = Const.WITHDRAW_STATUS.UNSIGNED;
        ret.withdrawInfo.txRaw = '';
        Erc20WithdrawalModel.add(ret.withdrawInfo, {
          isInsertIgnore: true
        }, next);
      }],
    }, (err, ret) => {
      if (err){
        return callback(err);
      }
      return callback(null, ret.record);
    });
  },

  /**
   * GET WALLET DETAILS
   * This API call retrieves wallet object information by the wallet ID.
   * This is useful to get the balance of the wallet, or identify the keys
   * used to sign with the wallet using the Get Keychain API.
   */
  getOneWallet: function (tokenSymbol, walletId, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');

    async.auto({
      wallet: (next) => {
        WalletModel.findOneOrFail(walletId, next);
      },
      balance: (next) => {
        WalletBalanceModel.findOneOrCreate(walletId, tokenSymbol, next);
      }
    }, (err, ret) => {
      if (err) return callback(err);

      if (!ret.wallet) {
        return callback(ErrorFactory.notFound(`Cannot find wallet: ${walletId}`));
      }

      const wallet = ret.wallet.toJSON();
      if (wallet.coin !== Const.COIN.ETH) {
        return callback(ErrorFactory.notFound(`Wallet ${walletId} is not for ERC20 token.`));
      }

      const balance = ret.balance.balance;
      wallet.coin = tokenSymbol;
      wallet.balance = balance;
      wallet.balanceString = balance.toString();

      return callback(null, wallet);
    });
  },

  getOneTransaction: function (tokenName, txid, callback) {
    const WalletModel = this.getModel('WalletModel');
    const EthAddressModel = this.getModel('EthAddressModel');
    const tokenConfig = Const.ERC20[tokenName];

    async.auto({
      tx: (next) => {
        web3.eth.getTransaction(txid, next);
      },
      receipt: (next) => {
        web3.eth.getTransactionReceipt(txid, next);
      },
      currentHeight: (next) => {
        web3.eth.getBlockNumber(next);
      },
      block: ['tx', (ret, next) => {
        if (!ret.tx) {
          return next(null, null);
        }

        const blockNumber = ret.tx.blockNumber;
        web3.eth.getBlock(blockNumber, next);
      }],
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (!ret.tx || !ret.block) {
        return callback(ErrorFactory.notFound(`Cannot find data of transaction ${txid}`));
      }

      const tx = ret.tx;
      const receipt = ret.receipt;
      const block = ret.block;
      const currentHeight = ret.currentHeight;

      if (receipt.status !== true && receipt.status !== Const.TX_STATUS.SUCCESS) {
        return callback(null, {
          id: txid,
          status: 'failed'
        });
      }

      if (tokenConfig.address.toLowerCase() !== tx.to.toLowerCase()) {
        return callback(ErrorFactory.badRequest(`Transaction ${txid} is not transfer of token ${tokenName}.`));
      }

      const transferLog = _.find(receipt.logs, (log) => log.topics[0].toLowerCase() === networkConfig.eventTopics.erc20Transfer);

      if (!transferLog) {
        return callback(ErrorFactory.badRequest(`Cannot find transfer event of ${tokenName} in the transaction.`));
      }

      const senderAddr = web3.eth.abi.decodeParameter('address', transferLog.topics[1]);
      const receiverAddr = web3.eth.abi.decodeParameter('address', transferLog.topics[2]);
      const amount = web3.eth.abi.decodeParameter('uint256', transferLog.data);

      const conditions = {
        address: {
          $in: [senderAddr, receiverAddr]
        }
      };

      EthAddressModel.find(conditions, (err, addresses) => {
        if (err) {
          return callback(err);
        }

        const keyedAddresses = _.keyBy(addresses, 'address');
        const entries = [];
        entries.push({
          address: senderAddr,
          value: -amount,
          valueString: (-amount).toString(),
          wallet: keyedAddresses[senderAddr] ? keyedAddresses[senderAddr].walletId : null,
        });

        entries.push({
          address: receiverAddr,
          value: amount,
          valueString: (amount).toString(),
          wallet: keyedAddresses[receiverAddr] ? keyedAddresses[receiverAddr].walletId : null,
        });

        const response = {
          id: txid,
          date: moment.unix(block.timestamp).format(),
          blockHash: block.hash,
          blockHeight: block.number,
          confirmations: currentHeight - block.number,
          // fee: parseInt(receipt.gasUsed),
          // feeString: receipt.gasUsed.toString(),
          entries: entries,
        };

        return callback(null, response);
      });
    });
  },

  listCoinWithdrawals: function (tokenSymbol, walletId, pagination, callback) {
    const Erc20WithdrawalModel = this.getModel('Erc20WithdrawalModel');

    async.auto({
      withdrawals: (next) => {
        Erc20WithdrawalModel.find({
          where: 'wallet_id = ? AND token_symbol = ?',
          params: [walletId, tokenSymbol],
          orderBy: 'created_at DESC',
          limit: pagination.limit,
          offset: pagination.offset,
        }, next);
      },
      count: (next) => {
        Erc20WithdrawalModel.count({
          where: 'wallet_id = ? AND token_symbol = ?',
          params: [walletId, tokenSymbol],
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret);
    });
  },

  listCoinDeposits: function (tokenSymbol, walletId, pagination, callback) {
    const Erc20DepositModel = this.getModel('Erc20DepositModel');

    async.auto({
      deposits: (next) => {
        Erc20DepositModel.find({
          where: 'wallet_id = ? AND token_symbol = ?',
          params: [walletId, tokenSymbol],
          orderBy: 'block_timestamp DESC',
          limit: pagination.limit,
          offset: pagination.offset
        }, next);
      },
      count: (next) => {
        Erc20DepositModel.count({
          where: 'wallet_id = ? AND token_symbol = ?',
          params: [walletId, tokenSymbol],
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret);
    });
  },

  getCoinAddressBalance:  function (tokenName, address, callback) {
    // example contract address of KNC token: 0xfc23aace8eaabc1deb29619b61826f92ccdc4788
    const tokenConfig = Const.ERC20[tokenName];
    const tokenContract = new web3.eth.Contract(ERC20ABI, tokenConfig.address);

    async.auto({
      balance: (next) => {
        if (!web3.utils.isAddress(address)) {
          return next("Invalid address");
        }

        tokenContract.methods.balanceOf(address).call().then((ret) => {
          const balanceRet = new BigNumber(ret.toString());
          const contractDecimal = new BigNumber(10).exponentiatedBy(tokenConfig.decimal);
          const displayedBalance = balanceRet.dividedBy(contractDecimal).toNumber();
          return next(null, displayedBalance);
        }).catch((err) => next(err));
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, ret.balance);
    });

  },
});