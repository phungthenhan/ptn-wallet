const _                       = require('lodash')
const async                   = require('async')
const request                 = require('superagent')
const moment                  = require('moment')
const Neon                    = require('@cityofzion/neon-js').default;
const Const                   = require('../common/Const')
const getNeoRPCNode           = require('../../microservices/neo/getNeoRPCNode')
const BaseCoinWalletService   = require('./BaseCoinWalletService')
const ErrorFactory            = require('sota-core').load('error/ErrorFactory')
const logger                  = require('sota-core').getLogger('NeoWalletService')

const NEONode = getNeoRPCNode();

module.exports = BaseCoinWalletService.extends({
  classname: 'NeoWalletService',
  $_coinSymbol: Const.COIN.NEO,

  /**
   * Create wallet address from a Neo node.
   */
  _createWalletAddress: function(walletId, callback) {
    const NeoAddressModel = this.getModel('NeoAddressModel');

    async.auto({
      record: (next) => {
        const newAddress = Neon.create.account();
        const wif = Neon.get.WIFFromPrivateKey(newAddress._privateKey);
        const address = newAddress.address;
        const privateKey = newAddress._privateKey;
        const publicKey = newAddress._publicKey;
        const scriptHash = newAddress._scriptHash;

        NeoAddressModel.add({
          walletId: walletId,
          privateKey: privateKey,
          address: address,
          wif: wif,
          publicKey: publicKey,
          scriptHash: scriptHash,
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret.record);
    });
  },

  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  _createWithdrawalRecord: function(walletId, coin, address, amount, callback) {
    const NeoWithdrawalModel = this.getModel('NeoWithdrawalModel');
    const reserve = Neon.create.account(process.env.NEO_RESERVE_WALLET_PRIVATE_KEY);

    async.waterfall([
      (next) => {
        NEONode.getBalance(reserve.address, next);
      },
      (reserveBalance, next) => {
        let txTransaction = Neon.create.tx({type: 128});
        txTransaction
          .addOutput('NEO', amount, address)
          .addRemark(`Sent ${amount} NEO from ${reserve.address} to ${address}`)
          .calculate(reserveBalance)
          .sign(reserve.WIF);

        const txid = txTransaction.hash;
        const serializedTx = txTransaction.serialize();
        next(null, {txid: txid, serializedTx: serializedTx});
      },
      (rawTx, next) => {
        NeoWithdrawalModel.add({
          wallet_id: walletId,
          from_address: reserve.address.toString(),
          to_address: address,
          amount: amount,
          txid: rawTx.txid,
          status: Const.WITHDRAW_STATUS.SIGNED,
          tx_raw: rawTx.serializedTx,
        }, next);
      }
    ], callback);

  },

  /**
   * GET ONE WALLET TRANSFER
   * Wallet transfers represent digital currency sends and receives on your wallet.
   * This is the recommended method to track funding withdrawals and deposits.
   * TODO: Cache data to DB for better performance
   */
  getOneTransaction: function(txid, callback) {
    const WalletModel = this.getModel('WalletModel');
    const NeoAddressModel = this.getModel('NeoAddressModel');

    async.auto({
      tx: (next) => {
        NEONode.getRawTransaction(txid, next);
      },
      currentHeight: (next) => {
        NEONode.getBlockCount(next);
      },
      vintx: ['tx', (ret, next) => {
        let vins = [];
        const transactionInfo = ret.tx;

        async.forEachLimit(transactionInfo.vin, transactionInfo.vin.length, (transactionIn, _next) => {

          let inTxid = transactionIn.txid.slice(2);
          let inNumber = transactionIn.vout;
          NEONode.getRawTransaction(inTxid, (err, transaction) => {
            if (err) {
              return _next(err);
            }

            vins.push(transaction.vout[inNumber]);
            return _next(null, null);
          });
        }, (err) => {
          if (err) {
            return next(err);
          }

          return next(null, vins);
        });

      }],
      addresses: ['tx', (ret, next) => {
        const addresses = [];
        const tx = ret.tx;

        _.each(tx.vout, (voutEntry) => {
          addresses.push(voutEntry.address);
        });

        if (!tx || JSON.stringify(tx.vin) === 'null' || JSON.stringify(tx.vout) === 'null') {
          return callback(ErrorFactory.notFound(`Cannot find transaction: ${txid}`));
        }
        NeoAddressModel.find({
          where: NeoAddressModel.whereIn('address', addresses.length),
          params: addresses,
        }, next);
      }],
      block: ['tx', (ret, next) => {
        const tx = ret.tx;

        NEONode.getBlock(tx.blockhash, next);
      }],
    }, (err, ret) => {
      if (err) {
        if (err.message && typeof err.message === 'string') {
          return callback(err.message);
        }
        return callback(err);
      }

      if (!ret.tx || JSON.stringify(ret.tx.vin) === 'null' || JSON.stringify(ret.tx.vout) === 'null') {
        return callback(ErrorFactory.notFound(`Cannot find transaction: ${txid}`));
      }

      const tx = ret.tx;
      const block = ret.block;
      const vins = ret.vintx;
      const currentHeight = ret.currentHeight;
      const entries = [];
      const addresses = _.keyBy(ret.addresses, 'address');

      _.each(vins, (vinEntry) => {
        entries.push({
          address: vinEntry.address,
          value: vinEntry.value,
          valueString: vinEntry.value.toString(),
          wallet: addresses[vinEntry.address] ? addresses[vinEntry.address].walletId : null
        });
      });

      _.each(tx.vout, (voutEntry) => {
        entries.push({
          address: voutEntry.address,
          value: voutEntry.value,
          valueString: voutEntry.value.toString(),
          wallet: addresses[voutEntry.address] ? addresses[voutEntry.address].walletId : null
        });
      });

      const response = {
        id: txid,
        date: moment.unix(tx.blocktime).format(),
        time: tx.blocktime,
        blocktime: tx.blocktime,
        blockHeight: block.index,
        confirmations: tx.confirmations,
        entries: entries,
      };

      callback(null, response);
    });
  },

  _subtractBalance: function(withdrawalRecord, walletId, coin, amount, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');

    async.waterfall([
      (next) => {
        WalletBalanceModel.findOneOrFail(walletId, coin, next);
      },
      (wallet, next) => {
        if ( parseInt(wallet.balance) < parseInt(withdrawalRecord.amount) ) {
          return next(ErrorFactory.badRequest(`Insufficient wallet's balance.`));
        }
        const amount = withdrawalRecord.amount;
        WalletBalanceModel.addWithdrawal(withdrawalRecord, walletId, coin, amount, next);
      }
    ], callback);
  }
});
