const _                       = require('lodash');
const async                   = require('async');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const ethConfig               = require('../../config/eth');
const AppService              = require('./AppService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const logger                  = require('sota-core').getLogger('WalletService');

const erc20Tokens = _.map(ethConfig.tokens, 'symbol');

module.exports = AppService.extends({
  classname: 'WalletService',

  /**
   * LIST WALLETS
   * This API call lists all of a user’s wallets for a given coin.
   * This is useful when you have over 500 wallets as you will not be able to use the Web UI.
   */
  getListWallets: function (userId, coin, pagination, callback) {
    const WalletModel = this.getModel('WalletModel');

    WalletModel.find({
      where: 'user_id = ? AND coin = ?',
      params: [userId, coin],
      limit: pagination.limit,
      offset: pagination.offset
    }, (err, data) => {
      if (err) {
        return callback(err);
      }

      return callback(null, {
        wallets: data
      });
    });
  },

  /**
   * GENERATE WALLET
   * This API call creates a new wallet. Under the hood, the SDK (or Bitkoex-Wallet Express) does the following:
   * 1. Creates the user keychain locally on the machine, and encrypts it with the provided passphrase
        (skipped if userKey is provided).
   * 2. Ceates the backup keychain locally on the machine, and encrypts it with the provided passphrase
        (skipped if backupXpub or backupXpubProvider are provided).
   * 3. Uploads the encrypted user and backup keychains to Bitkoex-Wallet.
   * 4. Creates the Bitkoex-Wallet key (and the backup key if backupXpubProvider is set) on the service.
   * 5. Creates the wallet on Bitkoex-Wallet with the 3 public keys above.
   */
  createWallet: function (userId, coin, label, callback) {
    const WalletModel = this.getModel('WalletModel');
    const WalletBalanceModel = this.getModel('WalletBalanceModel');


    async.auto({
      wallet: (next) => {
        WalletModel.add({
          coin: coin,
          label: label,
          user_id: userId,
        }, next);
      },

      generateDelegateAddress: ['wallet', (ret, next) => {
        this._modelPrefix = coin.charAt(0).toUpperCase() + coin.slice(1);
        const CoinWalletService = this.getService(`${this._modelPrefix}WalletService`);
        CoinWalletService.createWalletAddress(ret.wallet.id, next);
      }],

      saveDelegateAddress: ['generateDelegateAddress', (ret, next) => {
        WalletModel.update({
          set: 'delegate_address = ?',
          where: 'id = ?' ,
          params: [ret.generateDelegateAddress.address, ret.wallet.id],
        }, next);
      }],

      balances: ['wallet', (ret, next) => {
        const data = [];
        data.push({
          wallet_id: ret.wallet.id,
          coin: coin,
        });

        WalletBalanceModel.add(data, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, ret.wallet);
    });
  },

  /**
   * GET WALLET DETAILS
   * This API call retrieves wallet object information by the wallet ID.
   * This is useful to get the balance of the wallet, or identify the keys
   * used to sign with the wallet using the Get Keychain API.
   */
  getOneWallet: function (coin, walletId, callback) {
    const WalletModel = this.getModel('WalletModel');

    async.auto({
      wallet: (next) => {
        WalletModel.findOne({
          where: 'id = ? AND coin = ?',
          params: [walletId, coin],
        }, next);
      }
    }, (err, ret) => {
      if (err) return callback(err);

      if (!ret.wallet) {
        return callback(ErrorFactory.notFound(`Cannot find wallet: ${walletId}`));
      }

      const wallet = ret.wallet.toJSON();
      return callback(null, ret.wallet);
    });
  },

  handleDeposit: function (walletId, coin, DepositModel, data, options, callback) {
    if (!_.isPlainObject(data)) {
      throw new Error(`Only can take plain object as parameter.`);
    }

    if (typeof options === 'function') {
      callback = options;
      options = null;
    }

    const WalletBalanceModel = this.getModel('WalletBalanceModel');
    const WalletLogModel = this.getModel('WalletLogModel');
    const amount = data.amount;

    async.waterfall([
      (next) => {
        DepositModel.findOne({
          where: 'txid = ?',
          params: [data.txid]
        }, (err, ret) => {
          if (err) {
            return callback(err);
          }

          // If transaction is processed before, just return
          // without add wallet balance and deposit record.
          if (ret) {
            logger.info(`Transaction ${data.txid} is processed before. Ignore updating wallet balance.`);
            return callback(null, null);
          }

          // Else continue
          return next(null, null);
        });
      },
      (ret, next) => {
        DepositModel.add(data, options, next);
      },
      (record, next) => {
        WalletBalanceModel.addDeposit(record, walletId, coin, amount, next);
      }
    ], callback);
  },


  createUnsaveWalletAddress: function (walletId, coin, callback) {
    this._modelPrefix = coin.charAt(0).toUpperCase() + coin.slice(1);
    const CoinWalletService = this.getService(`${this._modelPrefix}WalletService`);
    const gateway = CoinWalletService.getGateway();
    const privateKey = gateway.getLib().PrivateKey(null, gateway.getNetwork());
    const address = privateKey.toAddress();
    return callback(null, {address: address.toString(), privateKey: privateKey.toString()});
  }
});
