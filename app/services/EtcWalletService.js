const async                   = require('async');
const request                 = require('superagent');
const RpcClient               = require('@dashevo/dashd-rpc');
const Const                   = require('../common/Const');
const BaseCoinWalletService   = require('./BaseCoinWalletService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const Paginator2              = require('sota-core').load('util/Paginator2');
const logger                  = require('sota-core').getLogger('EtcWalletService');

module.exports = BaseCoinWalletService.extends({
  classname: 'EtcWalletService',

  $_coinSymbol: Const.COIN.ETC,

  /**
   * CREATE WALLET ADDRESS
   * This API call is used to create a new receive address for your wallet, which enhances your privacy.
   * You may choose to call this API whenever a deposit is made.
   * The Bitkoex-Wallet API supports millions of addresses.
   * Please check the “Coin-Specific Implementation” with regards to fee address management for ethereum.
   */
  createWalletAddress: function (walletId, callback) {
    callback(`Does not support to create ETC address yet`);
  },

  /**
   * GET WALLET ADDRESS
   * This API call is used to get information about a single wallet address.
   * The address object can be fetched by using either the id or the address itself.
   */
  getOneWalletAddress: function (address, walletId, callback) {
    const EtcAddressModel = this.getModel('EtcAddressModel');
    const WalletModel = this.getModel('WalletModel');

    async.waterfall([
      (next) => {
        EtcAddressModel.findOne({
          where: 'address = ?',
          params: [address]
        }, next);
      },
      (add, next) => {
        if (!add) {
          return next(ErrorFactory.notFound(`Cannot find address: ${address}`));
        }

        if (add.walletId !== walletId) {
          return next(ErrorFactory.badRequest(`Address [${address}] does not belong to wallet [${walletId}]`));
        }

        return next(null, add);
      }
    ], callback);
  }
});
