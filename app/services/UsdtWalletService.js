
const Const                   = require('../common/Const');
const _                       = require('lodash');
const async                   = require('async');
const BaseCoinWalletService   = require('./BaseCoinWalletService');
const getUsdtRPCNode          = require('../../microservices/usdt/getUsdtRPCNode');
const bitcore                 = require('bitcore-lib');
const moment                  = require('moment');
const util = require('util');

const USDTNode = getUsdtRPCNode();

module.exports = BaseCoinWalletService.extends({
  classname: 'UsdtWalletService',

  $_coinSymbol: Const.COIN.USDT,

  _createWalletAddress : function (walletId, callback) {
    const UsdtAddressModel = this.getModel('UsdtAddressModel');

    async.auto({
      address: (next) => {
        USDTNode.getNewAddress(next);
      },

      dumpKey : ['address', (ret, next) => {
        USDTNode.dumpPrivKey(ret.address[0], (err, wif) => {
          if (err) {
            return next(err);
          }
          const privateKey = bitcore.PrivateKey.fromWIF(wif);
          next(null, {
            privateKey: privateKey.toString(),
            address: ret.address[0],
          });
        });
      }],

      record : [ 'dumpKey', (ret, next) => {
        UsdtAddressModel.add({
          walletId: walletId,
          address: ret.dumpKey.address,
          privateKey: ret.dumpKey.privateKey,
        }, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, ret.record);
    });
  },

  _createWithdrawalRecord : function (walletId, tokenCoin, address, amount, callback) {

    const UsdtWithdrawalModel = this.getModel('UsdtWithdrawalModel');
    const WalletWebhookService = this.getService('WalletWebhookService');
    const USDT_PROPERTY = 2;
    let realAmount = parseInt(amount) / Const.SATOSHI_FACTOR;

    let privateKey = bitcore.PrivateKey.fromString(process.env.USDT_RESERVE_WALLET_PRIVATE_KEY);
    let withdrawalAddress = privateKey.toAddress(process.env.USDT_NETWORK=='mainnet'
      ?bitcore.Networks.mainnet
      :bitcore.Networks.testnet);

    async.auto({
      checkBalance: (next) => {
        this.getAddressBalance(tokenCoin, withdrawalAddress.toString(), (err, ret) => {
          if (err) {
            return next(err);
          }
          const convertBalance = ret.balance;

          if (amount > convertBalance) {
            return next(`Address ${withdrawalAddress.toString()} have not insufficient balance`);
          }
          next(null, null);
        });
      },

      changeAddress : ['checkBalance', (ret, next) => {
        return next(null, withdrawalAddress.toString());
      }],

      payloadSend : ['checkBalance', (ret, next) => {
        USDTNode.callRpc('omni_createpayload_simplesend', [USDT_PROPERTY, realAmount]).then((ret) => {
          next(null, ret);
        });
      }],

      listUnspent : ['changeAddress', (ret, next) => {
        USDTNode.callRpc('listunspent', [Const.MIN_CONFIRMATIONS, Const.MAX_CONFIRMATIONS, [ret.changeAddress]])
          .then((unspents) => {
            _.each(unspents, (unspent) => {
              unspent.value = unspent.amount;
            });
            next(null, unspents);
          });
      }],

      rawtx : ['listUnspent', (ret, next) => {

        USDTNode.callRpc('createrawtransaction', [ret.listUnspent, {}]).then((ret) => {
          next(null, ret);
        });

      }],

      rawtxOpreturn : ['payloadSend', 'rawtx', (ret, next) => {

        USDTNode.callRpc('omni_createrawtx_opreturn', [ret.rawtx, ret.payloadSend]).then((ret) => {
          next(null, ret);
        });
      }],

      rawtxReference : ['rawtxOpreturn', (ret, next) => {
        
        USDTNode.callRpc('omni_createrawtx_reference', [ret.rawtxOpreturn, address, realAmount ]).then((ret) => {
          next(null, ret);
        });
      }],

      rawtxChange : ['rawtxReference', 'listUnspent', (ret, next) => {

        const length =  ret.rawtxReference.length;
        const fee = Math.ceil(Const.FEE_PER_KB * length / 1024) / Const.SATOSHI_FACTOR;

        USDTNode.callRpc('omni_createrawtx_change', [ret.rawtxReference, ret.listUnspent, ret.changeAddress, fee])
          .then((ret) => {
            next(null, ret);
          });
      }],

      sign: ['rawtxChange', (ret, next) => {
        USDTNode.callRpc('signrawtransaction', [ret.rawtxChange]).then((ret) => {
          next(null, ret);
        });
      }],

      decodeTx: ['rawtxChange', (ret, next) => {
        USDTNode.callRpc('omni_decodetransaction', [ret.rawtxChange]).then((ret) => {
          next(null, ret);
        });
      }],

      record: ['sign', 'decodeTx', (ret, next) => {
        UsdtWithdrawalModel.add({
          wallet_id: walletId,
          from_address: ret.changeAddress,
          to_address: address,
          amount: amount,
          txid: ret.decodeTx.txid,
          status: Const.WITHDRAW_STATUS.SIGNED,
          tx_raw: ret.sign.hex,
        }, next);
      }],
      webhook: ['record' , (ret, next) => {
        // register wallet webhook when user withdraw somethin'
        WalletWebhookService.createTxChangedWebhookProgress(tokenCoin, walletId, ret.decodeTx.txid,null, next);
      }],

    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, ret.record);
    });
  },

  getOneTransaction : function (txid, callback) {
    const UsdtAddressModel = this.getModel('UsdtAddressModel');
    async.auto({
      // get raw information of transaction
      tx : (next) => {
        USDTNode.omniGetTransaction(txid,  (err, ret) => {
          return next(err, ret);
        });
      },

      txInfo : ['tx', (ret, next) => {
        const tx = ret.tx;
        const entries = [];

        const senderAddr = ret.tx.sendingaddress;
        const receiverAddr = ret.tx.referenceaddress;

        const conditions = {
          address: {
            $in: [senderAddr, receiverAddr]
          }
        };

        UsdtAddressModel.find(conditions, (err, addresses) => {
          if (err) {
            return callback(err);
          }
          const keyedAddresses = _.keyBy(addresses, 'address');
          // send
          entries.push({
            address: ret.tx.sendingaddress,
            value: -( ret.tx.amount * Const.SATOSHI_FACTOR ),
            valueString: `${ret.tx.amount * Const.SATOSHI_FACTOR}`,
            wallet: keyedAddresses[senderAddr]?keyedAddresses[senderAddr].walletId: null,
          });

          // receive
          entries.push({
            address: ret.tx.referenceaddress,
            value: ret.tx.amount * Const.SATOSHI_FACTOR,
            valueString: `${ret.tx.amount * Const.SATOSHI_FACTOR}`,
            wallet: keyedAddresses[receiverAddr]?keyedAddresses[receiverAddr].walletId: null,
          });

          tx.entries = entries;
          next(null, tx);
        });
      }]
    }, (err, ret) => {
      if (err) {
        if (err.message && typeof err.message === 'string') {
          return callback(err.message);
        }
        return callback(err);
      }

      if (!ret.tx) {
        return callback(ErrorFactory.notFound(`Cannot find transaction: ${txid}`));
      }

      const response = {
        id: ret.txInfo.txid ,
        date: moment.unix(ret.txInfo.blocktime).format(),
        type: ret.txInfo.type,
        propertyid: ret.txInfo.propertyid,
        time: ret.txInfo.blocktime,
        blocktime: ret.txInfo.blocktime,
        blockHeight: ret.txInfo.block,
        confirmations: ret.txInfo.confirmations,
        entries: ret.txInfo.entries,
      };

      callback(null, response);
    });
  },

  getCoinAddressBalance:  function (coin, address, callback) {
    async.auto({
      balance: (next) => {
        // example address: yN3DoZ8QDedfPzR82MYivpyMZ1Waz4RYKq
        USDTNode.omniGetBalance(address, process.env.USDT_NETWORK=='testnet'?2:1, next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      let convertBalance = ret.balance[0].balance * Const.SATOSHI_FACTOR
      return callback(null, convertBalance);
    });
  }
});