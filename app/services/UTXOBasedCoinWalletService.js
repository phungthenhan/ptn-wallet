const _                       = require('lodash');
const async                   = require('async');
const moment                  = require('moment');
const Const                   = require('../common/Const');
const BaseCoinWalletService   = require('./BaseCoinWalletService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const logger                  = require('sota-core').getLogger('BaseCoinWalletService');
const DashGateway             = require('../blockchain').DashGateway;
const BigNumber               = require('bignumber.js');

module.exports = BaseCoinWalletService.extends({
  classname: 'UTXOBasedCoinWalletService',

  $_coinSymbol: null, // Must be defined in derived class

  _createWalletAddress: function (walletId, callback) {
    const coincore = this.getGateway().getLib();
    const privateKey = new coincore.PrivateKey(null, this.getWalletNetwork());
    const address = privateKey.toAddress();

    const CoinAddressModel = this.getModel(`${this._coinSymbol.charAt(0).toUpperCase() + this._coinSymbol.slice(1)}AddressModel`);
    CoinAddressModel.add({
      walletId: walletId,
      privateKey: privateKey.toString(),
      wif: privateKey.toWIF(),
      address: address.toString(),
      network: privateKey.network.toString()
    }, callback);
  },

  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  _createWithdrawalRecord: function (walletId, coin, address, amountSats, callback) {
    const Coin = coin.charAt(0).toUpperCase() + coin.slice(1).toLowerCase();
    const CoinWithdrawalModel = this.getModel(`${Coin}WithdrawalModel`);
    const CoinHotWalletModel = this.getModel(`${Coin}HotWalletModel`);
    const gateway = this.getGateway();
    const coincore = gateway.getLib();

    if (!coincore.Address.isValid(address, gateway.getNetwork())) {
      return callback(ErrorFactory.badRequest(`Invalid address: ${address}`));
    }

    async.waterfall([

      (next) => {
        CoinHotWalletModel.findOneOrFail(next);
      },

      (hotWallet, next) => {
        this.prebuildWithdrawalData(walletId, address, amountSats, hotWallet, next);
      },

      (data, next) => {
        CoinWithdrawalModel.add(data, next);
      }

    ], callback);
  },

  buildWithdrawalData: function (walletId, address, amountSats, hotWallet, callback){
    const gateway = this.getGateway();
    const coincore = gateway.getLib();
    amountSats = parseInt(amountSats);

    async.auto({

      build: (next) => {
        this.prebuildWithdrawalData(walletId, address, amountSats, hotWallet, next);
      },

      info: (next) => {
        gateway.getRPCNodeInfo(next);
      },

      utxos:  ( next) => {
        gateway.getAddressUtxos(hotWallet.address, next);
      },

    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const buildData = ret.build;
      const relayFee = ret.info.relayfee * Const.SATOSHI_FACTOR;
      const hotAddress = hotWallet.address;
      const hotPrivateKey = hotWallet.privateKey;
      const utxos = ret.utxos;
      const inputs = [];
      const estimatedTotalAmount = amountSats + relayFee;
      let totalInput = 0;

      for (let i = 0; i < utxos.length; i++) {
        const utxo = utxos[i];
        inputs.push(new coincore.Transaction.UnspentOutput(utxo));
        totalInput += parseInt(utxo.amountSat);
        if (totalInput > estimatedTotalAmount) {
          break;
        }
      }


      let tx = new coincore.Transaction()
        .from(inputs)
        .to(address, amountSats)
        .change(hotAddress)
        .sign(hotPrivateKey);

      const len = tx.serialize().length;
      let calculatedFee = Math.ceil(coincore.Transaction.FEE_PER_KB * len / 1024);
      if (calculatedFee < relayFee) calculatedFee += relayFee;
      tx = tx.fee(calculatedFee).sign(hotPrivateKey);

      buildData.fee = calculatedFee;
      buildData.txid = tx.hash.toString();
      buildData.tx_raw = tx.serialize();

      return callback(null, buildData);
    });
  },

  prebuildWithdrawalData: function (walletId, address, amountSats, hotWallet, callback){
    const tempFee = 0.000005;
    return callback(null, {
      wallet_id: walletId,
      from_address: hotWallet.address,
      to_address: address,
      amount: amountSats,
      fee: tempFee * Const.SATOSHI_FACTOR, // change
      txid: `TMP_WITHDRAWAL_TX_${hotWallet.address}_${Date.now().toString()}`, // change
      status: Const.WITHDRAW_STATUS.UNSIGNED,
      tx_raw: 'TEMP_TRANSACTION_RAW', // change
    });
  },

  checkWithdrawBalanceInfo: function (withdrawal, callback){
    async.waterfall([
      (next) => {
        this.getAddressBalance(this._coinSymbol, withdrawal.fromAddress, next);
      },
      (senderBalance, next) => {
        const realBalance = new BigNumber(senderBalance);
        const amount = new BigNumber(withdrawal.amount);
        if (amount.isGreaterThan(realBalance)){
          return next(`Insufficient reserve's balance. (${realBalance} < ${amount})`);
        }
        return next(null, true);
      }
    ], callback)
  },

  getCoinAddressBalance: function (coin, address, callback) {
    const gateway = this.getGateway();
    gateway.getAddressBalance(address, callback);
  }

});