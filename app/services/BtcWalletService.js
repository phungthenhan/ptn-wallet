const _                           = require('lodash');
const async                       = require('async');
const request                     = require('superagent');
const util                        = require('util');
const moment                      = require('moment');
const Const                       = require('../common/Const');
const BtcGateway                  = require('../blockchain').BtcGateway;
const UTXOBasedCoinWalletService  = require('./UTXOBasedCoinWalletService');
const ErrorFactory                = require('sota-core').load('error/ErrorFactory');
const logger                      = require('sota-core').getLogger('BtcWalletService');

module.exports = UTXOBasedCoinWalletService.extends({
  classname: 'BtcWalletService',

  $_coinSymbol: Const.COIN.BTC,

  getGateway: function () {
    return BtcGateway;
  },

  getWalletRPCHost: function () {
    return process.env.BTC_WALLET_RPC_HOST;
  },

  getWalletNetwork: function () {
    return process.env.BTC_NETWORK;
  },

  /**
   * GET ONE WALLET TRANSFER
   * Wallet transfers represent digital currency sends and receives on your wallet.
   * This is the recommended method to track funding withdrawals and deposits.
   * TODO: Cache data to DB for better performance
   */
  getOneTransaction: function (txid, callback) {
    const WalletModel = this.getModel('WalletModel');
    const BtcAddressModel = this.getModel('BtcAddressModel');

    if (txid.match(/^TMP_WITHDRAWAL_TX_(.+)/)){
      return callback(null, {
        id: txid,
        date: '0000-00-00',
        time: 0,
        blocktime: 0,
        blockHeight: -1,
        confirmations: -1,
        entries: [],
      });
    }

    if (!txid.match(/^([A-Fa-f0-9]{64})$/)) {
      return callback(ErrorFactory.badRequest(`Invalid txid format: ${txid}`));
    }

    async.auto({
      tx: (next) => {
        BtcGateway.getRawTransaction(txid, 1, next);
      },
      addresses: ['tx', (ret, next) => {
        const addresses = [];
        const tx = ret.tx;
        let stopScriptPubKeyType = '';

        _.each(tx.vin, (vinEntry) => {
          addresses.push(vinEntry.address);
        });

        _.each(tx.vout, (voutEntry) => {
          if (voutEntry.scriptPubKey.type === 'pubkeyhash' || voutEntry.scriptPubKey.type === 'scripthash') {
           addresses.push(voutEntry.scriptPubKey.addresses[0]);
         }
         else if(!voutEntry.scriptPubKey.type || voutEntry.scriptPubKey.type === 'nulldata'){
          
        } else {
          stopScriptPubKeyType = `Unknown scriptPubKey type: ${voutEntry.scriptPubKey.type}`;
          return false;
        }
      });

        if(stopScriptPubKeyType){
          return next(stopScriptPubKeyType);
        }

        BtcAddressModel.find({
          where: BtcAddressModel.whereIn('address', addresses.length),
          params: addresses,
        }, next);
      }],
    }, (err, ret) => {
      if (err) {
        if (err.message && typeof err.message === 'string') {
          return callback(err.message);
        }

        return callback(err);
      }

      if (!ret.tx) {
        return callback(ErrorFactory.notFound(`Cannot find transaction: ${txid}`));
      }

      const tx = ret.tx;
      const entries = [];
      const addresses = _.keyBy(ret.addresses, 'address');

      _.each(tx.vin, (vinEntry) => {
        entries.push({
          address: vinEntry.address,
          value: -vinEntry.valueSat,
          valueString: (-vinEntry.valueSat).toString(),
          wallet: addresses[vinEntry.address] ? addresses[vinEntry.address].walletId : null
        });
      });

      _.each(tx.vout, (voutEntry) => {
        if (voutEntry.scriptPubKey.type === 'pubkeyhash' || voutEntry.scriptPubKey.type === 'scripthash') {
          entries.push({
          address: voutEntry.scriptPubKey.addresses[0],
          value: voutEntry.valueSat,
          valueString: voutEntry.valueSat.toString(),
          wallet: addresses[voutEntry.scriptPubKey.addresses[0]] ? addresses[voutEntry.scriptPubKey.addresses[0]].walletId : null
          })
        }
      });

      const response = {
        id: txid,
        date: moment.unix(tx.time).format(),
        time: tx.time,
        blocktime: tx.blocktime,
        blockHeight: tx.height,
        confirmations: tx.confirmations,
        entries: entries,
      };

      callback(null, response);
    });
  },
});