const _                       = require('lodash');
const async                   = require('async');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const AppService              = require('./AppService');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const Paginator2              = require('sota-core').load('util/Paginator2');
const Utils                   = require('sota-core').load('util/Utils');
const logger                  = require('sota-core').getLogger('WalletWebhookService');

module.exports = AppService.extends({
  classname: 'WalletWebhookService',

  /**
   * ADD A WALLET WEBHOOK
   * Add a webhook that will result in an HTTP callback at the specified URL from Bitkoex-Wallet when events are triggered.
   * There is a limit of 10 Webhooks of each type per wallet.
   * Types of wallet webhooks available:
   * - Transfer webhooks will fire on any transfer on the wallet.
   * - Pending approval webhooks will fire when an event triggers policy on the wallet (e.g send transaction,
       user change, policy change, pending approval updated).
   * - Address confirmation webhooks will fire when an address has been initialized on the wallet.
   */
  createWalletWebhook: function (params, callback) {
    const coin = params.coin;
    const walletId = params.walletId;
    const type = params.type;
    const numConfirmations = params.numConfirmations || 0;
    const url = params.url;
    const label = params.label || 'Wallet webhook';

    const WalletWebhookModel = this.getModel('WalletWebhookModel');
    WalletWebhookModel.add({
      coin: coin,
      wallet_id: walletId,
      type: type,
      num_confirmations: numConfirmations,
      label: label,
      url: url,
    }, callback);
  },

  /**
   * LIST WALLET WEBHOOKS
   * List all webhooks set up on the wallet.
   * Currently, the types of webhooks that can be attached to a wallet are transfer notifications.
   */
  getListWalletWebhooks: function (walletId, coin, callback) {
    const WalletWebhookModel = this.getModel('WalletWebhookModel');

    WalletWebhookModel.find({
      where: 'wallet_id = ? AND coin = ?',
      params: [walletId, coin],
      limit: 1000
    }, (err, data) => {
      if (err) {
        return callback(err);
      }

      return callback(null, {
        webhooks: data
      });
    });
  },

  /**
   * REMOVE A WALLET WEBHOOK
   * Removing a webhook will cause new events of the specified type to no longer trigger HTTP callbacks to your URLs.
   */
  removeWalletWebhook: function (params, callback) {
    const coin = params.coin;
    const walletId = params.walletId;
    const type = params.type;
    const url = params.url;

    const WalletWebhookModel = this.getModel('WalletWebhookModel');
    WalletWebhookModel.remove({
      where: 'coin = ? AND wallet_id = ? AND type = ? AND url = ?',
      params: [coin, walletId, type, url]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, {
        success: 1
      });
    });
  },

  createMultiWalletDepositWebhookProgress: function (coin, walletIds, txid, callback) {
    async.each(walletIds, (walletId, next) => {
      this.createWalletDepositWebhookProgress(coin, walletId, txid, next);
    }, callback);
  },

  createWalletDepositWebhookProgress: function (coin, walletId, txid, callback) {
    const WalletWebhookModel = this.getModel('WalletWebhookModel');
    const WalletWebhookProgressModel = this.getModel('WalletWebhookProgressModel');

    async.auto({
      webhooks: (next) => {
        WalletWebhookModel.find({
          where: 'coin = ? AND wallet_id = ? AND type = ?',
          params: [coin, walletId, Const.WEBHOOK_TYPE.TRANSFER],
        }, next);
      },
      add: ['webhooks', (ret, next) => {
        const webhooks = ret.webhooks;
        if (!webhooks.length) {
          return next(null, null);
        }

        const data = _.map(webhooks, (elem) => ({
            webhook_id: elem.id,
            txid: txid,
            last_try: Utils.nowInMilis(),
          }));
        WalletWebhookProgressModel.add(data, { isInsertIgnore: true }, next);
      }],
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, true);
    });
  },


  createTxChangedWebhookProgress: function (coin, walletId, txid, dataObject, callback) {
    const WalletWebhookModel = this.getModel('WalletWebhookModel');
    const WalletWebhookProgressModel = this.getModel('WalletWebhookProgressModel');
    const TxChangeLogModel = this.getModel('TxChangeLogModel');
    dataObject.coin = coin;

    async.auto({
      webhooks: (next) => {
        WalletWebhookModel.find({
          where: 'coin = ? AND wallet_id = ? AND type = ?',
          params: [coin, walletId, Const.WEBHOOK_TYPE.TXCHANGED],
        }, next);
      },
      process: ['webhooks', (ret, next) => {
        const webhooks = ret.webhooks;
        if (!webhooks.length) {
          return next(null, null);
        }

        const webHookInfos = _.map(webhooks, (elem) => ({
            webhook_id: elem.id,
            data: null,
            last_try: Utils.nowInMilis(),
          }));

        async.each(webHookInfos, (webHookInfo, _next) => {
          async.waterfall([
            (__next) => {
              WalletWebhookProgressModel.appendTxChangeProgress(webHookInfo, dataObject, __next);
            },
          ], _next)
        }, next)
      }],
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }
      return callback(null, true);
    });
  },

});