const _                         = require('lodash');
const async                     = require('async');
const BaseGateway               = require('../BaseGateway');
const Const                     = require('../../common/Const');
const ERC20ABI                  = require('../../../config/eth/abi/erc20.json');
const BigNumber                 = require('bignumber.js');
const ErrorFactory              = require('sota-core').load('error/ErrorFactory');
const logger                    = require('sota-core').getLogger('QtumGateway');

const httpProviders = {
  rinkeby: 'https://rinkeby.infura.io/ioANs615ip3V5aTNKUA8',
  mainnet: 'https://mainnet.infura.io/ioANs615ip3V5aTNKUA8',
};

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(httpProviders[process.env.ETH_NETWORK]));
const formatters = require('web3-core-helpers').formatters;
web3.extend({
  methods: [{
    name: 'getLogs',
    call: 'eth_getLogs',
    params: 1,
    inputFormatter: [formatters.inputLogFormatter],
    outputFormatter: formatters.outputLogFormatter
  }]
});

class EthGateway extends BaseGateway {

  constructor () {
    super();
  }

  getLib () {
    return web3;
  }

  /**
   * Get WEB3 RPC client to connect to a full node
   */
  getRPCClient () {
    return web3;
  }

  getAPIEndpoint () {
    return null;
  }

  getBlockInfo (blockNumber, callback) {
    async.auto({
      block: (next) => {
        web3.eth.getBlock(blockNumber, next);
      },
      latestBlockNumber: (next) => {
        web3.eth.getBlockNumber(next);
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const block = ret.block;
      const latestBlockNumber = ret.latestBlockNumber;

      if (!block) {
        logger.error(`Something went wrong. Cannot get block: ${blockNumber}`);
        return callback(`Could not get block ${blockNumber}. Wait for the next tick...`);
      }

      if (!latestBlockNumber) {
        logger.error(`Something went wrong. Cannot get latest block number on the network`);
        return callback(`Could not get latest block number. Wait for the next tick...`);
      }

      return callback(null, {
        number: block.number,
        hash: block.hash,
        timestamp: block.timestamp,
        confirmations: ret.latestBlockNumber - block.number,
      });
    });
  }

  getTransactionInfo (txid, callback) {
    async.auto({
      tx: (next) => {
        web3.eth.getTransaction(txid, next);
      },
      receipt: (next) => {
        web3.eth.getTransactionReceipt(txid, next);
      },
      block: ['tx', (ret, next) => {
        if (!ret.tx || !ret.tx.blockNumber) {
          logger.info(`Still could not get full of tx info: ${txid}`);
          return next(null, null);
        }
        this.getBlockInfo(ret.tx.blockNumber, next)
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const tx = ret.tx;
      const receipt = ret.receipt;

      if (!tx) {
        logger.info(`Still could not get tx info: ${txid}`);
        return callback(Const.ASYNC_BREAKER);
      }

      if (!receipt) {
        logger.info(`Still could not get tx receipt: ${txid}`);
        return callback(Const.ASYNC_BREAKER);
      }

      if (typeof receipt.status === 'boolean') {
        receipt.status = receipt.status ? Const.TX_STATUS.SUCCESS : Const.TX_STATUS.FAILED;
      }

      if (receipt.status !== Const.TX_STATUS.FAILED && receipt.status !== Const.TX_STATUS.SUCCESS) {
        return callback(`Transaction ${txid} has invalid receipt status: ${receipt.status}`);
      }

      return callback(null, {
        confirmations: ret.block.confirmations,
        blockHash: tx.blockHash,
        timestamp: ret.block.timestamp,
        blockNumber: tx.blockNumber,
        status: receipt.status,
        fee: receipt.gasUsed * tx.gasPrice,
      });
    });
  }


  resignTransaction (withdrawal, reserve, callback) {
    let toAddress = withdrawal.toAddress;
    let dataRaw = null;
    let value = withdrawal.amount;
    if (withdrawal.tokenSymbol){
      const tokenConfig = Const.ERC20[withdrawal.tokenSymbol];
      const token = new web3.eth.Contract(ERC20ABI, tokenConfig.address);
      toAddress = tokenConfig.address;
      dataRaw = token.methods.transfer(withdrawal.toAddress, withdrawal.amount).encodeABI();
      value = 0;
    }
    web3.eth.accounts.signTransaction({
      from: withdrawal.fromAddress,
      to: toAddress,
      value: value,
      data: dataRaw,
      gasPrice: withdrawal.gasPrice,
      gas: withdrawal.gasLimit,
    }, reserve.privateKey.toString(), callback);
  }
}

module.exports = EthGateway;