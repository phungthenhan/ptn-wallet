const _             = require('lodash');
const async         = require('async');
const logger        = require('sota-core').getLogger('BaseGateway');

/**
 * The gateway provides methods/interfaces for our service
 * to connect to blockchain network
 * The method will be implemented in derived classes
 * They can be done via RPC calls, RESTful APIs, ...
 */

class BaseGateway {

  /**
   * Get a hex-encoded serialized transaction or a JSON object describing the transaction.
   *
   * @param {String} txid: The transaction hash (id) that we want to track
   * @param {Number} verbosity: The format of result
   *   - Verbosity is 0: return hex-encoded transaction content
   *   - Verbosity is 1: return transaction in JSON
   * @returns {Mixed}:
   *   - Verbosity is 0: return String
   *   - Verbosity is 1: return Object
   */
  getRawTransaction (txid, verbosity, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Get one transaction object
   *
   * @params {String} txid: the transaction hash
   * @returns {BaseTransaction} tx: the transaction details
   * TODO: unify format of raw transaction details object
   */
  getOneTransaction (txid, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Returns transactions with given txids
   *
   * @ param {Array} txids: the array of transaction hashes/ids
   * @ returns {Array} txs: the array of detailed transactions
   */
  getTransactionsByIds (txids, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Returns the header hash of a block
   * at the given height in the local best block chain
   *
   * @ param {Number} height: the height index/block number
   * @ returns {String} hash: the header hash of the block we're looking for
   */
  getBlockHash (height, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * The getblock RPC gets a block with a particular header hash
   * from the local block database either as a JSON object or as a serialized block.
   *
   * @param {String} blockHash: The block hash (or block number in case the parameter is Number)
   * @param {Number} verbosity: The format of result
   *   - Verbosity is 0: result is a string, that is serialized/hex-encoded block data
   *   - Verbosity is 1: result is an Object with basic information about block
   *   - Verbosity is 2: result is an Object with information about the block and all transactions
   */
  getRawBlock (blockHash, verbosity, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Get block details in application-specified format
   *
   * @param {String} blockHash: the block hash (or block number in case the parameter is Number)
   * @param {Number} verbosity: The format of result
   *   - Verbosity is 0: result is a string, that is serialized/hex-encoded block data
   *   - Verbosity is 1: result is an Object with basic information about block
   * @returns {BaseBlock} block: the block detail
   * TODO: unify format of block detail object
   */
  getOneBlock (blockHash, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * No param
   * Returns the number of blocks in the local best block chain.
   */
  getBlockCount (callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Returns all transactions in givent block.
   *
   * @params {String|Number} blockHash: header hash or height of the block
   * @returns {Array|Transaction}: an array of transactions
   */
  getBlockTransactions (blockHash, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Returns all transactions that matched with search condition.
   *
   * @params {Object} options: have these properties:
   *   - fromBlockNumber {Number}
   *   - toBlockNumber {Number}
   * @returns {Array|Transaction}: an array of transactions
   */
  getBatchTransactions (options, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Validate a transaction and broadcast it to the blockchain network
   *
   * @param {String} rawTx: the hex-encoded transaction data
   * @returns {String} txid: the transaction hash in hex
   */
  sendRawTransaction (rawTx, callback) {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Get balance of an address
   *
   * @param {String} address: address that want to query balance
   * @returns {Number} balance: the current balance of address
   */
  getAddressBalance (address, callback) {
    throw new Error(`Implement me in derived class.`);
  }
}

module.exports = BaseGateway;
