const BtcGatewayClass = require('./btc/BtcGateway');
const DashGatewayClass = require('./dash/DashGateway');
const QtumGatewayClass = require('./qtum/QtumGateway');
const EthGatewayClass = require('./eth/EthGateway');
const UsdtGatewayClass = require('./usdt/UsdtGateway');

module.exports.BtcGateway = new BtcGatewayClass();
module.exports.DashGateway = new DashGatewayClass();
module.exports.QtumGateway = new QtumGatewayClass();
module.exports.EthGateway = new EthGatewayClass();
module.exports.UsdtGateway = new UsdtGatewayClass();