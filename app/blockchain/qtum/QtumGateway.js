const _                 = require('lodash');
const async             = require('async');
const RpcClient         = require('qtumd-rpc');
const qtumcore          = require('qtumcore-lib');
const networkConfig     = require('../../../config/qtum');
const UTXOBasedGateway  = require('../utxo-based/UTXOBasedGateway');
const logger            = require('sota-core').getLogger('QtumGateway');

const rpcClient = new RpcClient({
  protocol: process.env.QTUM_WALLET_RPC_PROTOCOL,
  user: process.env.QTUM_WALLET_RPC_USER,
  pass: process.env.QTUM_WALLET_RPC_PASSWORD,
  host: process.env.QTUM_WALLET_RPC_HOST,
  port: process.env.QTUM_WALLET_RPC_PORT,
});

qtumcore.Transaction.FEE_SECURITY_MARGIN = 15000;

class QtumGateway extends UTXOBasedGateway {

  constructor () {
    super();
  }

  getLib () {
    return qtumcore;
  }

  getNetwork () {
    return process.env.QTUM_NETWORK;
  }

  /**
   * Get RPC client to connect to a full node
   */
  getRPCClient () {
    return rpcClient;
  }

  getAPIEndpoint () {
    return _.sample(networkConfig.apiEndpoints);
  }

}

module.exports = QtumGateway;