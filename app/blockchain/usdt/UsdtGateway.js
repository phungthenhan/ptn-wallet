const _                 = require('lodash');
const async             = require('async');
const OmniClient      = require('./../../../test/OmniClient.js').OmniClient;
const bitcore           = require('bitcore-lib');
const networkConfig     = require('../../../config/btc');
const UTXOBasedGateway  = require('../utxo-based/UTXOBasedGateway');
const logger            = require('sota-core').getLogger('BtcGateway');

const rpcClient = new OmniClient({
  host: process.env.USDT_WALLET_RPC_HOST,
  port: process.env.USDT_WALLET_RPC_PORT,
  user: process.env.USDT_WALLET_RPC_USER,
  pass: process.env.USDT_WALLET_RPC_PASSWORD
});


class UsdtGateway extends UTXOBasedGateway {

  constructor () {
    super();
  }

  getNetwork () {
    return process.env.USDT_NETWORK;
  }
  getLib () {
    return bitcore;
  }

  /**
   * Get RPC client to connect to a full node
   */
  getRPCClient () {
    return rpcClient;
  }

  getAPIEndpoint () {
    return _.sample(networkConfig.apiEndpoints);
  }


  getBlockTransactions (blockNumber, callback) {
    async.waterfall([
      (next) => {
        rpcClient.callRpc('omni_listblocktransactions', [blockNumber],(err, txs) => {
          next(err, txs);
        });
      },
      (idTxs, next) => {
        this.getTxsOfBlock(idTxs, next);
      }
    ], callback);
  }

  getBatchTransactions (options, callback) {
    let txs = [];
    const fromBlockNumber = options.fromBlockNumber;
    const toBlockNumber = options.toBlockNumber;

    if (!fromBlockNumber || !toBlockNumber) {
      return callback(`You must specify the block range, currently: start=${fromBlockNumber}, end=${toBlockNumber}`);
    }

    if (fromBlockNumber > toBlockNumber) {
      return callback(`Start block must be less or equal end block, currently start=${fromBlockNumber}, end=${toBlockNumber}`);
    }

    const blockNumbers = [];
    for (let no = fromBlockNumber; no <= toBlockNumber; no++) {
      blockNumbers.push(no);
    }

    async.eachLimit(blockNumbers, 1, (blockNumber, next) => {
      this.getBlockTransactions(blockNumber, (err, ret) => {
        if (err) {
          return next(err);
        }
        txs = txs.concat(ret);
        return next(null, null);
      });
    }, (err) => {
      if (err) {
        return callback(err);
      }


      txs = _.compact(txs);
      return callback(null, txs);
    });
  }

  getTxsOfBlock(idTxs, callback) {
    const txs = idTxs;
    const all_txs = [];
    if (txs.length == 0) {
      return callback(null);
    }
    if (txs != null && txs.length > 0) {
      async.each(txs, (txAddress, _next) => {
        rpcClient.callRpc('omni_gettransaction', [txAddress], (err, tx) => {

          if (tx == null) {
            return _next(null);
          }

          tx.time = tx.blocktime;
          tx.height = tx.block;
          all_txs.push(tx);
          _next(null);
        });
      }, (err) => {
        if (err) {
          return callback(err);
        }
        return callback(null, all_txs);
      });
    }
  }

  getBlockCount(callback) {

    rpcClient.callRpc('getblockcount', [], callback);
  }

}

module.exports = UsdtGateway;