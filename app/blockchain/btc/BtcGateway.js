const _                 = require('lodash');
const async             = require('async');
const RpcClient         = require('bitcoind-rpc');
const bitcore          = require('bitcore-lib');
const networkConfig     = require('../../../config/btc');
const UTXOBasedGateway  = require('../utxo-based/UTXOBasedGateway');
const logger            = require('sota-core').getLogger('BtcGateway');

const rpcClient = new RpcClient({
  protocol: process.env.BTC_WALLET_RPC_PROTOCOL,
  user: process.env.BTC_WALLET_RPC_USER,
  pass: process.env.BTC_WALLET_RPC_PASSWORD,
  host: process.env.BTC_WALLET_RPC_HOST,
  port: process.env.BTC_WALLET_RPC_PORT,
});

class BtcGateway extends UTXOBasedGateway {

  constructor () {
    super();
  }

  getNetwork () {
    return process.env.BTC_NETWORK;
  }
  getLib () {
    return bitcore;
  }

  /**
   * Get RPC client to connect to a full node
   */
  getRPCClient () {
    return rpcClient;
  }

  getAPIEndpoint () {
    return _.sample(networkConfig.apiEndpoints);
  }

}

module.exports = BtcGateway;