const _                 = require('lodash');
const async             = require('async');
const RpcClient         = require('@dashevo/dashd-rpc');
const dashcore          = require('@dashevo/dashcore-lib');
const networkConfig     = require('../../../config/dash');
const UTXOBasedGateway  = require('../utxo-based/UTXOBasedGateway');
const logger            = require('sota-core').getLogger('DashGateway');

const rpcClient = new RpcClient({
  protocol: process.env.DASH_WALLET_RPC_PROTOCOL,
  user: process.env.DASH_WALLET_RPC_USER,
  pass: process.env.DASH_WALLET_RPC_PASSWORD,
  host: process.env.DASH_WALLET_RPC_HOST,
  port: process.env.DASH_WALLET_RPC_PORT,
});

class DashGateway extends UTXOBasedGateway {

  constructor () {
    super();
  }

  getLib () {
    return dashcore;
  }

  getNetwork () {
    return process.env.DASH_NETWORK;
  }

  /**
   * Get RPC client to connect to a full node
   */
  getRPCClient () {
    return rpcClient;
  }

  getAPIEndpoint () {
    return _.sample(networkConfig.apiEndpoints);
  }

}

module.exports = DashGateway;