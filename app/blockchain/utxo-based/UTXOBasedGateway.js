const _             = require('lodash');
const async         = require('async');
const request       = require('superagent');
const Const         = require('../../common/Const');
const BaseGateway   = require('../BaseGateway');
const logger        = require('sota-core').getLogger('UTXOBasedGateway');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');

class UTXOBasedGateway extends BaseGateway {

  constructor () {
    super();
  }

  /**
   * Get lib to handle offline operations:
   * - BTC: bitcore
   * - Dash: dashcore
   * - Qtum: qtumcore
   * ...
   */
  getLib () {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Get network to handle offline operations:
   * - BTC: bitcore
   * - Dash: dashcore
   * - Qtum: qtumcore
   * ...
   */
  getNetwork () {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Get RPC client to connect to a full node
   */
  getRPCClient () {
    throw new Error(`Implement me in derived class.`);
  }

  /**
   * Wrap RPC call
   */
  handleRPCCall (method, args, callback) {
    const wrappedCallback = (err, ret) => {
      if (err) {
        if (err.code && err.message) {
          logger.info(`Request method=${method}, args=${JSON.stringify(args)}: ${err.message} (code: ${err.code})`);
          return callback(err);
        }

        return callback(err);
      }

      if (ret.error) {
        logger.info(`Request method=${method}, args=${JSON.stringify(args)}: ${ret.error}`);
        return callback(ret.error);
      }

      return callback(null, ret.result);
    };

    const rpcClient = this.getRPCClient();
    rpcClient[method].apply(rpcClient, args.concat(wrappedCallback));
  }

  /**
   * Get general info of RPC node
   */
  getRPCNodeInfo (callback) {
    this.handleRPCCall('getInfo', [], callback);
  }

  /**
   * Get a hex-encoded serialized transaction or a JSON object describing the transaction.
   *
   * @param {String} txid: The transaction hash (id) that we want to track
   * @param {Number} verbosity: The format of result
   *   - Verbosity is 0: return hex-encoded transaction content
   *   - Verbosity is 1: return transaction in JSON
   * @returns {Mixed}:
   *   - Verbosity is 0: return String
   *   - Verbosity is 1: return Object
   */
  getRawTransaction (txid, verbosity, callback) {
    if (!txid || typeof txid !== 'string') {
      callback(`UTXOBasedGateway::getRawTransaction invalid txid: ${txid}`);
    }

    if (typeof verbosity === 'function') {
      callback = verbosity;
      verbosity = 0;
    }

    if (verbosity !== 0 && verbosity !== 1) {
      callback(`UTXOBasedGateway::getRawTransaction invalid verbosity: ${verbosity}`);
    }

    this.handleRPCCall('getRawTransaction', [txid, verbosity], callback);
  }

  /**
   * Get one transaction object
   *
   * @params {String} txid: the transaction hash
   * @returns {Object} tx: the transaction details
   */
  getOneTransaction (txid, callback) {
    this.getRawTransaction(txid, 1, callback);
  }

  /**
   * Returns transactions with given txids
   *
   * @ param {Array} txids: the array of transaction hashes/ids
   * @ returns {Array} txs: the array of detailed transactions
   */
  getTransactionsByIds (txids, callback) {
    const txs = [];
    async.eachLimit(_.uniq(txids), 50, (txid, next) => {
      this.getOneTransaction(txid, (err, tx) => {
        if (err) {
          return next(err);
        }

        txs.push(tx);
        next(null, null);
      });
    }, (err) => {
      if (err) return callback(err);

      return callback(null, txs);
    });
  }

  /**
   * Returns the header hash of a block
   * at the given height in the local best block chain
   *
   * @ param {Number} height: the height index/block number
   * @ returns {String} hash: the header hash of the block we're looking for
   */
  getBlockHash (height, callback) {
    if (typeof height !== 'number') {
      throw new Error(`UTXOBasedGateway::getBlockHash invalid height: ${height}`);
    }

    this.handleRPCCall('getBlockHash', [height], callback);
  }

  /**
   * The getblock RPC gets a block with a particular header hash
   * from the local block database either as a JSON object or as a serialized block.
   *
   * @param {String} blockHash: The block hash (or block number in case the parameter is Number)
   */
  getRawBlock (blockHash, verbosity, callback) {
    if (typeof verbosity === 'function') {
      callback = verbosity;
      verbosity = 0;
    }

    // Also accept block number as parameter
    if (typeof blockHash === 'number') {
      const blockNum = blockHash;
      async.waterfall([
        (next) => {
          this.getBlockHash(blockNum, next);
        },
        (hash, next) => {
          this.getRawBlock(hash, verbosity, next);
        }
      ], callback);
      return;
    }

    if (typeof blockHash !== 'string') {
      throw new Error(`UTXOBasedGateway::getRawBlock invalid blockHash: ${blockHash}`);
    }

    this.handleRPCCall('getBlock', [blockHash, verbosity], callback);
  }

  /**
   * Get block details in application-specified format
   *
   * @param {String} blockHash: the block hash (or block number in case the parameter is Number)
   * @returns {BaseBlock} block: the block detail
   * TODO: unify format of block detail object
   */
  getOneBlock (blockHash, verbosity, callback) {
    if (typeof verbosity === 'function') {
      callback = verbosity;
      verbosity = 1;
    }

    this.getRawBlock(blockHash, verbosity, callback);
  }

  /**
   * No param
   * Returns the number of blocks in the local best block chain.
   */
  getBlockCount (callback) {
    this.handleRPCCall('getBlockCount', [], callback);
  }

  /**
   * Returns all transactions in givent block.
   *
   * @params {String|Number} blockHash: header hash or height of the block
   * @returns {Array|Transaction}: an array of transactions
   */
  getBlockTransactions (blockHash, callback) {
    async.waterfall([
      (next) => {
        this.getRawBlock(blockHash, 1, next);
      },
      (block, next) => {
        const txids = block.tx;
        this.getTransactionsByIds(txids, next);
      }
    ], callback);
  }

  /**
   * Returns all transactions that matched with search condition.
   *
   * @params {Object} options: have these properties:
   *   - fromBlockNumber {Number}
   *   - toBlockNumber {Number}
   * @returns {Array|Transaction}: an array of transactions
   */
  getBatchTransactions (options, callback) {
    let txs = [];

    const fromBlockNumber = options.fromBlockNumber;
    const toBlockNumber = options.toBlockNumber;

    if (!fromBlockNumber || !toBlockNumber) {
      return callback(`You must specify the block range, currently: start=${fromBlockNumber}, end=${toBlockNumber}`);
    }

    if (fromBlockNumber < 0 || toBlockNumber < 0) {
      return callback(`Start and end block number must be greater than zero, currently: start=${fromBlockNumber}, end=${toBlockNumber}`);
    }

    if (fromBlockNumber > toBlockNumber) {
      return callback(`Start block must be less or equal end block, currently start=${fromBlockNumber}, end=${toBlockNumber}`);
    }

    const blockNumbers = [];
    for (let no = fromBlockNumber; no <= toBlockNumber; no++) {
      blockNumbers.push(no);
    }

    async.each(blockNumbers, (blockNumber, next) => {
      this.getBlockTransactions(blockNumber, (err, ret) => {
        if (err) {
          return next(err);
        }

        txs = txs.concat(_.map(ret, tx => {
          if (!tx.height) {
            tx.height = blockNumber;
          }

          return tx;
        }));
        return next(null, null);
      });
    }, (err) => {
      if (err) {
        return callback(err);
      }

      txs = _.compact(txs);
      return callback(null, txs);
    });
  }

  /**
   * Validate a transaction and broadcast it to the blockchain network
   *
   * @param {String} rawTx: the hex-encoded transaction data
   * @returns {String} txid: the transaction hash in hex
   */
  sendRawTransaction (rawTx, callback) {
    this.handleRPCCall('sendRawTransaction', [rawTx], callback);
  }

  /**
   * Get balance of an address
   *
   * @param {String} address: address that want to query balance
   * @returns {Number} balance: the current balance of address in satoshi
   */
  getAddressBalance (address, callback) {
    const apiEndpoint = this.getAPIEndpoint();
    const url = `${apiEndpoint}/addr/${address}/?noTxList=1`;

    request(url,  (err, res) => {
      if (err) {
        return callback(err);
      }

      return callback(null, res.body.balanceSat);
    });
  }

  /**
   * Get UTXOs of an address
   */
  getAddressUtxos (address, callback) {
    const apiEndpoint = this.getAPIEndpoint();
    const url = `${apiEndpoint}/addr/${address}/utxo`;

    request(url,  (err, res) => {
      if (err) {
        return callback(err);
      }

      const utxos = res.body;
      _.each(utxos, (utxo) => {
        if (!utxo.amountSat) {
          utxo.amountSat = utxo.amount * Const.SATOSHI_FACTOR;
        }
      });

      return callback(null, utxos);
    });
  }

  getTransactionInfo(txid, callback){
    async.auto({
      tx: (next) => {
        this.getRawTransaction(txid, 1, next);
      },
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const tx = ret.tx;
      if (!tx || !tx.blockhash) {
        return callback(Const.ASYNC_BREAKER);
      }

      return callback(null, {
        blockHash: tx.blockhash,
        status: Const.TX_STATUS.SUCCESS,
        confirmations: tx.confirmations,
        timestamp: tx.time,
        blockNumber: tx.height,
      });
    });
  }
}

module.exports = UTXOBasedGateway;
