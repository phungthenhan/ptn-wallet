const _                       = require('lodash');
const async                   = require('async');
const Checkit                 = require('cc-checkit');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const AppController           = require('./AppController');
const logger                  = require('sota-core').getLogger('WebhookController');

module.exports = AppController.extends({
  classname: 'WebhookController',

  /**
   * LIST WALLET WEBHOOKS
   * List all webhooks set up on the wallet.
   * Currently, the types of webhooks that can be attached to a wallet are transfer notifications.
   */
  getListWalletWebhooks: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const WalletWebhookService = req.getService('WalletWebhookService');
    const coin = params.coin;
    const walletId = params.walletId;

    WalletWebhookService.getListWalletWebhooks(walletId, coin, this.ok.bind(this, req, res));
  },

  /**
   * ADD A WALLET WEBHOOK
   * Add a webhook that will result in an HTTP callback at the specified URL from Bitkoex-Wallet when events are triggered.
   * There is a limit of 10 Webhooks of each type per wallet.
   * Types of wallet webhooks available:
   * - Transfer webhooks will fire on any transfer on the wallet.
   * - Pending approval webhooks will fire when an event triggers policy on the wallet (e.g send transaction,
       user change, policy change, pending approval updated).
   * - Address confirmation webhooks will fire when an address has been initialized on the wallet.
   */
  createWalletWebhook: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      url: ['required', 'string'],
      type: ['required', 'string'],
      numConfirmations: ['natural'],
      label: ['string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const WalletWebhookService = req.getService('WalletWebhookService');

    WalletWebhookService.createWalletWebhook(params, this.ok.bind(this, req, res));
  },

  /**
   * REMOVE A WALLET WEBHOOK
   * Removing a webhook will cause new events of the specified type to no longer trigger HTTP callbacks to your URLs.
   */
  removeWalletWebhook: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      type: ['required', 'string'],
      url: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const WalletWebhookService = req.getService('WalletWebhookService');

    WalletWebhookService.removeWalletWebhook(params, this.ok.bind(this, req, res));
  },

  /**
   * SIMULATE WALLET WEBHOOK
   * This API allows you to simulate and test a webhook so you can view its response.
   */
  simulateWebhook: function (req, res) {
    // TBD: implement me.
  },

  /**
   * LIST USER WEBHOOKS
   * List all webhooks set up on the user.
   * Currently, the types of webhooks that can be attached to a user are block notifications.
   */
  getListUserWebhooks: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const UserWebhookService = req.getService('UserWebhookService');
    const coin = params.coin;
    const userId = req.user ? req.user.id : 0;

    UserWebhookService.getListUserWebhooks(userId, coin, this.ok.bind(this, req, res));
  },

  /**
   * ADD A USER WEBHOOK
   * Add a webhook that will result in an HTTP callback at the specified URL from Bitkoex-Wallet when events are triggered.
   * There is one type of user webhook available:
   * - Block webhooks will fire when a new block is seen on the coin network.
   * - Wallet confirmation webhooks will fire when a wallet has been initialized.
   */
  createUserWebhook: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const UserWebhookService = req.getService('UserWebhookService');
    const coin = params.coin;
    const userId = req.user ? req.user.id : 0;

    UserWebhookService.createUserWebhook(userId, params, this.ok.bind(this, req, res));
  },

  /**
   * REMOVE A WALLET WEBHOOK
   * Removing a webhook will cause new events of the specified type to no longer trigger HTTP callbacks to your URLs.
   */
  removeUserWebhook: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const UserWebhookService = req.getService('UserWebhookService');
    const coin = params.coin;
    const userId = req.user ? req.user.id : 0;

    UserWebhookService.deleteUserWebhook(webhookId, this.ok.bind(this, req, res));
  },

});

