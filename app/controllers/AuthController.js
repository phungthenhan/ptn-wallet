const AuthController          = require('sota-core').load('controller/AuthController');

module.exports = AuthController.extends({
  classname: 'AuthController',

  generateAccessToken: function ($super, user, expiredTime) {
    // Default 100 years (never-expired) token.
    return $super(user, expiredTime || 3600 * 24 * 365 * 100);
  },

});
