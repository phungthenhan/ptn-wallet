const async                   = require('async');
const AppController           = require('./AppController');
const logger                  = log4js.getLogger('WebViewController');

module.exports = AppController.extends({
  classname: 'WebViewController',

  collectorPage: function (req, res) {
    res.render('collector/index');
  },

});
