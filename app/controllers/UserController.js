const _                       = require('lodash');
const async                   = require('async');
const Checkit                 = require('cc-checkit');
const AppController           = require('./AppController');
const ErrorFactory            = require('sota-core').load('error/ErrorFactory');
const logger                  = require('sota-core').getLogger('UserController');

module.exports = AppController.extends({
  classname: 'UserController',

  /*
   * GET CURRENT SESSION INFORMATION
   * This API call retrieves information about the current session access token.
   * You can use this API call if you’d like to verify the scope of your access token,
   * which will include allowed scopes as well as the expiration date of the token and any associated unlocks.
   */
  getCurrentSession: function(req, res) {
    if (!req.user || !req.user.id) {
      res.unauthorized(`Unauthorized. Cannot get session information.`);
      return;
    }

    const UserService = req.getService('UserService');
    UserService.getCurrentSession(req.user.id, this.ok.bind(this, req, res));
  },

  /*
   * CREATE API ACCESS TOKENS
   * TBD: Implement me if needed.
   */
  createSession: function (req, res) {
    res.badRequest(`TBD: implement me.`);
  },

  /*
   * LOCK
   * Locks the current session, preventing future automated sends
   * without a subsequent unlock (OTP will be required for the unlock).
   * TBD: implement me
   */
  lock: function (req, res) {
    res.badRequest(`TBD: implement me.`);
  },

  /*
   * UNLOCK
   * Unlock the current session, which is required for certain other sensitive API calls.
   * TBD: implement me
   */
  unlock: function (req, res) {
    res.badRequest(`TBD: implement me.`);
  },

  /*
   * GET CURRENT USER PROFILE
   * This API call retrieves information about the current authenticated user.
   * You can use this API call as a sort of sanity test or if you use multiple users in your code.
   */
  me: function(req, res) {
    if (!req.user || !req.user.id) {
      res.unauthorized(`Unauthorized. Cannot get user information.`);
      return;
    }

    const UserService = req.getService('UserService');
    UserService.getUserDetails(req.user.id, this.ok.bind(this, req, res));
  },

  changePassword: function (req, res) {
    const [err, params] = new Checkit({
      password: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      return res.badRequest(err.toString());
    }

    const UserModel = req.getModel('UserModel');
    UserModel.updatePassword(req.user.id, params.password, this.ok.bind(this, req, res));
  }

});
