const async                   = require('async');
const AppController           = require('./AppController');
const logger                  = log4js.getLogger('DebugController');

module.exports = AppController.extends({
  classname: 'DebugController',

  testPage: function (req, res) {
    res.render('test');
  },

  demoPage: function (req, res) {
    res.render('demo/index');
  },

});
