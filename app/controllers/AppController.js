const _                       = require('lodash');
const ExternalServiceAdapter  = require('sota-core').load('external_service/foundation/ExternalServiceAdapter');
const BaseController          = require('sota-core').load('controller/BaseController');

module.exports = BaseController.extends({
  classname: 'AppController',

  render: function (res, templateName, data) {
    if (!data) {
      data = {};
    }

    data.getTemplate = function (templateName) {
      return templateName.toString();
    }.bind(this, templateName);

    res.render('main', data);
  },

  _response: function (method, req, res, err, result) {
    if (err) {
      return req.rollback(err);
    }

    res.json(result);
  }

});
