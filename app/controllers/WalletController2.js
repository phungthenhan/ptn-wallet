const _                       = require('lodash');
const async                   = require('async');
const Checkit                 = require('cc-checkit');
const request                 = require('superagent');
const Const                   = require('../common/Const');
const AppController           = require('./AppController');
const logger                  = require('sota-core').getLogger('WalletController2');

module.exports = AppController.extends({
  classname: 'WalletController2',

  getOrCreateDefaultAddress: function (req, res) {
    return res.ok(`TBD: Implement me.`);
  },

  generateNewDefaultAddress: function (req, res) {
    return res.ok(`TBD: Implement me.`);
  },

  sendCoins: function (req, res) {
    return res.ok(`TBD: Implement me.`);
  },

  getAddressBalance: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      address: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    // TODO: implement me.
    const balance = 1000000000;
    return res.json({
      coin: params.coin,
      address: params.address,
      balance: balance,
      balanceString: balance.toString(),
    });
  },

});

