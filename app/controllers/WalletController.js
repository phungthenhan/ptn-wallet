const _                       = require('lodash');
const async                   = require('async');
const Checkit                 = require('cc-checkit');
const BigNumber               = require('bignumber.js');
const Const                   = require('../common/Const');
const AppController           = require('./AppController');
const logger                  = require('sota-core').getLogger('WalletController');

const listCoins = _.values(Const.COIN);
const listUTXOCoins = _.values(Const.UTXO_COIN);
const listERC20Tokens = _.keys(Const.ERC20);

module.exports = AppController.extends({
  classname: 'WalletController',

  /**
   * LIST WALLETS
   * This API call lists all of a user’s wallets for a given coin.
   * This is useful when you have over 500 wallets as you will not be able to use the Web UI.
   */
  getListWallets: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    // Check whether the coin is matched with predefined ones.
    let coin = params.coin;
    if (!_.includes(listCoins, coin) && !_.includes(listERC20Tokens, coin)) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    const pagination = req.pagination;
    if (_.isEmpty(pagination)) {
      res.badRequest(`No pagination info.`);
      return;
    }

    // ERC20 tokens and ETH use the same address and wallet
    if (_.includes(listERC20Tokens, coin)) {
      coin = Const.COIN.ETH;
    }

    const WalletService = req.getService('WalletService');
    const userId = req.user ? req.user.id : 0;

    WalletService.getListWallets(userId, coin, pagination, this.ok.bind(this, req, res));
  },

  /**
   * GENERATE WALLET
   * This API call creates a new wallet. Under the hood, the SDK (or Bitkoex-Wallet Express) does the following:
   * 1. Creates the user keychain locally on the machine, and encrypts it with the provided passphrase
        (skipped if userKey is provided).
   * 2. Ceates the backup keychain locally on the machine, and encrypts it with the provided passphrase
        (skipped if backupXpub or backupXpubProvider are provided).
   * 3. Uploads the encrypted user and backup keychains to Bitkoex-Wallet.
   * 4. Creates the Bitkoex-Wallet key (and the backup key if backupXpubProvider is set) on the service.
   * 5. Creates the wallet on Bitkoex-Wallet with the 3 public keys above.
   */
  createWallet: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      label: ['required', 'string'],
      passphrase: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    let coin = params.coin;
    if (!_.includes(listCoins, coin) && !_.includes(listERC20Tokens, coin)) {
      res.badRequest(`Invalid coin type: ${coin}`);
    }

    // ERC20 tokens and ETH use the same address and wallet
    if (_.includes(listERC20Tokens, coin)) {
      coin = Const.COIN.ETH;
    }

    const userId = req.user ? req.user.id : 0;
    const label = params.label;

    if (_.includes(listUTXOCoins, coin)){
      const UTXOBasedWalletService = req.getService('UTXOBasedWalletService');
      UTXOBasedWalletService.createWallet(userId, coin, label, this.ok.bind(this, req, res));
    } else {
      const WalletService = req.getService('WalletService');
      WalletService.createWallet(userId, coin, label, this.ok.bind(this, req, res));
    }
  },

  /**
   * GET WALLET DETAILS
   * This API call retrieves wallet object information by the wallet ID.
   * This is useful to get the balance of the wallet, or identify the keys
   * used to sign with the wallet using the Get Keychain API.
   */
  getWalletDetails: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getOneWallet(coin, walletId, this.ok.bind(this, req, res));
  },

  /**
   * GET WALLET BY ADDRESS
   * This API call retrieves wallet object information by an address belonging to the wallet.
   * This is useful to get the balance of the wallet, or identify the keys used to sign
   * with the wallet using the Get Keychain API.
   */
  getWalletByAddress: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      address: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const address = params.address;

    if (coin === Const.COIN.DASH) {
      const DashWalletService = req.getService('DashWalletService');
      DashWalletService.getOneWalletByAddress(address, this.ok.bind(this, req, res));
    } else {
      res.badRequest(`Invalid coin type: ${coin}`);
    }
  },

  /**
   * CREATE WALLET ADDRESS
   * This API call is used to create a new receive address for your wallet, which enhances your privacy.
   * You may choose to call this API whenever a deposit is made.
   * The Bitkoex-Wallet API supports millions of addresses.
   * Please check the “Coin-Specific Implementation” with regards to fee address management for ethereum.
   */
  createWalletAddress: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.createWalletAddress(walletId, this.ok.bind(this, req, res));
  },

  /**
   * GET WALLET ADDRESS
   * This API call is used to get information about a single wallet address.
   * The address object can be fetched by using either the id or the address itself.
   */
  getOneWalletAddress: function(req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      address: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const address = params.address;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getOneWalletAddress(address, walletId, this.ok.bind(this, req, res));
  },

  /**
   * LIST WALLET ADDRESSES
   * This API call is used to retrieve a wallet’s addresses.
   * This covers all receive addresses, including both external and change addresses.
   */
  getWalletAddresses: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const pagination = req.pagination;
    if (!_.isEmpty) {
      res.badRequest(`No pagination info.`);
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getWalletAddresses(walletId, pagination, this.ok.bind(this, req, res));
  },

  /**
   * SEND TRANSACTION
   * This API call allows you to create and send cryptocurrency to a destination address.
   */
  sendCoins: function(req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      address: ['required', 'string'],
      amount: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const address = params.address;
    const userId = req.user.id;

    const amount = new BigNumber(params.amount);

    if (!amount.isInteger()) {
      res.badRequest(`Amount to send must be integer. Invalid value: ${params.amount}`);
      return;
    }

    if (!amount.isGreaterThan(0)) {
      res.badRequest(`Amount to send must be positive. Invalid value: ${params.amount}`);
      return;
    }

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.sendCoins(userId, coin, params, this.ok.bind(this, req, res));
  },

  /**
   * SEND TRANSACTION TO MANY
   * This API call allows you to create a transaction and send to multiple addresses.
   * This may be useful if you schedule outgoing transactions in bulk,
   * as you will be able to process multiple recipients
   * and lower the aggregate amount of blockchain fees paid.
   * TBD: revise me.
   */
  sendCoinsToMany: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      recipients: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.sendCoinsToMany(params, this.ok.bind(this, req, res));
  },

  /**
   * LIST WALLET TRANSFERS
   * Retrieves a list of transfers, which correspond to the deposits and withdrawals of digital currency on a wallet.
   * TBD: Revise me
   */
  listWalletTranfers: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const pagination = _.assign(req.pagination, Const.DEFAULT_PAGINATION);

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.listWalletTransfers(walletId, pagination, this.ok.bind(this, req, res));
  },

  /**
   * GET ONE WALLET TRANSFER
   * Wallet transfers represent digital currency sends and receives on your wallet.
   * This is the recommended method to track funding withdrawals and deposits.
   * TBD: Revise me
   */
  getOneWalletTransfer: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      txid: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const txid = params.txid;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getOneWalletTransfer(walletId, txid, this.ok.bind(this, req, res));
  },

  /*
   * GET WALLET TRANSFER BY SEQUENCE ID
   * Get the transaction on a wallet sequence ID that was passed in
   * when sending a transaction (via Send Transaction or Send Transaction to Many).
   * This is useful for tracking an unsigned/unconfirmed transaction via your own unique ID,
   * as coin transaction IDs are not defined before co-signing.
   * A pending transaction that has not yet been co-signed by Bitkoex-Wallet will still have a sequence id.
   * TBD: Revise me
   */
  getOneWalletTransferBySequenceId: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
      sequenceId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const txid = params.txid;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getOneWalletTransferBySequenceId(walletId, txid, this.ok.bind(this, req, res));
  },

  /*
   * LIST WALLET TRANSACTIONS
   * This API call lists all blockchain transactions for a given wallet.
   */
  listWalletTransactions: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const pagination = req.pagination;
    if (_.isEmpty(req.pagination)) {
      res.badRequest(`No pagination info.`);
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.listWalletTransactions(walletId, pagination, this.ok.bind(this, req, res));
  },

  getAddressBalance: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      address: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const  coin = params.coin;
    const address = params.address;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getAddressBalance(coin, address, this.ok.bind(this, req, res));
  },

  /*
   * LIST COIN WITHDRAWALS
   */
  listCoinWithdrawals: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }
    const pagination = req.pagination;
    if (_.isEmpty(req.pagination)) {
      res.badRequest(`No pagination info.`);
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.listCoinWithdrawals(coin, walletId, pagination, this.ok.bind(this, req, res));
  },

  /*
   * LIST COIN WITHDRAWALS
   */
  listCoinDeposits: function(req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;
    const validTokens = _.keys(Const.ERC20);
    const pagination = req.pagination;
    if (_.isEmpty(req.pagination)) {
      res.badRequest(`No pagination info.`);
      return;
    }

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.listCoinDeposits(coin, walletId, pagination, this.ok.bind(this, req, res));
  },

  /*
   * GET ONE WALLET TRANSACTION
   * This API call retrieves object information for a specific tranasaction hash by specifying the transaction ID.
   */
  getOneTransaction: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['string'],
      txid: ['required', 'string']
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const txid = params.txid;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    // For ERC20 tokens, need to pass token name as parameter
    if (_.includes(listERC20Tokens, coin)) {
      CoinWalletService.getOneTransaction(coin, txid, this.ok.bind(this, req, res));
      return;
    }

    CoinWalletService.getOneTransaction(txid, this.ok.bind(this, req, res));
  },

  /*
   * LIST WALLET UNSPENTS
   * This API call will retrieve the unspent transaction outputs (UTXOs) within a wallet.
   * This may be relevant if you are trying to do manual coin control
   * when operating an unspent-based blockchain such as Bitcoin.
   * TBD: revise me.
   */
  getWalletUnspents: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getWalletUnspents(walletId, txid, this.ok.bind(this, req, res));
  },

  /*
   * GET WALLET MAXIMUM SPENDABLE
   * This API call will retrieve the maximum amount that can be spent with a single transaction from the wallet.
   * The maximum spendable amount can differ from a wallet’s total balance.
   * A transaction can only use up to 200 unspents.
   * Wallets that have more than 200 unspents cannot spend the full balance in one transaction.
   * Additionally, the value returned for the maximum spendable amount accounts for the current fee level
   * by deducting the estimated fees.
   * The amount will only be calculated based on the unspents that fit the parameters passed.
   * TBD: revise me.
   */
  getMaximumSpendable: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      walletId: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const coin = params.coin;
    const walletId = params.walletId;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getMaximumSpendable(walletId, this.ok.bind(this, req, res));
  },

  /*
   * CONSOLIDATE WALLET UNSPENTS
   * This SDK call will consolidate the unspents that match the parameters,
   * and consolidate them into the number specified by `numUnspentsToMake`
   * TBD: revise me.
   */
  consolidateWalletUnspents: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  /*
   * FANOUT WALLET UNSPENTS
   * This SDK call will fanout the unspents currently in the wallet that match the parameters,
   * and use them as inputs to create more unspents.
   * TBD: revise me.
   */
  fanoutWalletUnspents: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  /*
   * BUILD TRANSACTION
   * Create a transaction with the specified parameters.
   * This builds a transaction object, but does not sign or send it.
   * TBD: revise me.
   */
  buildTransaction: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  /*
   * BUILD TRANSACTION
   * Create a transaction with the specified parameters.
   * This builds a transaction object, but does not sign or send it.
   * TBD: revise me.
   */
  buildTransaction: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  /*
   * SIGN TRANSACTION
   * Sign the given transaction with the specified keychain.
   * All signing is done locally and can be performed offline.
   * Signing can happen two ways: with a `prv` argument representing the private key,
   * or with `keychain` and `walletPassphrase` arguments (for signing with an encrypted private key).
   * TBD: revise me.
   */
  signTransaction: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  /*
   * SEND TRANSACTION
   * Submit a half-signed transaction. The server will do one of the following:
   * - apply the final signature, and submit to the Bitcoin P2P network
   * - gather additional approvals from other wallet admins
   * - reject the transaction. This may happen, for example, because of a policy violation
   * TBD: revise me.
   */
  sendTransaction: function (req, res) {
    return res.badRequest('TBD: implement me.');
  },

  getAddressTransactions: function(req, res) {
    const [err, params] = new Checkit({
      address: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const AddressModel = req.getModel('AddressModel');
    const DashWithdrawalModel = req.getModel('DashWithdrawalModel');

    async.auto({
      // address: (next) => {
      //   AddressModel.findOne({
      //     where: 'address = ?',
      //     params: [params.address]
      //   }, next);
      // },
      transactions: (next) => {
        DashWithdrawalModel.find({
          where: 'from_address = ? OR to_address = ?',
          params: [params.address, params.address]
        }, next);
      }
    }, (err, ret) => {
      if (err) {
        logger.error(err);
        res.sendError(err);
        return;
      }

      res.ok(ret.transactions);
    });
  },

  _getCoinWalletService: function (req, coin) {

    if (coin === Const.COIN.BTC) {
      return req.getService('BtcWalletService');
    }

    if (coin === Const.COIN.DASH) {
      return req.getService('DashWalletService');
    }

    if (coin === Const.COIN.ETH) {
      return req.getService('EthWalletService');
    }

    if (coin === Const.COIN.QTUM) {
      return req.getService('QtumWalletService');
    }

    if (coin === Const.COIN.USDT) {
      return req.getService('UsdtWalletService');
    }

    if (coin === Const.COIN.NEO) {
      return req.getService('NeoWalletService');
    }

    if (_.includes(listERC20Tokens, coin)) {
      return req.getService('Erc20WalletService');
    }

  },

  getTransactionTrace: function (req, res) {
    const [err, params] = new Checkit({
      coin: ['required', 'string'],
      txid: ['required', 'string'],
    }).validateSync(req.allParams);

    if (err) {
      res.badRequest(err.toString());
      return;
    }

    const  coin = params.coin;
    const txid = params.txid;

    const CoinWalletService = this._getCoinWalletService(req, coin);
    if (!CoinWalletService) {
      res.badRequest(`Invalid coin type: ${coin}`);
      return;
    }

    CoinWalletService.getTransactionTrace(coin, txid, this.ok.bind(this, req, res));
  },

});

