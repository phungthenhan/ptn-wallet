const _ = require('lodash');

module.exports = function parseIntegerArray(val) {
  if (!val) {
    return [];
  }

  return _.map(val.split(','), function (e) {
    return parseInt(e);
  });
};
