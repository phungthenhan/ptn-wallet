module.exports = (val) => {
  return (typeof val === 'string') && val.length === 32;
};
