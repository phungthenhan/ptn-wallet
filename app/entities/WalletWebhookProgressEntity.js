const _             = require('lodash');
const async         = require('async');
const Const         = require('../common/Const');
const BaseEntity    = require('sota-core').load('entity/BaseEntity');
const Utils         = require('sota-core').load('util/Utils');
const logger        = require('sota-core').getLogger('WalletWebhookProgressEntity');

module.exports = BaseEntity.extends({
  classname: 'WalletWebhookProgressEntity',

  markDone: function (callback) {
    this.isProcessed = Const.BOOLEAN.TRUE;
    this.save(callback);
  },

  markFailed: function (msg, callback) {
    const WalletWebhookProgressModel = this.getModel();
    const WalletWebhookLogModel = this.getModel().getModel('WalletWebhookLogModel');

    async.parallel([
      (next) => {
        WalletWebhookProgressModel.update({
          set: '`retry_count` = `retry_count` + 1, last_try = ?',
          where: 'id = ?',
          params: [Utils.nowInMilis(), this.id],
        }, next);
      },
      (next) => {
        WalletWebhookLogModel.add({
          webhook_progress_id: this.id,
          msg: msg
        }, next);
      }
    ], callback);

  },

});
