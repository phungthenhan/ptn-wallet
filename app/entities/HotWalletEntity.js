const _             = require('lodash');
const BaseEntity    = require('sota-core').load('entity/BaseEntity');

module.exports = BaseEntity.extends({
  classname: 'HotWalletEntity',

  toJSON: function ($super) {
    const data = $super();
    data.privateKey = this.privateKey;
    return data;
  },
});
