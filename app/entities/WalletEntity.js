const _             = require('lodash');
const BaseEntity    = require('sota-core').load('entity/BaseEntity');

module.exports = BaseEntity.extends({
  classname: 'WalletEntity',

  toJSON: function ($super) {
    const data = $super();
    data.receiveAddress = {
      address: data.delegateAddress
    };
    data.delegateAddress = undefined;

    return data;
  },

});
