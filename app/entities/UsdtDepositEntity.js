const _             = require('lodash');
const BaseEntity    = require('sota-core').load('entity/BaseEntity');

module.exports = BaseEntity.extends({
  classname: 'UsdtDepositEntity',

  toJSON: function ($super) {
    const data = $super();
    data.toAddress = data.address;
    return data;
  }

});
