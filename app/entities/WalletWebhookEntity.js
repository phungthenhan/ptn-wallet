const _             = require('lodash');
const async         = require('async');
const request       = require('superagent');
const BaseEntity    = require('sota-core').load('entity/BaseEntity');
const logger        = require('sota-core').getLogger('WalletWebhookEntity');

module.exports = BaseEntity.extends({
  classname: 'WalletWebhookEntity',

  activate: function (callback) {
    logger.info('Webhook activate: ' + JSON.stringify(this.toJSON()));
    const url = this.url;

    request
      .get(url)
      .end((err, response) => {
        if (err) {
          return callback(err);
        }

        return callback(null, response.body);
      });
  },

});
