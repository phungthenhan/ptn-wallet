const _                 = require('lodash');
const ErrorFactory      = require('sota-core').load('error/ErrorFactory');

module.exports = {
  findLatestWithdrawalByStatus: function (status, callback) {
    this.findOne({
      where: 'status = ?',
      params: [status],
      orderBy: "tried_at"
    }, callback);
  }
};
