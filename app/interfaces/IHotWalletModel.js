const async             = require('async');

module.exports = {
  add: function($super, data, options, callback) {
    if (typeof options === 'function') {
      callback = options;
      options = null;
    }

    const WalletModel = this.getModel('WalletModel');

    async.waterfall([
      (next) => {
        WalletModel.findOneOrFail(data.walletId, next);
      },
      (wallet, next) => {
        $super(data, options, next);
      }
    ], callback);
  },
};
