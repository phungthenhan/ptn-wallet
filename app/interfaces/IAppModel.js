const _                 = require('lodash');
const ErrorFactory      = require('sota-core').load('error/ErrorFactory');

module.exports = {
  all: function (callback) {
    this.find({}, (err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, _.map(ret, function(entity) {
        return entity.toJSON();
      }));
    })
  },

  findOneOrFail: function (callback) {
    this.findOne({}, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (!ret) {
        return callback(ErrorFactory.notFound(`There is no existed record`));
      }

      return callback(null, ret);
    })
  },
};
