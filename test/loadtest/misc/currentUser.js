import http from 'k6/http';
import { check } from 'k6';
import { TEST_SERVER_URL } from '../constants.js';
import { loginRequest } from '../commonRequests.js';


export { options } from '../k6options.js';


export function setup() {
  const loginResponse = loginRequest();
  return { token: loginResponse.token, };
};


export default function({ token }) {
  const url = TEST_SERVER_URL + '/api/v2/me';
  const params = {
    headers: {
      'Accept': 'application/json',
      'Authorization': token,
    },
  };

  const res = http.get(url, params);
  check(res, {
    'is user response status 200': (r) => r.status === 200,
  });
};
