import http from 'k6/http';
import { loginRequest } from '../commonRequests.js';


export { options } from '../k6options.js';


export default function() {
  loginRequest();
};
