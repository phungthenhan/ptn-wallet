import { loginRequest, createWalletRequest, getWalletRequest } from '../commonRequests.js';


export { options } from '../k6options.js';


const label = 'label';
const passphrase = 'passphrase';


export function setup() {
  const loginReponse = loginRequest();
  const createWalletReponse = createWalletRequest('eth', loginReponse.token, label, passphrase);

  return { token: loginReponse.token, walletId: createWalletReponse.id, };
};


export default function({ token, walletId }) {
  getWalletRequest('eth', token, walletId);
};
