import { loginRequest, createWalletRequest } from '../commonRequests.js';


export { options } from '../k6options.js';


const label = 'label';
const passphrase = 'passphrase';


export function setup() {
  const loginResponse = loginRequest();
  return { token: loginResponse.token, };
};


export default function({ token }) {
  createWalletRequest('eth', token, label, passphrase);
};
