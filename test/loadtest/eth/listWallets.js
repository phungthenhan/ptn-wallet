import { loginRequest, listWalletRequest } from '../commonRequests.js';


export { options } from '../k6options.js';


export function setup() {
  const loginResponse = loginRequest();
  return { token: loginResponse.token, };
};


export default function({ token }) {
  listWalletRequest('eth', token);
};
