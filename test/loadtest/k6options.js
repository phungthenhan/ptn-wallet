export const options = {
  vus: 20,
  rps: 1000,
  duration: '5m',
  noConnectionReuse: true,
};
