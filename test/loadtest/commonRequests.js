import http from 'k6/http';
import { check } from 'k6';
import { TEST_SERVER_URL, MOCK_DATA } from './constants.js';


export const loginRequest = () => {
  const url = TEST_SERVER_URL + '/api/login';
  const payload = {
    email: MOCK_DATA.USERNAME,
    password: MOCK_DATA.PASSWORD,
  };
  const params = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  };

  const res = http.post(url, payload, params);
  check(res, {
    'is login response status 200': (r) => r.status === 200,
  });

  return res.json();
};


export const listWalletRequest = (coin, token, shouldReturnResponse) => {
  const url = TEST_SERVER_URL + '/api/' + coin + '/wallet';
  const params = {
    headers: {
      'x-auth-token': token,
    },
  };

  const res = http.get(url, params);
  check(res, {
    'is list wallet response status 200': (r) => r.status === 200,
  });

  return res.json();
};


export const createWalletRequest = (coin, token, label, passphrase) => {
  const url = TEST_SERVER_URL + '/api/' + coin + '/wallet/generate';
  const payload = { label, passphrase, };
  const params = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': token,
    },
  };

  const res = http.post(url, payload, params);
  check(res, {
    'is new wallet response status 200': (r) => r.status === 200,
  });

  return res.json();
};


export const getWalletRequest = (coin, token, walletId) => {
  const url = TEST_SERVER_URL + '/api/' + coin + '/wallet/' + walletId;
  const params = {
    headers: {
      'x-auth-token': token,
    },
  };

  const res = http.get(url, params);
  check(res, {
    'is get wallet response status 200': (r) => r.status === 200,
  });

  return res.json();
};
