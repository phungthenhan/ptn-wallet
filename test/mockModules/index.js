const mockery = require('mockery');


mockery.enable({
  warnOnReplace: false,
  warnOnUnregistered: false,
  useCleanCache: true,
});

// eth
mockery.registerMock('web3', require('./web3'));

// dash
mockery.registerMock('@dashevo/dashd-rpc', require('./dashd-rpc'));

// qtum
mockery.registerMock('qtumd-rpc', require('./qtumd-rpc'));

// bitcore-lib
mockery.registerMock('bitcore-lib', require('./bitcore-lib'));
