const bitcore = require('bitcore-lib');


class TransactionMock {
    constructor(hex) {
        this.hash = hex;

        this.from = function() {
            return this;
        };

        this.to = function() {
            return this;
        };

        this.change = function() {
            return this;
        };
    }
}

TransactionMock.UnspentOutput = class UnspentOutput {

}


const bitcoreMock = {
    ...bitcore,
    Transaction: TransactionMock,
};


module.exports = bitcoreMock;
