const RpcClient = require('@dashevo/dashd-rpc');
const mockery = require('mockery');
const _ = require('lodash');


class RpcClientMock extends RpcClient {
  constructor(...params) {
    super(...params);


    this.getNewAddress = function (...params) {
      const fakeAddress = 'yNi5kbW5prg5aKSS9hBsi5PkNWsiJERtJp';
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakeAddress,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }

      return fakeAddress;
    };


    this.dumpPrivKey = function (...params) {
      const fakePrivateKey = 'cRYHtVJHqJNUtYP8pXbJKdEpNacFtQvf6s5V8XWMcERGUA9wLM2e';
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakePrivateKey,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }
      return fakePrivateKey;
    };

    this.getRawTransaction = function (txid, ...params) {
      const fakeResult = {
        "hex": "0100000001f0ede03d75050f20801d50358829ae02c058e8677\
                 d2cc74df51f738285013c26010000006a47304402203c375959\
                 2bf608ab79c01596c4a417f3110dd6eb776270337e575cdafc6\
                 99af20220317ef140d596cc255a4067df8125db7f349ad94521\
                 2e9264a87fa8d777151937012102a92913b70f9fb15a7ea5c42\
                 df44637f0de26e2dad97d6d54957690b94cf2cd05ffffffff01\
                 00ca9a3b0000000017a9149af61346ce0aa2dffcf697352b4b7\
                 04c84dcbaff8700000000",
        "txid": "7278d7d030f042ebe633732b512bcb31fff14a697675a1fe18\
                  84db139876e175",
        "hash": "7278d7d030f042ebe633732b512bcb31fff14a697675a1fe1884db139876e175",
        "size": 189,
        "vsize": 189,
        "version": 1,
        "locktime": 0,
        "vin": [{
          "address": DASH_SEND_ADDRESS,
          "txid": "263c018582731ff54dc72c7d67e858c002ae298835\
                          501d80200f05753de0edf0",
          "vout": 1,
          "scriptSig": {
            "asm": "304402203c3759592bf608ab79c01596c4a417f\
                             3110dd6eb776270337e575cdafc699af2022031\
                             7ef140d596cc255a4067df8125db7f349ad9452\
                             12e9264a87fa8d7771519370102a92913b70f9fb15a7ea5c42df44637f0de26e\
                             2dad97d6d54957690b94cf2cd05",
            "hex": "47304402203c3759592bf608ab79c01596c4a41\
                             7f3110dd6eb776270337e575cdafc699af20220\
                             317ef140d596cc255a4067df8125db7f349ad94\
                             5212e9264a87fa8d777151937012102a92913b7\
                             0f9fb15a7ea5c42df44637f0de26e2dad97d6d5\
                             4957690b94cf2cd05"
          },
          "sequence": 4294967295
        }],
        "vout": [{
          "value": 10.00000000,
          "n": 0,
          "valueSat": 12,
          "scriptPubKey": {
            "asm": "OP_HASH160 9af61346ce0aa2dffcf697352b4b\
                    704c84dcbaff OP_EQUAL",
            "hex": "a9149af61346ce0aa2dffcf697352b4b704c84d\
                             cbaff87",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": [
              DASH_RECEIVE_ADDRESS
            ]
          }
        }]
      };
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => {
          callback(null, {
            result: fakeResult,
            error: null,
            id: getRandomInt(1000, 2000),
          });
        });
      }

      return fakeResult;
    };

    this.listUnspent = function (...params) {
      const fakeData = [{
        "txid": "07b1d3fbc4d78d480d510264bb01bd390e81cda34dcb01e882e94adfa9355d10",
        "vout": 1,
        "address": "32NFcoB96ANoPnqycLH9WUd94o7bwRyq66",
        "account": "",
        "redeemScript": "0014f83a5fdb561c91cd7c4dc0e9fbe225f7f7636b4e",
        "scriptPubKey": "a914076ba75908b2a2e2ebe4b12589be998fdd12148987",
        "amount": 2000000000,
        "confirmations": 4469,
        "spendable": true,
        "solvable": true,
        "safe": true
      }];
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakeData,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }

      return fakeData;
    };

    this.signRawTransaction = function (...params) {
      const fakeData = {
        "hex": "030000807082c4030181a62e2fda07008642d3b8d0e82790bcc98558755698cbfed2d194708d608c31010000006a4730440220602975d9c404299577b94a9da39589ec067910963bf260fbc744ea84908db3c60220072513ba54e9309c59c6ea4f57bcee8acb40d0e6e1a2d1e12496b5f384eb90da012103d6d0db608d40693996e6dd87b28c10ec90fd3b3c3d9c44e470a81f2cc1ce2201ffffffff03a1860100000000001976a9142eca09954ca4411f6f30347b397e33d746adefee88aca1860100000000001976a9148e6ae59827a01e3ff07fa21748d1ee52c06da08588acab40bc11000000001976a914d02ff498b55babb5f649b3287c335e3c73d631d588ac00000000a14c030000",
        "complete": true
      };
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakeData,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }

      return fakeData;
    };
  }
}


module.exports = RpcClientMock;