const RpcClient = require('cc-qtumd-rpc');
const mockery = require('mockery');
const _ = require('lodash');


class RpcClientMock extends RpcClient {
  constructor(...params) {
    super(...params);


    this.getNewAddress = function (...params) {
      const fakeAddress = 'QMa9Gna2gzocRBdgTnHZTW5PuUJ2VYimkC';
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakeAddress,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }

      return fakeAddress;
    };


    this.dumpPrivKey = function (...params) {
      const fakePrivateKey = 'KwEy3izNyzBDb7HpXex4eecJvrK1n2oAcVwnFjnZecWWq3UDqWF9';
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => callback(null, {
          result: fakePrivateKey,
          error: null,
          id: getRandomInt(1000, 2000),
        }));
      }

      return fakePrivateKey;
    };

    this.getRawTransaction = function (txid, ...params) {
      const fakeResult = {
        "txid": txid,
        "hash": "f5fd9f3ff9c3d62eebfc9b112240ed67f8a493cff910c3f46cf0c8ab2475bdcc",
        "version": 2,
        "size": 482,
        "vsize": 482,
        "locktime": 0,
        "vin": [{
          "txid": "c0a0e0d103716f51a8c24d89fc42b78e0444b450ee666bd6593098e08a3ad8e3",
          "vout": 1,
          "scriptSig": {
            "asm": "3044022022ff80559afa1c09e5c29473ad48fb17ba22672151353cef94dcd4a3c5778827022015991d6206c8cd29244bc4a9a65793d386651d3dbac2bd027f38d0a964a11dbc[ALL]",
            "hex": "473044022022ff80559afa1c09e5c29473ad48fb17ba22672151353cef94dcd4a3c5778827022015991d6206c8cd29244bc4a9a65793d386651d3dbac2bd027f38d0a964a11dbc01"
          },
          "sequence": 4294967295,
          "address": QTUM_SEND_ADDRESS
        }],
        "vout": [{
          "value": 145.74838572,
          "n": 1,
          "scriptPubKey": {
            "asm": "0274c834f66294102b3009febd903a3e75adb3c2e0b081055516a3497acd1ff804 OP_CHECKSIG",
            "hex": "210274c834f66294102b3009febd903a3e75adb3c2e0b081055516a3497acd1ff804ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": [QTUM_SEND_ADDRESS]
          }
        }, {
          "value": 0.4,
          "n": 2,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 2e10fe88d6e075ad44a42f5c941599240aeba5fb OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a9142e10fe88d6e075ad44a42f5c941599240aeba5fb88ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": [QTUM_SEND_ADDRESS]
          }
        }, {
          "value": 0.4,
          "n": 3,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 edf2d506a7ba1966b858631accf8de3da1575556 OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914edf2d506a7ba1966b858631accf8de3da157555688ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qfFYCmU7ACsDUbor4Do9AP1XxkocEKhs3z"]
          }
        }, {
          "value": 0.4,
          "n": 4,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 8316e41fc0fbb2c9c56ee6e971c697629c22d29c OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a9148316e41fc0fbb2c9c56ee6e971c697629c22d29c88ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qVWX6fRUpbYZt5tLnJJ6uTim2mqDP5nv5D"]
          }
        }, {
          "value": 0.4,
          "n": 5,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 a1bd7f3b948aa9a77c6486ad5f1ebfe247fab4d4 OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914a1bd7f3b948aa9a77c6486ad5f1ebfe247fab4d488ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qYJaxQ579UNUiV8AnjP97ATnvvChFZmLLv"]
          }
        }, {
          "value": 0.4,
          "n": 6,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 ca01e35383979bdec15ce10a88cbdc6219ab3b09 OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914ca01e35383979bdec15ce10a88cbdc6219ab3b0988ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qbyVuxhRy4Y4dgRk2McVVEwqDVao73MrjV"]
          }
        }, {
          "value": 0.4,
          "n": 7,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 2e10fe88d6e075ad44a42f5c941599240aeba5fb OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a9142e10fe88d6e075ad44a42f5c941599240aeba5fb88ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qMkxbdz4kLXe4cMUn7vhhz2BPLXyCmMKn6"]
          }
        }, {
          "value": 0.4,
          "n": 8,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 795607d424e0b6091b187cfa4a31a5b7a5967916 OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914795607d424e0b6091b187cfa4a31a5b7a596791688ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qUcwykb7UstkWB2cLtaGjY19bfUjoxLaZS"]
          }
        }, {
          "value": 0.4,
          "n": 9,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 b29ec1e265ccb28ad6da4451eb9f275dcebc0226 OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914b29ec1e265ccb28ad6da4451eb9f275dcebc022688ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qZqqcqCsVtP2U38WWaUnwshHRpefvCa8hX"]
          }
        }, {
          "value": 0.4,
          "n": 10,
          "scriptPubKey": {
            "asm": "OP_DUP OP_HASH160 39d41c6c7c944c196852928721ad2e623442e9ba OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a91439d41c6c7c944c196852928721ad2e623442e9ba88ac",
            "reqSigs": 1,
            "type": "pubkeyhash",
            "addresses": ["qNq9mhTgH7KzKKDDwQ87Ain7mtyktheXyX"]
          }
        }],
        "hex": "0200000001e3d83a8ae0983059d66b66ee50b444048eb742fc894dc2a8516f7103d1e0a0c00100000048473044022022ff80559afa1c09e5c29473ad48fb17ba22672151353cef94dcd4a3c5778827022015991d6206c8cd29244bc4a9a65793d386651d3dbac2bd027f38d0a964a11dbc01ffffffff0b0000000000000000002c63ba640300000023210274c834f66294102b3009febd903a3e75adb3c2e0b081055516a3497acd1ff804ac005a6202000000001976a9142e10fe88d6e075ad44a42f5c941599240aeba5fb88ac005a6202000000001976a914edf2d506a7ba1966b858631accf8de3da157555688ac005a6202000000001976a9148316e41fc0fbb2c9c56ee6e971c697629c22d29c88ac005a6202000000001976a914a1bd7f3b948aa9a77c6486ad5f1ebfe247fab4d488ac005a6202000000001976a914ca01e35383979bdec15ce10a88cbdc6219ab3b0988ac005a6202000000001976a9142e10fe88d6e075ad44a42f5c941599240aeba5fb88ac005a6202000000001976a914795607d424e0b6091b187cfa4a31a5b7a596791688ac005a6202000000001976a914b29ec1e265ccb28ad6da4451eb9f275dcebc022688ac005a6202000000001976a91439d41c6c7c944c196852928721ad2e623442e9ba88ac00000000",
        "blockhash": "d988844b55a19a154d7f202f302b84c1c1eb34a592513204ac55ef81fd34c4fc",
        "confirmations": 21,
        "time": 1530587888,
        "blocktime": 1530587888
      };
      const callback = _.last(params);

      if (typeof callback === 'function') {
        process.nextTick(() => {
          callback(null, {
            result: fakeResult,
            error: null,
            id: getRandomInt(1000, 2000),
          });
        });
      }

      return fakeResult;
    };
  }
}


module.exports = RpcClientMock;