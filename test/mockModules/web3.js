const web3 = require('web3');
const mockery = require('mockery');
const _ = require('lodash');
const BigNumber = require('bignumber.js');


class web3Mock extends web3 {
  constructor(...params) {
    super(...params);


    this.eth.getTransaction = function(transactionHash, callback) {
      const fakeData = {
        "hash": transactionHash,
        "nonce": 2,
        "blockHash": "0xef95f2f1ed3ca60b048b4bf67cde2195961e0bba6f70bcbea9a2c4e133e34b46",
        "blockNumber": 3,
        "transactionIndex": 0,
        "from": ETH_SEND_ADDRESS,
        "to": ETH_RECEIVE_ADDRESS,
        "value": 123,
        "gas": 314159,
        "gasPrice": 13,
        "input": "0x57cb2fc4"
      };

      if (callback) {
        process.nextTick(() => callback(null, fakeData));
      }

      return fakeData;
    }


    this.eth.getTransactionReceipt = function(hashString, callback) {
      const fakeData = {
        "transactionHash": hashString,
        "transactionIndex": 0,
        "blockHash": "0xef95f2f1ed3ca60b048b4bf67cde2195961e0bba6f70bcbea9a2c4e133e34b46",
        "blockNumber": 3,
        "contractAddress": ETH_SEND_ADDRESS,
        "cumulativeGasUsed": 314159,
        "gasUsed": 30234,
        "logs": [],
        "status": "0x1"
      };

      if (callback) {
        process.nextTick(() => callback(null, fakeData));
      }

      return fakeData;
    };


    this.eth.getBlockNumber = function(callback) {
      if (callback) {
        process.nextTick(() => callback(null, 1000));
      }

      return 1000;
    };


    this.eth.getBlock = function(blockHashOrBlockNumber,callback) {
      const fakeData = {
        "number": 3,
        "hash": "0xef95f2f1ed3ca60b048b4bf67cde2195961e0bba6f70bcbea9a2c4e133e34b46",
        "parentHash": "0x2302e1c0b972d00932deb5dab9eb2982f570597d9d42504c05d9c2147eaf9c88",
        "nonce": "0xfb6e1a62d119228b",
        "sha3Uncles": "0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347",
        "logsBloom": "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
        "transactionsRoot": "0x3a1b03875115b79539e5bd33fb00d8f7b7cd61929d5a3c574f507b8acf415bee",
        "stateRoot": "0xf1133199d44695dfa8fd1bcfe424d82854b5cebef75bddd7e40ea94cda515bcb",
        "miner": "0x8888f1f195afa192cfee860698584c030f4c9db1",
        "difficulty": '21345678965432',
        "totalDifficulty": '324567845321',
        "size": 616,
        "extraData": "0x",
        "gasLimit": 3141592,
        "gasUsed": 21662,
        "timestamp": 1429287689,
        "transactions": [
            "0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b"
        ],
        "uncles": []
      };

      if (callback) {
        process.nextTick(() => callback(null, fakeData));
      }

      return fakeData;
    };


    this.eth.getBalance = function(addressHexString, ...params) {
      const fakeBalance = new BigNumber(10000000000000000000);
      const callback = _.last(params);

      if (typeof callback === 'function') {
        callback(null, fakeBalance.plus(21).toString(10));
      }

      return fakeBalance.plus(21).toString(10);
    };

    this.eth.getGasPrice = function (...params) {
      const fakeBalance = new BigNumber(1);
      const callback = _.last(params);

      if (typeof callback === 'function') {
        callback(null, fakeBalance.toString(10));
      }

      return fakeBalance.toString(10);
    };
  }
}


module.exports =  web3Mock;
