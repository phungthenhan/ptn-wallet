const _         = require('lodash');
const util      = require('util');
const request   = require('superagent');
const async     = require('async');
const assert    = require('assert');

global.getUrl = (route) => {
  const port = process.env.PORT;
  if (route.startsWith('/')) {
    route = route.slice(1);
  }

  return util.format(`http://localhost:${port}/api/${route}`);
}

global.getErrorText = (err) => {
  if (err && err.response && err.response.text) {
    return err.response.text;
  }

  return err.toString();
}

global.loginWithoutFail = (credentials, callback) => {
  request
    .post(getUrl('/login'))
    .send(credentials)
    .end((err, res) => {
      if (err) throw err;

      assert(!_.isNil(res.body));

      // Got valid user, without exposed password
      const user = res.body;
      assert(user.id > 0);
      assert(!user.password);

      // Got access token
      assert(!_.isEmpty(user.token));

      return callback(null, user);
    });
};

global.loginToGetTokens = (loginDefs, callback) => {
  async.forEach(loginDefs, (loginDef, next) => {
    let email, password;
    if (typeof loginDef === 'string') {
      email = loginDef
      password = '1'
    } else {
      email = loginDef.email
      password = loginDef.password
    }

    const credentials = { email, password };

    loginWithoutFail(credentials, (err, user) => {
      if (err) throw err;
      G_AUTH_TOKENS[user.id] = user.token;
      return next(null, null);
    });

  }, callback);
};

global.getListWallets = (userId, coin, params, callback) => {
  request
    .get(getUrl(`/${coin}/wallet`))
    .set('x-auth-token', G_AUTH_TOKENS[userId])
    .send(params)
    .end((err, res) => {
      if (err) return callback(err);

      if (!res.body.wallets) {
        throw new Error(`Invalid format response data: ${res.body}`);
      }

      return callback(null, res.body.wallets);
    });
};

global.isEmpty = (value) => {
  return value === undefined || value === null;
};

global.isNotEmpty = (value) => {
  return !isEmpty(value);
};

global.getRandomFloat = (min, max) => {
  return Math.random() * (max - min) + min;
};

global.getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
