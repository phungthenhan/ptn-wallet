/* eslint no-multi-spaces: ["error", { exceptions: { "VariableDeclarator": true } }] */
require('dotenv').config();

process.env.PORT = process.env.TEST_SERVER_PORT;

const path          = require('path');
const util          = require('util');
const _             = require('lodash');
const async         = require('async');
const assert        = require('assert');
const request       = require('superagent');
const requireDir    = require('require-directory');
const mockery       = require('mockery')

let middlewares = require('../config/Middlewares');
_.pull(middlewares.list, 'requestLogger');

const SotaCore      = require('sota-core');

const redis         = SotaCore.redis.createClient({
  host: process.env.REDIS_SERVER_ADDRESS,
  port: process.env.REDIS_SERVER_PORT
});

const logConfig = {
  appenders: {
    out: {
      type: 'console',
      level: process.env.LOG_LEVEL || 'WARN'
    },
    err: {
      type: 'console'
    }
  },
  categories: {
    default: {
      appenders: ['out'],
      level: process.env.LOG_LEVEL || 'WARN'
    },
    err: {
      appenders: ['err'],
      level: 'ERROR'
    }
  }
};
const log4js        = SotaCore.configureLogger(logConfig);
const queryLogger   = SotaCore.getLogger('Query');
const logger        = SotaCore.getLogger('Test');

const knexConfig = {
  client: 'mysql',
  connection:
  {
    host: process.env.MYSQL_DB_HOST_TEST,
    user: process.env.MYSQL_USERNAME_TEST,
    database: process.env.MYSQL_DBNAME_TEST,
    password: process.env.MYSQL_PASSWORD_TEST,
    port: process.env.MYSQL_DB_PORT_TEST,
    charset: 'utf8'
  },
  migrations: {
    directory: './db/mysql/migrations_knex'
  },
  seeds: {
    directory: './db/mysql/seeds/test'
  }
};
const knex            = require('knex')(knexConfig);
knex.on('query', (data) => {
  queryLogger.trace(knex.raw(data.sql, data.bindings).toString());
});

global.G_AUTH_TOKENS = {};
// Load global configuration
requireDir(module, './config', { recurse: true });

require('./funcs');

process.on('uncaughtException', (err) => {
  logger.error(`======= BEGIN UNCAUGHTEXCEPTION =======`);
  logger.error(getErrorText(err));
  logger.error(err.stack);
  logger.error(`======= END UNCAUGHTEXCEPTION =======`);
  process.nextTick(() => {
    process.exit(1);
  });
});

before((done) => {
  require('./mockModules');

  logger.fatal('Setting up the test DB before running tests...')
  knex.migrate.rollback()
    .then(makeDBSchema)
    .then(makeDBSeeds)
    .then(startServer)
    .then(loginAllAccount)
    .then(() => {
      done();
    })
    .catch((err) => {
      logger.error(err);
    });
});

afterEach(cleanDBAndFlushCache);

require('./apitest');
require('./end/Example.spec.js');

function makeDBSchema () {
  logger.fatal('Migrate latest DB schema...');
  return knex.migrate.latest();
}

function makeDBSeeds () {
  logger.fatal('Creating DB seeds...')
  return knex.seed.run();
}

function resetIncrement () {
  return knex.raw('ALTER TABLE user AUTO_INCREMENT=1000001');
}

function startServer () {
  return new Promise(function (resolve, reject) {
    logger.fatal('Starting server...')
    const app = SotaCore.createServer({
      rootDir: path.join(path.resolve('.', 'server'))
    });
    app.start();

    setTimeout(() => {
      resolve(true);
    }, 1000);
  });
}

function loginAllAccount () {
  return new Promise(function (resolve, reject) {
    logger.fatal('Logging in to all accounts to get auth tokens...');
    const accountDefs = ['admin'];

    loginToGetTokens(accountDefs, (err, ret) => {
      if (err) return reject(err);
      logger.fatal('Authorized tokens: ' + JSON.stringify(G_AUTH_TOKENS));
      resolve(ret);
    });
  })
}

function cleanDBAndFlushCache (done) {
  logger.info('Cleaning the test DB after each test...')
  knex.seed.run()
    .then(resetIncrement)
    .then(() => {
      redis.flushdb(done);
    })
    .catch(function (err) {
      logger.warn('Got error: ' + err.toString() + '. Retrying...')
      cleanDBAndFlushCache(done);
    });
}
