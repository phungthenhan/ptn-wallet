process.env.DASH_NETWORK='testnet';
// default account
global.DEFAULT_USERNAME = 'admin';
global.DEFAULT_PASSWORD = '1';


// eth
global.ETH_RECEIVE_ADDRESS = '0xE401117EAd2b4a8Abf4eB99950CC350ce1e0457A';
global.ETH_SEND_ADDRESS = '0xA0631a5beFf3509A7dFDfD09caDcC836bb09B483';
global.ETH_DEFAULT_WALLET_ID = 2001;


// dash
global.DASH_RECEIVE_ADDRESS = 'yb21342iADyqAotjwcn4imqjvAcdYhnzeH';
global.DASH_SEND_ADDRESS = 'yNi5kbW5prg5aKSS9hBsi5PkNWsiJERtJp';
global.DASH_DEFAULT_WALLET_ID = 1001;


// qtum
global.QTUM_RECEIVE_ADDRESS = 'qJ8E9gTRmWxnPnKXgNKQQoAL8P44stFNYG';
global.QTUM_SEND_ADDRESS = 'QWgdd4W4LNsKUumNX9ShmCd1ZxqdJdXfFH';
global.QTUM_DEFAULT_WALLET_ID = 4001;


// btc
global.BTC_RECEIVE_ADDRESS = 'mvTnhjENj5GDu29TrXVSSVzbovAgf5Gdik';
global.BTC_SEND_ADDRESS = '1HUG3qHBthPb2CpknyyktfiyeLbUxBqh7a';
global.BTC_DEFAULT_WALLET_ID = 5001;

