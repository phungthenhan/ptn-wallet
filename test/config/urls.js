const TEST_SERVER_URL = `${process.env.TEST_SERVER_HOST || 'localhost'}:${process.env.TEST_SERVER_PORT || 9002}`;

global.URLS = {
  TEST_SERVER_URL,

  // eth
  ETH_WALLET_API_URL: TEST_SERVER_URL + '/api/eth/wallet/',

  // dash
  DASH_WALLET_API_URL: TEST_SERVER_URL + '/api/dash/wallet/',

  // qtum
  QTUM_WALLET_API_URL: TEST_SERVER_URL + '/api/qtum/wallet/',

  // btc
  BTC_WALLET_API_URL: TEST_SERVER_URL + '/api/btc/wallet/',

  // login
  LOGIN_API_URL: TEST_SERVER_URL + '/api/login',

  // user
  USER_API_URL: TEST_SERVER_URL + '/api/v2/me',
};
