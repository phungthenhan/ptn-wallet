/**
 * Wallet
 */

// eth
require('./wallet/eth/1.createWallet');
require('./wallet/eth/2.getOneWallet');
require('./wallet/eth/3.listWallets');
require('./wallet/eth/4.createWalletAddress');
require('./wallet/eth/5.sendTransaction');
require('./wallet/eth/6.getTransactionDetail');

// dash
require('./wallet/dash/1.createWallet');
require('./wallet/dash/2.getOneWallet');
require('./wallet/dash/3.listWallets');
require('./wallet/dash/4.createWalletAddress');
require('./wallet/dash/5.sendTransaction');
require('./wallet/dash/6.getTransactionDetail');

// qtum
require('./wallet/qtum/1.createWallet');
require('./wallet/qtum/2.getOneWallet');
require('./wallet/qtum/3.listWallets');
require('./wallet/qtum/4.createWalletAddress');
require('./wallet/qtum/5.sendTransaction');
require('./wallet/qtum/6.getTransactionDetail');

// btc
require('./wallet/btc/1.createWallet');
require('./wallet/btc/2.getOneWallet');
require('./wallet/btc/3.listWallets');
require('./wallet/btc/4.createWalletAddress');
require('./wallet/btc/5.sendTransaction');
require('./wallet/btc/6.getTransactionDetail');




/**
 * Misc api
 */

// login
require('./misc/login');

// get current user
require('./misc/currentUser');
