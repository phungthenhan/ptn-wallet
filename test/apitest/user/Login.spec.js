/* global describe it getUrl */
const _         = require('lodash');
const assert    = require('assert');
const async     = require('async');
const request   = require('superagent');

describe('Check login APIs', function () {
  it('Local login', function (done) {
    async.waterfall([
      (next) => {
        loginWithoutFail({
          email: 'admin',
          password: '1'
        }, next);
      }
    ], (err, user) => {
      if (err) throw err;

      assert.equal(user.id, 1);

      done();
    });

  });

});
