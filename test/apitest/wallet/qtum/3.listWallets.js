const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');
const _ = require('lodash');


const MockData = {
  coin: 'qtum',
  requestParams: {
    label: 'testLabel',
    passphrase: 'testPassphrase',
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
    'x-auth-token': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    wallets: ['required', 'array']
  },
  walletPrototype: {
    'id': ['required', 'number'],
    'userId': ['required', 'number'],
    'label': ['required', 'string'],
    'coin': ['required', 'string'],
    'receiveAddress': ['required', 'object'],
  },
};

// Return a function to reset request data
const CreateWalletRequest = () => {
  return request
    .post(URLS.QTUM_WALLET_API_URL + 'generate')
    .send({
      label: MockData.requestParams.label,
      passphrase: MockData.requestParams.passphrase,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const ListWalletsRequest = () => {
  return request
    .get(URLS.QTUM_WALLET_API_URL)
    .set('x-auth-token', MockData.requestHeader['x-auth-token']());
};



describe('III. QTUM wallet - List wallets service', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // Correct request
    it('1.1 Correct request - should return 200', (done) => {
      ListWalletsRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });

    // Add new wallet
    it('1.2 Add new wallet - should return new wallet', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          ListWalletsRequest()
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send list of wallets');

              const body = res.body;
              const [responseError] = (new Checkit(MockData.responsePrototype)).validateSync(body);
              assert.ok(isEmpty(responseError), 'Server should send correct format data');

              const createdWallet = _.find(body.wallets, { id: newWallet.id });
              assert.ok(isNotEmpty(createdWallet), 'Response should contain new wallet');
              const [responseItemError] = (new Checkit(MockData.walletPrototype)).validateSync(createdWallet);
              assert.ok(isEmpty(responseItemError), 'Server should send correct wallet format');

              assert.equal(createdWallet.id, newWallet.id);
              assert.equal(createdWallet.userId, newWallet.userId);
              assert.equal(createdWallet.label, newWallet.label);
              assert.equal(createdWallet.coin, newWallet.coin);

              done();
            });
        });
    });

  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "x-auth-token" - should return 401', (done) => {
      ListWalletsRequest()
        .set('x-auth-token', 'Wrong token')
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 2.2 Missing header 'x-auth-token'
    it('2.2 Missing header "x-auth-token" - should return 401', (done) => {
      ListWalletsRequest()
        .set('x-auth-token', null)
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 2.3 Valid header "x-auth-token"
    it('2.3 Valid header "x-auth-token" - should return 200', (done) => {
      ListWalletsRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });

  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Few wallets
    it (`3.1 Response time with few wallets - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      ListWalletsRequest()
        .timeout({
          response: CONSTRAINTS.RESPONSE_TIME,
        })
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });

    // Lots of wallets
    // it (`3.2 Response time with lots of wallets - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
    //   const largeCreateWalletRequests = _.map(new Array(10000), () => CreateWalletRequest());
    //   Promise
    //     .all(largeCreateWalletRequests)
    //     .then(() => {
    //       ListWalletsRequest()
    //         .timeout({
    //           response: CONSTRAINTS.RESPONSE_TIME,
    //         })
    //         .end((err, res) => {
    //           assert.ok(res, 'Server should send successful response');

    //           const status = res.status;
    //           assert.equal(status, 200);

    //           done();
    //         });
    //     })
    // });
  });

});
