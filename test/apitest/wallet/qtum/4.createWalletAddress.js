const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');
const _ = require('lodash');


const MockData = {
  coin: 'qtum',
  requestParams: {
    label: 'testLabel',
    passphrase: 'testPassphrase',
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
    'x-auth-token': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    'id': ['required', 'number'],
    'walletId': ['required', 'number'],
    'address': ['required', 'string'],
  },
};

const CreateWalletRequest = () => {
  return request
    .post(URLS.QTUM_WALLET_API_URL + 'generate')
    .send({
      label: MockData.requestParams.label,
      passphrase: MockData.requestParams.passphrase,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const CreateWalletAddressRequest = (walletId) => {
  return request
    .post(URLS.QTUM_WALLET_API_URL + walletId + '/address')
    .send({
      label: MockData.requestParams.label,
      passphrase: MockData.requestParams.passphrase,
    })
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const GetWalletAddressRequest = (walletId) => {
  return request
    .get(URLS.QTUM_WALLET_API_URL + walletId + '/addresses')
    .set('x-auth-token', MockData.requestHeader['x-auth-token']());
};


describe('IV. QTUM wallet - Create wallet address', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // Check if create wallet was stored in database
    it('1.1 Correct wallet id - should send correct response', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send new wallet');

              const body = res.body;
              const [error] = (new Checkit(MockData.responsePrototype)).validateSync(body);
              assert.ok(isEmpty(error), 'Server should send correct format data');

              assert.equal(body.walletId, newWallet.id);

              done();
            });
        });
    });

    // Check database
    it('1.2 Correct wallet id - An address should be saved in database', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send new address');

              const newAddress = res.body;

              GetWalletAddressRequest(newWallet.id)
                .end((err, res) => {
                  assert.ok(res, 'Server should send successful response');
                  assert.ok(res.body, 'Server should send wallet\'s addresses');
                  assert.ok(res.body.addresses, 'Server should send wallet\'s addresses');

                  const walletAddresses = res.body.addresses;
                  const createdAddress = _.find(walletAddresses, { id: newAddress.id });

                  assert.equal(createdAddress.walletId, newAddress.walletId);
                  assert.equal(createdAddress.address, newAddress.address);

                  done();
                });
            });
        });
    });

    // Wrong wallet id
    it('1.3 Wrong wallet id - should return 404', (done) => {
      CreateWalletAddressRequest('wrongWalletId')
        .end((err, res) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 404);

          done();
        });
    });

  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "Authorization" - should return 401', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .set('Authorization', 'Wrong token')
            .end((err) => {
              assert.ok(err, 'Server should send error response');

              const status = err.status;
              assert.equal(status, 401);

              done();
            });
        });
    });

    // 2.2 Missing header 'Authorization'
    it('2.2 Missing header "Authorization" - should return 401', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .set('Authorization', null)
            .end((err) => {
              assert.ok(err, 'Server should send error response');

              const status = err.status;
              assert.equal(status, 401);

              done();
            });
        });
    });

    // 2.3 Valid header "Authorization"
    it('2.3 Valid header "Authorization" - should return 200', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });

  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          CreateWalletAddressRequest(newWallet.id)
            .timeout({
              response: CONSTRAINTS.RESPONSE_TIME,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });
  });

});
