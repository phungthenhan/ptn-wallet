const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  coin: 'eth',
  requestParams: {
    label: 'testLabel',
    passphrase: 'testPassphrase',
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
    'x-auth-token': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    'id': ['required', 'number'],
    'userId': ['required', 'number'],
    'label': ['required', 'string'],
    'coin': ['required', 'string'],
    'receiveAddress': ['required', 'object'],
    'balance': ['required', 'number'],
    'balanceString': ['required', 'string'],
  },
};

// Return a function to reset request data
const CreateWalletRequest = () => {
  return request
    .post(URLS.ETH_WALLET_API_URL + 'generate')
    .send({
      label: MockData.requestParams.label,
      passphrase: MockData.requestParams.passphrase,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const GetWalletRequest = (walletId) => {
  return request
    .get(URLS.ETH_WALLET_API_URL + walletId)
    .set('x-auth-token', MockData.requestHeader['x-auth-token']());
};


describe('II. ETH wallet - Get one wallet service', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // Check if create wallet was stored in database
    it('1.1 Correct wallet id - should send correct response', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send new wallet');

              const body = res.body;
              const [error] = (new Checkit(MockData.responsePrototype)).validateSync(body);
              assert.ok(isEmpty(error), 'Server should send correct format data');

              assert.equal(body.id, newWallet.id);
              assert.equal(body.userId, newWallet.userId);
              assert.equal(body.label, newWallet.label);
              assert.equal(body.coin, newWallet.coin);
              assert.equal(body.balance, 0);
              assert.equal(body.balanceString, '0');

              done();
            });
        });
    });

    // Wrong wallet id
    it('1.2 Wrong wallet id - should return 404', (done) => {
      GetWalletRequest('wrongWalletId')
        .end((err, res) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 404);

          done();
        });
    });

  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "x-auth-token" - should return 401', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .set('x-auth-token', 'Wrong token')
            .end((err) => {
              assert.ok(err, 'Server should send error response');

              const status = err.status;
              assert.equal(status, 401);

              done();
            });
        });
    });

    // 2.2 Missing header 'x-auth-token'
    it('2.2 Missing header "x-auth-token" - should return 401', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .set('x-auth-token', null)
            .end((err) => {
              assert.ok(err, 'Server should send error response');

              const status = err.status;
              assert.equal(status, 401);

              done();
            });
        });
    });

    // 2.3 Valid header "x-auth-token"
    it('2.3 Valid header "x-auth-token" - should return 200', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });

  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .timeout({
              response: CONSTRAINTS.RESPONSE_TIME,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });
  });

});
