const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  coin: 'eth',
  walletId: ETH_DEFAULT_WALLET_ID,
  requestParams: {
    address: ETH_RECEIVE_ADDRESS,
    amount: '0',
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    txid: ['required', 'string'],
    status: ['required', 'string'],
  },
};

// Return a function to reset request data
const SendTransactionRequest = (walletId) => {
  return request
    .post(URLS.ETH_WALLET_API_URL + walletId + '/sendcoins')
    .send({
      address: MockData.requestParams.address,
      amount: MockData.requestParams.amount,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const GetWalletRequest = (walletId) => {
  return request
    .get(URLS.ETH_WALLET_API_URL + walletId)
    .set('x-auth-token', MockData.requestHeader['Authorization']());
};

const GetTransactionDetailRequest = (walletId, txid) => {
  return request
    .get(URLS.ETH_WALLET_API_URL + walletId + '/tx/' + txid);
};


describe('V. ETH wallet - Send transaction service', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // 1.1 Correct request
    it('1.1 Correct request - should send status 200', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });

    // Correct request
    it('1.2 Correct request - should correct response', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');
              assert.ok(res.body, 'Server should send response');

              const transaction = res.body;
              const [transactionError] = (new Checkit(MockData.responsePrototype)).validateSync(transaction);
              assert.ok(isEmpty(transactionError), 'Server should send correct response format');

              done();
            });
        });
    });

    // Correct request
    it('1.3 Correct request - can get transaction from network', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');
              assert.ok(res.body, 'Server should send response');

              const newTransaction = res.body;

              GetTransactionDetailRequest(MockData.walletId, newTransaction.txid)
                .end((err, res) => {
                  assert.ok(res, 'Server should transaction detail');
                  assert.ok(res.body, 'Server should transaction detail');

                  const transactionDetail = res.body;
                  // const entries = transactionDetail.entries;
                  // for (let [index, entry] of entries.entries()) {
                  //   if (index === 0) { // First item is send address
                  //     assert.equal(entry.address, ETH_SEND_ADDRESS);
                  //   } else {
                  //     assert.equal(entry.address, ETH_RECEIVE_ADDRESS);
                  //   }
                  //   assert.equal(entry.wallet, MockData.walletId);
                  // }
                  done();
                });
            });
        });
    });

    // Invalid request - Missing "address"
    it('1.4 Invalid request - Missing "address" - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: null,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // Invalid request - Missing "amount"
    it('1.5 Invalid request - Missing "amount" - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: null,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // Invalid request - Invalid "address"
    it('1.6 Invalid request - Invalid "address" - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: 'Invalid address',
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // Invalid request - Amount is under 0
    it('1.7 Invalid request - Amount is under 0 - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: -10,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // Invalid request - Amount is not a number
    it('1.8 Invalid request - Amount is not a number - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: transactionAmount + 'number',
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // Invalid request - Amount is over balance
    it('1.9 Invalid request - Amount is over balance - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const BigNumber = require('bignumber.js');
          const balance = new BigNumber(res.body.balance);

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: balance.plus(1).toString(),
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 400);

              done();
            });
        });
    });

    // 1.10 Invalid request - Amount is a float number
    it('1.10 Invalid request - should send status 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000.5005;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: transactionAmount,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');

              const status = res.status;
              assert.equal(status, 400);

              done();
            });
        });
    });
  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "Authentication" - should return 401', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .set('Authorization', 'Invalid token')
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(err), 'Server should return error response');

              const status = err.status;
              assert.equal(status, 401);

              done();
            });
        });
    });


    // 2.2 Valid header "Authorization"
    it('2.2 Valid header "Authorization" - should return 200', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(res), 'Server should return successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });
  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .timeout({
              response: CONSTRAINTS.RESPONSE_TIME * 10,
            })
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(isNotEmpty(res), 'Server should return successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });
  });

});
