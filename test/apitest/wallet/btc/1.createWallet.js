const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  coin: 'btc',
  requestParams: {
    label: 'testLabel',
    passphrase: 'testPassphrase',
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    'id': ['required', 'number'],
    'userId': ['required', 'number'],
    'label': ['required', 'string'],
    'coin': ['required', 'string'],
    'receiveAddress': ['required', 'object'],
  },
};

// Return a function to reset request data
const CreateWalletRequest = () => {
  return request
    .post(URLS.BTC_WALLET_API_URL + 'generate')
    .send({
      label: MockData.requestParams.label,
      passphrase: MockData.requestParams.passphrase,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const GetWalletRequest = (walletId) => {
  return request
    .get(URLS.BTC_WALLET_API_URL + walletId)
    .set('x-auth-token', MockData.requestHeader['Authorization']());
};


describe('I. BTC wallet - Create new wallet service', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // 1.1 Missing "label" and "passphrase" params
    it('1.1 Missing "label" and "passphrase" params - should return 400', (done) => {
      CreateWalletRequest()
        .send({
          label: '',
          passphrase: '',
        })
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 400);

          done();
        });
    });

    // 1.2 Missing "label" param
    it('1.2 Missing "label" param - should return 400', (done) => {
      CreateWalletRequest()
        .send({
          label: '',
          passphrase: MockData.requestParams.passphrase,
        })
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 400);

          done();
        });
    });

    // 1.3 Missing "passphrase" param
    it('1.3 Missing "passphrase" param - should return 400', (done) => {
      CreateWalletRequest()
        .send({
          label: MockData.requestParams.label,
          passphrase: '',
        })
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 400);

          done();
        });
    });

    // 1.4 Addiction request params
    it('1.4 Addiction request params - should return 400', (done) => {
      CreateWalletRequest()
        .send({
          label: MockData.requestParams.label,
          passphrase: MockData.requestParams,
          addictionParam1: 'addictionParam1',
          addictionParam2: 'addictionParam2',
        })
        .end((err, res) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 400);

          done();
        });
    });

    // Correct request
    it('1.5 Correct request - Should return correct response', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const body = res.body;

          const [error] = (new Checkit(MockData.responsePrototype)).validateSync(body);
          assert.ok(isEmpty(error), 'Server should send correct format data');

          assert.equal(body.label, MockData.requestParams.label);
          assert.equal(body.coin, MockData.coin);

          done();
        });
    });

    // Check if create wallet was stored in database
    it('1.6 Correct request - A new wallet and new wallet\'s balance should be created in database', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send new wallet');

          const newWallet = res.body;

          GetWalletRequest(newWallet.id)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send new wallet');

              const body = res.body;

              assert.equal(body.id, newWallet.id);
              assert.equal(body.userId, newWallet.userId);
              assert.equal(body.label, newWallet.label);
              assert.equal(body.coin, newWallet.coin);
              assert.equal(body.balance, 0);
              assert.equal(body.balanceString, '0');

              done();
            });
        });
    });

  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "Authorization" - should return 401', (done) => {
      CreateWalletRequest()
        .set('Authorization', 'Wrong token')
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 2.2 Missing header 'Authorization'
    it('2.2 Missing header "Authorization" - should return 401', (done) => {
      CreateWalletRequest()
        .set('Authorization', null)
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 2.3 Valid header "Authorization"
    it('2.3 Valid header "Authorization" - should return 200', (done) => {
      CreateWalletRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });

  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      CreateWalletRequest()
        .timeout({
          response: CONSTRAINTS.RESPONSE_TIME,
        })
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });
  });

});
