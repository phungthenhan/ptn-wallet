/* global describe it getUrl */
const _         = require('lodash');
const assert    = require('assert');
const async     = require('async');
const request   = require('superagent');

describe('Get List DASH Wallets', function () {
  it('Get all DASH wallets in one page', function (done) {
    async.waterfall([
      (next) => {
        const params = {};
        getListWallets(1, 'dash', params, next);
      }
    ], (err, wallets) => {
      if (err) throw err;

      assert.equal(wallets.length, 3);

      done();
    });

  });

});
