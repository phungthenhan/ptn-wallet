const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  coin: 'eth',
  walletId: DASH_DEFAULT_WALLET_ID,
  requestParams: {
    address: DASH_RECEIVE_ADDRESS,
    amount: 0,
  },
  requestHeader: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': () => G_AUTH_TOKENS['1'],
  },
  responsePrototype: {
    id: ['required', 'string'],
    date: ['required', 'string'],
    time: ['required', 'number'],
    blocktime: ['required', 'number'],
    blockHeight: ['required', 'number'],
    confirmations: ['required', 'number'],
    entries: ['required', 'array'],
  },
  entryPrototype: {
    address: ['required', 'string'],
    value: ['number'],
    valueString: ['string'],
    wallet: ['required', 'number']
  },
};

// Return a function to reset request data
const SendTransactionRequest = (walletId) => {
  return request
    .post(URLS.DASH_WALLET_API_URL + walletId + '/sendcoins')
    .send({
      address: MockData.requestParams.address,
      amount: MockData.requestParams.amount,
    })
    .set('Content-Type', MockData.requestHeader['Content-Type'])
    .set('Authorization', MockData.requestHeader['Authorization']());
};

const GetWalletRequest = (walletId) => {
  return request
    .get(URLS.DASH_WALLET_API_URL + walletId)
    .set('x-auth-token', MockData.requestHeader['Authorization']());
};

const GetTransactionDetailRequest = (walletId, txid) => {
  return request
    .get(URLS.DASH_WALLET_API_URL + walletId + '/tx/' + txid);
};


describe('II. DASH wallet - Get one wallet service', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // Check if create wallet was stored in database
    it('1.1 Correct wallet id - should send correct response', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');
              assert.ok(res.body, 'Server should send response');

              const newTransaction = res.body;

              GetTransactionDetailRequest(MockData.walletId, newTransaction.txid)
                .end((err, res) => {
                  assert.ok(res, 'Server should transaction detail');
                  assert.ok(res.body, 'Server should transaction detail');

                  const transactionDetail = res.body;
                  const [transactionDetailFormatError] = (new Checkit(MockData.responsePrototype)).validateSync(transactionDetail);
                  assert(isEmpty(transactionDetailFormatError), 'Server should return correct transaction detail format');
                  done();
                });
            });
        });
    });

    // Wrong txid
    it('1.3 Wrong txid - should return 400', (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');
              assert.ok(res.body, 'Server should send response');

              const newTransaction = res.body;

              GetTransactionDetailRequest(MockData.walletId, 'InvalidTxid')
                .end((err, res) => {
                  assert.ok(err, 'Server should send error response');

                  const status = err.status;
                  assert.equal(status, 400);

                  done();
                });
            });
        });
    });
  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */
  describe('3. Performance testing', () => {
    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      GetWalletRequest(MockData.walletId)
        .end((err, res) => {
          assert.ok(res, 'Server should send wallet');
          assert.ok(res.body, 'Server should send wallet');
          assert.ok(isNotEmpty(res.body.balance), 'Server should send balance');

          const balance = res.body.balance;
          const transactionAmount = 50000000;

          SendTransactionRequest(MockData.walletId)
            .send({
              address: MockData.requestParams.address,
              amount: `${transactionAmount}`,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send response');
              assert.ok(res.body, 'Server should send response');

              const newTransaction = res.body;

              GetTransactionDetailRequest(MockData.walletId, newTransaction.txid)
                .timeout({
                  response: CONSTRAINTS.RESPONSE_TIME,
                })
                .end((err, res) => {
                  assert.ok(res, 'Server should send successful response');

                  const status = res.status;
                  assert.equal(status, 200);

                  done();
                });
            });
        });
    });
  });
});
