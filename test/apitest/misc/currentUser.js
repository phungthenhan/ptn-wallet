const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  requestHeader: {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  requestParams: {
    username: DEFAULT_USERNAME,
    password: DEFAULT_PASSWORD,
  },
  responsePrototype: {
    id: ['number', 'required'],
    username: ['string', 'required'],
    avatarUrl: ['string'],
    email: ['string', 'required'],
    fullName: ['string', 'required'],
    apiKey: ['string'],
    apiSecret: ['string'],
  },
};

const LoginRequest = () => {
  return request
    .post(URLS.LOGIN_API_URL)
    .send({
      email: MockData.requestParams.username,
      password: MockData.requestParams.password,
    })
    .set('Accept', MockData.requestHeader['Accept'])
    .set('Content-Type', MockData.requestHeader['Content-Type']);
};

const GetCurrentUserRequest = (token) => {
  return request
    .get(URLS.USER_API_URL)
    .set('Authorization', token);
};


describe('Current user api', function() {

  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // Valid request
    it('1.1 Valid request - Should return status 200', (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send successful response');
          assert.ok(res.body.token, 'Server should send a token');

          const user = res.body;
          const token = res.body.token;

          GetCurrentUserRequest(token)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });

    it('1.2 Valid request - Should return correct response', (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send successful response');
          assert.ok(res.body.token, 'Server should send a token');

          const user = res.body;
          const token = res.body.token;

          GetCurrentUserRequest(token)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');
              assert.ok(res.body, 'Server should send successful response');

              const body = res.body;
              const [error] = (new Checkit(MockData.responsePrototype)).validateSync(body);
              assert.ok(isEmpty(error), 'Server should send correct response format');

              assert.equal(body.id, user.id);
              assert.equal(body.username, user.username);
              assert.equal(body.avatarUrl, user.avatarUrl);
              assert.equal(body.email, user.email);
              assert.equal(body.fullName, user.fullName);
              assert.equal(body.apiKey, user.apiKey);
              assert.equal(body.apiSecret, user.apiSecret);

              done();
            });
        });
    });
  });



  /**
   * ------------------------------------------------------------------------
   * Security testing
   * ------------------------------------------------------------------------
   */

  describe('2. Security testing', () => {
    // Wrong token
    it('2.1 Invalid header "Authorization" - should return 401', (done) => {
      GetCurrentUserRequest('WrongToken')
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 2.2 Missing header 'Authorization'
    it('2.2 Missing header "Authorization" - should return 401', (done) => {
      GetCurrentUserRequest('WrongToken')
        .end((err) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });
  });


  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {
    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');
          assert.ok(res.body, 'Server should send successful response');
          assert.ok(res.body.token, 'Server should send a token');

          const user = res.body;
          const token = res.body.token;

          GetCurrentUserRequest(token)
            .timeout({
              response: CONSTRAINTS.RESPONSE_TIME,
            })
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });
  });

});
