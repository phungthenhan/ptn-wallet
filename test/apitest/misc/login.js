const request = require('superagent');
const assert = require('assert');
const Checkit = require('cc-checkit');


const MockData = {
  requestHeader: {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  requestParams: {
    username: DEFAULT_USERNAME,
    password: DEFAULT_PASSWORD,
  },
  responsePrototype: {
    id: ['number'],
    username: ['string'],
    avatarUrl: ['string'],
    email: ['string'],
    fullName: ['string'],
    apiKey: ['string'],
    apiSecret: ['string'],
    token: ['string', 'required'],
  },
};


const LoginRequest = () => {
  return request
    .post(URLS.LOGIN_API_URL)
    .send({
      email: MockData.requestParams.username,
      password: MockData.requestParams.password,
    })
    .set('Accept', MockData.requestHeader['Accept'])
    .set('Content-Type', MockData.requestHeader['Content-Type']);
};

const ListWalletsRequest = (token) => {
  return request
    .get(URLS.DASH_WALLET_API_URL)
    .set('x-auth-token', token);
};


describe('Login api', () => {
  /**
   * ------------------------------------------------------------------------
   * Contract testing
   * ------------------------------------------------------------------------
   */
  describe('1. Contract testing', () => {
    // 1.1 Valid request
    it('1.1 Valid request - should return 200', (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });

    // 1.2 Valid request
    it('1.2 Valid request - should return correct response', (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const body = res.body;
          const [error] = (new Checkit(MockData.responsePrototype)).validateSync(body);
          assert(isEmpty(error), 'Server should send correct response format');

          done();
        });
    });

    // 1.3 Valid request
    it('1.3 Valid request - use token to request another api', (done) => {
      LoginRequest()
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const body = res.body;
          const token = body.token;

          ListWalletsRequest(token)
            .end((err, res) => {
              assert.ok(res, 'Server should send successful response');

              const status = res.status;
              assert.equal(status, 200);

              done();
            });
        });
    });

    // 1.4 Invalid request
    it('1.4 Invalid username - should return 401', (done) => {
      LoginRequest()
        .send({
          email: 'Invalid username',
          password: MockData.requestParams.password,
        })
        .end((err, res) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });

    // 1.5 Invalid request
    it('1.5 Invalid password - should return 401', (done) => {
      LoginRequest()
        .send({
          email: MockData.requestParams.username,
          password: 'InvalidPassword',
        })
        .end((err, res) => {
          assert.ok(err, 'Server should send error response');

          const status = err.status;
          assert.equal(status, 401);

          done();
        });
    });
  });



  /**
   * ------------------------------------------------------------------------
   * Performance testing
   * ------------------------------------------------------------------------
   */

  describe('3. Performance testing', () => {

    // Response time
    it (`3.1 Response time - should be under ${CONSTRAINTS.RESPONSE_TIME} ms`, (done) => {
      LoginRequest()
        .timeout({
          response: CONSTRAINTS.RESPONSE_TIME,
        })
        .end((err, res) => {
          assert.ok(res, 'Server should send successful response');

          const status = res.status;
          assert.equal(status, 200);

          done();
        });
    });
  });

});
