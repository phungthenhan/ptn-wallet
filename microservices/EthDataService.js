require('dotenv').config();
require('log4js').configure({
  appenders: {
    out: {
      type: 'console',
      level: process.env.LOG_LEVEL || 'WARN'
    }
  },
  categories: {
    default: {
      appenders: ['out'],
      level: process.env.LOG_LEVEL || 'WARN'
    }
  }
});

const EthTransactionCrawler = require('./eth/EthTransactionCrawler');
(new EthTransactionCrawler()).start();

const ERC20TransactionCrawler = require('./eth/ERC20TransactionCrawler');
(new ERC20TransactionCrawler()).start();
