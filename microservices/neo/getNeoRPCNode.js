const {rpc, tx, api, wallet, settings}    = require ('@cityofzion/neon-js');
const async                               = require('async');
const _                                   = require('lodash');
const networkConfig                       = require('../../config/neo');
const getNeoRPCEndpoint                   = require('./getNeoRPCEndpoint');
const logger                              = require('sota-core').getLogger('getNeoRPCNode');
const Neon                                = require('@cityofzion/neon-js').default;

const DEBUG_RESULT = false;
const NEONode = {};
const rpcQueryMethods = [
  'getBlock',
  'getBlockHash',
  'getBestBlockHash',
  'getBlockCount',
  'getBlockSysFee',
  'getConnectionCount',
  'getContractState',
  'getPeers',
  'getRawMemPool',
  'getRawTransaction',
  'getStorage',
  'getTxOut',
  'getVersion',
  'sendRawTransaction'
];
const apiMethods = [
  'getBalance',
  'getTransactionHistory'
];

for (let i = 0; i < rpcQueryMethods.length; i++) {
  const method = rpcQueryMethods[i];

  Object.defineProperty(NEONode, method, {
    writable: false,
    value: function () {
      const args = Array.prototype.slice.call(arguments)
      if (!args.length) {
        throw new Error(`Calling invalid method on NEO node: ${method}`);
      }

      const len = args.length;
      const originalCallback = args[len-1];
      const params = args.slice(0, len-1);
      if (typeof originalCallback !== 'function') {
        throw new Error(`The last argument of NEONode.${method} should be a callback function`);
      }

      getNeoRPCEndpoint((err, rpcEndpoint) => {
        if (err) {
          return originalCallback(err);
        }

        rpc.Query[method](...params)
          .execute(rpcEndpoint)
          .then(res => {
            if (DEBUG_RESULT) {
              logger.info(`[NEONode DEBUG] RPC method=${method}, params=${JSON.stringify(params)}, rpcEndpoint=${rpcEndpoint}. Result: ${JSON.stringify(res)}`);
            }

            if (!res || res.result === false) {
              return originalCallback(`Failed to execute RPC method=${method}, params=${JSON.stringify(params)}, rpcEndpoint=${rpcEndpoint}`);
            }

            return originalCallback(null, res.result);
          })
          .catch(err => {
            if (DEBUG_RESULT) {
              logger.error(`[NEONode DEBUG] RPC method=${method}, params=${JSON.stringify(params)}, rpcEndpoint=${rpcEndpoint}`);
              logger.error(err);
            }

            return originalCallback(err);
          });
      });
    }
  });
}

for (let i = 0; i < apiMethods.length; i++) {
  const method = apiMethods[i];

  Object.defineProperty(NEONode, method, {
    writable: false,
    value: function () {
      const args = Array.prototype.slice.call(arguments)
      if (!args.length) {
        throw new Error(`Calling invalid method on NEO node: ${method}`);
      }

      const len = args.length;
      const originalCallback = args[len-1];
      const params = args.slice(0, len-1);
      if (typeof originalCallback !== 'function') {
        throw new Error(`The last argument of NEONode.${method} should be a callback function`);
      }

      api.neoscan[method](networkConfig.netName, ...params)
        .then(res => {
          if (!res) {
            return originalCallback(`Failed to execute api ${method} with params: ${params}`);
          }

          if (DEBUG_RESULT) {
            logger.info(`[NEONode DEBUG] neoscan API method=${method}, params=${JSON.stringify(params)}. Result: ${JSON.stringify(res)}`);
          }

          return originalCallback(null, res);
        })
        .catch(err => {
          if (DEBUG_RESULT) {
            logger.error(`[NEONode DEBUG] neoscan API method=${method}, params=${JSON.stringify(params)}.`);
            logger.error(err);
          }

          originalCallback(err);
        });
    }
  });
}

Object.defineProperty(NEONode, 'getBatchTransactions', {
  writable: false,
  value: getBatchTransactions
});

Object.defineProperty(NEONode, 'getBlockTransactions', {
  writable: false,
  value: getBlockTransactions
});

function getBatchTransactions (options, callback) {
  let txs = [];
  const fromBlockNumber = options.fromBlockNumber;
  const toBlockNumber = options.toBlockNumber;

  if (!fromBlockNumber || !toBlockNumber) {
    return callback(`You must specify the block range, currently: start=${fromBlockNumber}, end=${toBlockNumber}`);
  }

  if (fromBlockNumber > toBlockNumber) {
    return callback(`Start block must be less or equal end block, currently start=${fromBlockNumber}, end=${toBlockNumber}`);
  }

  const blockNumbers = [];
  for (let no = fromBlockNumber; no <= toBlockNumber; no++) {
    blockNumbers.push(no);
  }

  async.each(blockNumbers, (blockNumber, next) => {
    NEONode.getBlockTransactions(blockNumber, (err, ret) => {
      if (err) {
        return next(err);
      }
      txs = txs.concat(ret);
      return next(null, null);
    });
  }, (err) => {
    if (err) {
      return callback(err);
    }

    txs = _.compact(txs);
    return callback(null, txs);
  });
}

function getBlockTransactions (blockNumber, callback) {
  async.waterfall([
    (next) => {
      NEONode.getBlock(blockNumber, next);
    },
    (block, next) => {
      _getTxsOfBlock(block, next);
    }
  ], callback);
}

function _getTxsOfBlock(block, callback) {
  const txs = block.tx;
  _.each(txs, (tx) => {
    tx.height = block.index;
    tx.blockhash = block.hash;
    tx.time = block.time;
  });

  return callback(null, txs);
}

module.exports = () => {
  return NEONode;
}
