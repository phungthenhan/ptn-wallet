const {rpc, tx, api, wallet, settings}    = require ('@cityofzion/neon-js');
const async                               = require('async');
const _                                   = require('lodash');
const networkConfig                       = require('../../config/neo');
const logger                              = require('sota-core').getLogger('getNeoRPCEndpoint');
const Neon                                = require('@cityofzion/neon-js').default;

/**
 * Add private network to list network Neonjs.
 */

const isPrivateNet = networkConfig.netName === 'PrivateNet';
if (isPrivateNet) {
  const privateNet = new rpc.Network({
    name: networkConfig.netName,
    extra: {
      neoscan: networkConfig.neoscanUrl
    }
  });

  Neon.settings.addNetwork(privateNet);
}

module.exports = (callback) => {
  if (isPrivateNet) {
    return callback(null, networkConfig.rpcEndpoint);
  }

  api.neoscan
    .getRPCEndpoint(networkConfig.netName)
    .then(rpcEndpoint => {
      return callback(null, rpcEndpoint);
    })
    .catch(err => {
      logger.error(`Cannot get RPC endpoint for network: ${networkConfig.netName}.`);
      logger.error(err);
      logger.warn(`Will use endpoint in config: ${networkConfig.rpcEndpoint}`)
      return callback(null, networkConfig.rpcEndpoint);
    });
}