const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/neo');
const BaseDepositCrawler        = require('../foundation/BaseDepositCrawler');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('NeoDepositCrawler');
const getNeoRPCNode             = require('./getNeoRPCNode');

const CACHED_BLOCK_TIME = {};
const NEONode = getNeoRPCNode();

class NeoDepositCrawler extends BaseDepositCrawler {

  static getLatestBlockOnNetwork (callback) {
    NEONode.getBlockCount(callback);
  }

  static getCoinSymbol () {
    return Const.COIN.NEO;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'NeoAddressModel';
  }

  static getDepositModelName () {
    return 'NeoDepositModel';
  }

  static getBlockNumInOneGo () {
    return 10;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  extractAddressesFromTx (tx) {
    return _.chain(tx.vout)
      .map(this._extractAddressesFromOutput)
      .compact()
      .uniq()
      .value();
  }

  _extractAddressesFromOutput (output) {
    if (!output) {
      return null;
    }

    return output.address;
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    NEONode.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  }

  constructDepositEntitiesFromTx (tx) {
    // Override if needed
    return super.constructDepositEntitiesFromTx(tx);
  }


  /**
   * Outputs have simple format:
   * {
   *   address: int,
   *   amount: int,
   * }
   */
  extractOutputsFromTx (tx) {
    const vouts = tx.vout || [];
    const addressAmounts = {};


    _.forEach(vouts, (vout) => {
      if (!vout) {
        return null;
      }

      // Addresses array should have only 1 item, take it
      const address = vout.address;

      // If address is not a watching one, ignore
      if (!this._cachedAddresses[address]) {
        return;
      }

      // Add accumulation output value for address
      if (!addressAmounts[address]) {
        addressAmounts[address] = 0;
      }

      addressAmounts[address] += vout.value;
    });

    return _.map(Object.keys(addressAmounts), (address) => ({
        address: address,
        amount: addressAmounts[address],
      }));
  }
}

module.exports = NeoDepositCrawler;