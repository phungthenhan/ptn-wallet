const _                             = require('lodash');
const async                         = require('async');
const networkConfig                 = require('../../config/neo');
const Const                         = require('../../app/common/Const');
const BaseWithdrawalProcessor       = require('../foundation/BaseWithdrawalProcessor');
const getNeoRPCNode                 = require('./getNeoRPCNode');
const ExSession                     = require('sota-core').load('common/ExSession');
const Utils                         = require('sota-core').load('util/Utils');
const logger                        = require('sota-core').getLogger('NeoWithdrawalProcessor');

const NEONode = getNeoRPCNode();

class NeoWithdrawalProcessor extends BaseWithdrawalProcessor {

  constructor () {
    super();
    this.classname = 'NeoWithdrawalProcessor';
    this._coinSymbol = Const.COIN.NEO;
    this._withdrawalModelClassname = 'NeoWithdrawalModel';
    this._requiredConfirmations = networkConfig.requiredConfirmation;
    this.TIMER = 2000;
  }

  _doSendTransaction (withdrawalRecord, callback) {
    const now = Utils.now();

    NEONode.sendRawTransaction(withdrawalRecord.txRaw, (err, ret) => {
      if (err) {
        logger.error(`Failed to sent signed transaction ${withdrawalRecord.txid}: ${err.toString()}`);
      } else {
        logger.info(`${this.classname}::_doSendTransaction: ${withdrawalRecord.txid}. Result: ${JSON.stringify(ret)}`);
        withdrawalRecord.setData({
          status: Const.WITHDRAW_STATUS.SENT
        });
      }

      withdrawalRecord.setData({
        tried_at: now
      }).save(callback);
    });
  }

  _getBlockInfo (blockNumber, callback) {
    async.auto({
      block: (next) => {
        NEONode.getBlock(blockNumber, next);
      },
      latestBlockNumber: (next) => {
        NEONode.getBlockCount(next);
      },
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const block = ret.block;
      const latestBlockNumber = ret.latestBlockNumber;

      if (!block) {
        logger.error(`Something went wrong. Cannot get block: ${blockNumber}`);
        return callback(`Could not get block ${blockNumber}. Wait for the next tick...`);
      }

      if (!latestBlockNumber) {
        logger.error(`Something went wrong. Cannot get latest block number on the network`);
        return callback(`Could not get latest block number. Wait for the next tick...`);
      }

      return callback(null, {
        number: block.index,
        hash: block.hash.slice(2),
        timestamp: block.time,
        confirmations: latestBlockNumber - block.index,
      });
    });
  }

  _getTxInfo(withdrawalRecord, callback) {
    const createdAt = withdrawalRecord.createdAt;
    const txid = withdrawalRecord.txid;
    const now = Utils.now();
    const data = {};

    async.auto({
      tx: (next) => {
        NEONode.getRawTransaction(txid, next);
      },
      block: ['tx', (ret, next) => {
        NEONode.getBlock(ret.tx.blockhash, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const tx = ret.tx;
      const block = ret.block;

      if (tx.txid === 'not found' || tx.block_height === null) {
        return callback(null, null);
      }

      return callback(null, {
        blockNumber: block.index,
        status: Const.TX_STATUS.SUCCESS,
        fee: tx.sys_fee + tx.net_fee,
      });

    });
  }

  _getCoinSymbol () {
    return Const.COIN.NEO;
  }

};

module.exports = NeoWithdrawalProcessor;
