require('dotenv').config();
const path                = require('path');
const SotaCore            = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore             = require('bitcore-lib');
const dashcore            = require('@dashevo/dashcore-lib');
const logger              = SotaCore.getLogger('BtcDepositService');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});

process.nextTick(() => {
  const BtcDepositCrawler = require('./btc/BtcDepositCrawler');
  const DepositCrawlerManager = require('./foundation/DepositCrawlerManager');
  const crawler = new DepositCrawlerManager();
  crawler.start(BtcDepositCrawler);
});
app.start();
