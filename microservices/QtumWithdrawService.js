require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const Signer = require('./qtum/QtumWithdrawalSigner');
  const Sender = require('./qtum/QtumWithdrawalSender');
  const Verifier = require('./qtum/QtumWithdrawalVerifier');
  const WithdrawalManager = require('./foundation/WithdrawalManager');
  const manager = new WithdrawalManager();
  manager.start(Signer, Sender, Verifier);
});
