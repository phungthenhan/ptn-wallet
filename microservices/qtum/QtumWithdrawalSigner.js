const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSigner      = require('../foundation/BaseWithdrawalSigner');
const logger                    = require('sota-core').getLogger('QtumWithdrawalSigner');
const Utils                     = require('sota-core').load('util/Utils');
const QtumGateway               = require('../../app/blockchain').QtumGateway;
const UtxoBasedWithdrawalSigner = require('../utxo-based/UtxoBasedWithdrawalSigner');

class QtumWithdrawalSigner extends UtxoBasedWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.QTUM;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'QtumWalletService'
  }

  static getWithdrawalModelName () {
    return 'QtumWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'QtumHotWalletModel';
  }

  static getGateway(){
    return QtumGateway;
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    return super.signWithdrawalTransaction(withdrawal, hotWallets, callback);
  }

  writeTxChangeLog (withdrawal, newTxid, callback) {
    return super.writeTxChangeLog (withdrawal, newTxid, callback);
  }
}

module.exports = QtumWithdrawalSigner;