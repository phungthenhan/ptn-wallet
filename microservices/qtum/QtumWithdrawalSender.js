const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSender      = require('../foundation/BaseWithdrawalSender');
const logger                    = require('sota-core').getLogger('QtumWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const QtumGateway               = require('../../app/blockchain').QtumGateway;
const UtxoBasedWithdrawalSender = require('../utxo-based/UtxoBasedWithdrawalSender')

class QtumWithdrawalSender extends UtxoBasedWithdrawalSender {

  static getCoinSymbol () {
    return Const.COIN.QTUM;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'QtumWalletService'
  }

  static getWithdrawalModelName () {
    return 'QtumWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'QtumHotWalletModel';
  }

  static getGateway(){
    return QtumGateway;
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    return super.doSendTransaction(withdrawal, callback);
  }
}

module.exports = QtumWithdrawalSender;
