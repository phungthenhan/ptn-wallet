const _                         = require('lodash');
const async                     = require('async');
const qtumcore                  = require('qtumcore-lib');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/qtum');
const QtumGateway               = require('../../app/blockchain').QtumGateway;
const UTXOBasedDepositCrawler   = require('../foundation/UTXOBasedDepositCrawler');
const logger                    = require('sota-core').getLogger('QtumDepositCrawler');

class QtumDepositCrawler extends UTXOBasedDepositCrawler {

  static getCoinSymbol () {
    return Const.COIN.QTUM;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'QtumAddressModel';
  }

  static getDepositModelName () {
    return 'QtumDepositModel';
  }

  static getBlockNumInOneGo () {
    return 2;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  static getLatestBlockOnNetwork (callback) {
    QtumGateway.getBlockCount(callback);
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    QtumGateway.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  }

  constructDepositEntitiesFromTx (tx) {
    // Override if needed
    return super.constructDepositEntitiesFromTx(tx);
  }

  extractOutputsFromTx (tx) {
    // Override if needed
    return super.extractOutputsFromTx(tx);
  }

}

module.exports = QtumDepositCrawler;
