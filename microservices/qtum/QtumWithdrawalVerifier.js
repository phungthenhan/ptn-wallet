const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalVerifier      = require('../foundation/BaseWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('QtumWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const QtumGateway               = require('../../app/blockchain').QtumGateway;
const UtxoBasedWithdrawalVerifier = require('../utxo-based/UtxoBasedWithdrawalVerifier');

class QtumWithdrawalVerifier extends UtxoBasedWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.QTUM;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'QtumWalletService'
  }

  static getWithdrawalModelName () {
    return 'QtumWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'QtumHotWalletModel';
  }

  static getGateway(){
    return QtumGateway;
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {
    return super.getTxInfo(withdrawal, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = QtumWithdrawalVerifier;