const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const BaseWithdrawalSigner      = require('../foundation/BaseWithdrawalSigner');
const logger                    = require('sota-core').getLogger('EthWithdrawalSigner');
const Utils                     = require('sota-core').load('util/Utils');
const EthGateway                = require('../../app/blockchain').EthGateway;

const web3 = EthGateway.getLib();

class EthWithdrawalSigner extends BaseWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.ETH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'EthWalletService'
  }

  static getWithdrawalModelName () {
    return 'EthWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    if (withdrawal == null) {
      return callback(Const.ASYNC_BREAKER);
    }
    const now = Utils.now();
    let reserve = null;

    async.auto({
      checkBalance: (next) => {
        return this.findAvailableBalanceHotWallet(withdrawal, hotWallets, next);
      },

      gasPrice: (next) => {
        web3.eth.getGasPrice(next);
      },

      reserve: ["checkBalance", (ret, next) => {
        const randomWallet = _.sample(ret.checkBalance);
        reserve = web3.eth.accounts.privateKeyToAccount(randomWallet.privateKey);
        return next(null, reserve);
      }],

      prepare: ["reserve", (ret, next) => {
        withdrawal.setData({
          fromAddress : ret.reserve.address,
          gasPrice : ret.gasPrice,
        });
        return next(null, ret);
      }],

      sign: ["prepare", (ret, next) => {
        EthGateway.resignTransaction(withdrawal, ret.reserve, next);
      }],

      tx_changed: ["sign", (ret, next) => {
        // check if Txid was changed, push to trace array
        const newTxid = web3.utils.sha3(ret.sign.rawTransaction);
        this.writeTxChangeLog(withdrawal, newTxid, next);
      }],

      record: ["tx_changed", (ret, next) => {
        const newTxid = web3.utils.sha3(ret.sign.rawTransaction);
        withdrawal.setData({
          status: Const.WITHDRAW_STATUS.SIGNED,
          txid: newTxid,
          tx_raw: ret.sign.rawTransaction,
          tried_at: now
        }).save(next);
      }]
    }, callback)
  }

  writeTxChangeLog (withdrawal, newTxid, callback) {
    return super.writeTxChangeLog (withdrawal, newTxid, callback);
  }
}

module.exports = EthWithdrawalSigner;