const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const ERC20ABI                  = require('../../config/eth/abi/erc20.json');
const getLatestProcessedBlock   = require('./getLatestProcessedBlock');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('ERC20DepositCrawler');
const web3                      = require('../../app/blockchain').EthGateway.getLib();

let LATEST_PROCESSED_BLOCK = 0;
const BATCH_BLOCK_SIZE = 1000;
const REQUIRED_CONFIRMATION = networkConfig.requiredConfirmation;

const tokensByAddress = _.keyBy(networkConfig.tokens, 'address');
const tokensBySymbol = _.keyBy(networkConfig.tokens, 'symbol');

class ERC20DepositCrawler {

  start (tokenSymbol) {
    this._tokenConfig = tokensBySymbol[tokenSymbol];
    if (!this._tokenConfig) {
      logger.error(`Invalid token: ${tokenSymbol}`);
      process.exit(1);
    }

    this._tokenContract = new web3.eth.Contract(ERC20ABI, this._tokenConfig.address);

    this.restart();
  }

  restart () {
    async.auto({
      latestProcessedBlock: (next) => {
        if (LATEST_PROCESSED_BLOCK > 0) {
          return next(null, LATEST_PROCESSED_BLOCK);
        }

        getLatestProcessedBlock(this._tokenConfig, next);
      },
      processBlocks: ['latestProcessedBlock', (ret, next) => {
        let latestProcessedBlock = ret.latestProcessedBlock - 1;
        if (latestProcessedBlock < networkConfig.startCrawlBlock) {
          latestProcessedBlock = networkConfig.startCrawlBlock;
        }

        this.processBlocks(latestProcessedBlock, next);
      }]
    }, (err, ret) => {
      if (err) {
        logger.error(err);
        logger.info(`Crawler will be restarted in a few seconds...`);
      } else {
        logger.info(`Already processed the newest block. Crawler will be restarted in a few seconds...`);
      }

      setTimeout(() => {
        this.restart();
      }, networkConfig.averageBlockTime);
    });
  }

  processBlocks (latestProcessedBlock, callback) {
    let fromBlockNumber, toBlockNumber;
    async.auto({
      latestOnchainBlock: (next) => {
        web3.eth.getBlockNumber(next);
      },
      processBlocksOnce: ['latestOnchainBlock', (ret, next) => {
        const latestOnchainBlock = ret.latestOnchainBlock;
        fromBlockNumber = latestProcessedBlock + 1;

        // Crawl the newest block already
        if (fromBlockNumber > latestOnchainBlock - REQUIRED_CONFIRMATION) {
          toBlockNumber = latestProcessedBlock;
          return next(null, true);
        }

        toBlockNumber = latestProcessedBlock + BATCH_BLOCK_SIZE;
        if (toBlockNumber > latestOnchainBlock - REQUIRED_CONFIRMATION) {
          toBlockNumber = latestOnchainBlock - REQUIRED_CONFIRMATION;
        }

        this._processBlocksOnce(fromBlockNumber, toBlockNumber, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (ret.processBlocksOnce === true) {
        return callback(null, true);
      }

      LATEST_PROCESSED_BLOCK = toBlockNumber;
      process.nextTick(() => {
        this.processBlocks(LATEST_PROCESSED_BLOCK, callback);
      });
    });
  }

  _processBlocksOnce (fromBlockNumber, toBlockNumber, callback) {
    logger.info(`_processBlocksOnce: ${fromBlockNumber}→${toBlockNumber}`);
    async.auto({
      data: (next) => {
        const topic = networkConfig.eventTopics.erc20Transfer;
        const options = {
          fromBlock: fromBlockNumber,
          toBlock: toBlockNumber,
        };

        this._tokenContract.getPastEvents(topic, options, next)
      },
      processData: ['data', (ret, next) => {
        this._processLogData(ret.data, next);
      }],
    }, callback);
  }

  _processLogData (arrayData, callback) {
    const exSession = new ExSession();
    async.waterfall([
      (next) => {
        async.each(arrayData, (tx, _next) => {
          this._processOneTransaction(exSession, tx, _next);
        }, next);
      },
      (ret, next) => {
        if (typeof ret === 'function') {
          next = ret;
        }

        exSession.commit(next);
      },
    ], (err, ret) => {
      exSession.destroy();
      if (err) {
        return callback(err);
      }

      return callback(null, null);
    });
  }

  _processOneTransaction (exSession, tx, callback) {
    const WalletService = exSession.getService('WalletService');
    const Erc20DepositModel = exSession.getModel('Erc20DepositModel');
    const EthAddressModel = exSession.getModel('EthAddressModel');
    const WalletWebhookService = exSession.getService('WalletWebhookService');
    const fromAddress = tx.returnValues.from;
    const toAddress = tx.returnValues.to;
    async.auto({
      address: (next) => {
        const conditions = {
          address: toAddress
        };

        EthAddressModel.findOne(conditions, next);
      },
      tx: (next) => {
        Erc20DepositModel.findOne({
          where: 'txid = ?',
          params: [tx.transactionHash]
        }, next);
      },
      block: ['address', 'tx', (ret, next) => {
        if (!ret.address) {
          return next(null, true);
        }

        if (ret.tx) {
          logger.info(`Transaction ${tx.transactionHash} was recorded already.`);
          return next(null, true);
        }

        web3.eth.getBlock(tx.blockNumber, next);
      }],
      isInternalTx: ['block', (ret, next) => {
        if (!ret.block) {
          return next(`Cannot get data of block: ${tx.blockNumber}`);
        }

        EthAddressModel.findOne({
          where: 'address = ?',
          params: [fromAddress]
        }, (err, ret) => {
          if (err) {
            return next(err);
          }

          return next(null, !!ret);
        });
      }],
      record: ['isInternalTx', (ret, next) => {
        if (!ret.address || ret.tx) {
          return next(null, null);
        }

        if (ret.isInternalTx) {
          logger.warn(`Didn't record deposit ${tx.transactionHash} since it's an internal transaction.`);
          return next(null, null);
        }

        async.parallel([
          (_next) => {
            const walletId = ret.address.walletId;
            const amount = tx.returnValues.value.toString();
            const coin = this._tokenConfig.symbol;
            const options = {};
            const data = {
              block_number: tx.blockNumber,
              block_hash: tx.blockHash,
              block_timestamp: ret.block.timestamp,
              tx_index: tx.transactionIndex,
              txid: tx.transactionHash,
              token_address: this._tokenConfig.address,
              token_symbol: coin,
              wallet_id: ret.address.walletId,
              from_address: fromAddress,
              to_address: toAddress,
              amount: amount,
            };

            WalletService.handleDeposit(walletId, coin, Erc20DepositModel, data, options, _next);
          },
          (_next) => {
            const coin = this._tokenConfig.symbol;
            const walletId = ret.address.walletId;
            const txid = tx.transactionHash;
            WalletWebhookService.createWalletWebhookProgress(coin, walletId, txid, _next);
          }
        ], next);
      }],
    }, callback);
  }

};

module.exports = ERC20DepositCrawler;
