const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const BaseWithdrawalSender      = require('../foundation/BaseWithdrawalSender');
const logger                    = require('sota-core').getLogger('EthWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const EthGateway                = require('../../app/blockchain').EthGateway;

const web3 = EthGateway.getLib();

class EthWithdrawalSender extends BaseWithdrawalSender {

  static getCoinSymbol () {
    return Const.COIN.ETH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'EthWalletService'
  }
  
  static getWithdrawalModelName () {
    return 'EthWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    if (!withdrawal){
      return callback(null, null);
    }
    web3.eth.sendSignedTransaction(withdrawal.txRaw, (err, ret) => {
      return this.afterSendTransaction(err, ret, withdrawal, callback);
    });
  }
}

module.exports = EthWithdrawalSender;
