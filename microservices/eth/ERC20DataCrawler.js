const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const ERC20ABI                  = require('../../config/eth/abi/erc20.json');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('ERC20DataCrawler');
const web3                      = require('../../app/blockchain').EthGateway.getLib();

let LATEST_PROCESSED_BLOCK = 0;
const BATCH_BLOCK_SIZE = 1000;
const REQUIRED_CONFIRMATION = networkConfig.requiredConfirmation;
const tokensByAddress = _.keyBy(networkConfig.tokens, 'address');
const tokensBySymbol = _.keyBy(networkConfig.tokens, 'symbol');

class ERC20DataCrawler {

  start (tokenSymbol) {
    // Implement me.
  }

};

module.exports = ERC20DataCrawler;
