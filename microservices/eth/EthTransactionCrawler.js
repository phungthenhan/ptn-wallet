const _                 = require('lodash');
const async             = require('async');
const log4js            = require('log4js');
const BN                = require('bignumber.js');
const Const             = require('../../app/common/Const');
const networkConfig     = require('../../config/eth');
const web3              = require('../../app/blockchain').EthGateway.getLib();

const logger = log4js.getLogger('EthTransactionCrawler');
const knex = require('knex')({
  client: 'mysql',
  connection: {
    host : process.env.MYSQL_DB_HOST,
    user : process.env.MYSQL_USERNAME,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DBNAME,
  }
});

let LATEST_PROCESSED_BLOCK = 0;
let LATEST_ONCHAIN_BLOCK = 0;
const DUMMY_ETH_CONTRACT_ADDRESS = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee';
const FINISHED_CRAWLING_MSG = `Already processed the newest block. Crawler will be restarted in a few seconds...`;
const REQUIRED_CONFIRMATION = networkConfig.requiredConfirmation;
const TRANSACTION_TABLE_NAME = process.env.ETH_NETWORK === 'mainnet' ? `eth_transaction` : `eth_transaction_${process.env.ETH_NETWORK}`;
const BLOCK_TABLE_NAME = process.env.ETH_NETWORK === 'mainnet' ? `eth_block` : `eth_block_${process.env.ETH_NETWORK}`;

class EthTransactionCrawler {

  start () {
    async.waterfall([
      (next) => {
        if (LATEST_PROCESSED_BLOCK > 0) {
          return next(null, LATEST_PROCESSED_BLOCK);
        }

        this._queryLatestBlock(next);
      },
      (latestProcessedBlock, next) => {
        this.processBlocks(latestProcessedBlock, next);
      }
    ], (err, ret) => {
      if (err && err === FINISHED_CRAWLING_MSG) {
        logger.info(FINISHED_CRAWLING_MSG);
        setTimeout(this.start.bind(this), networkConfig.averageBlockTime);
        return;
      }

      if (err) {
        logger.error(err);
      }

      logger.info(`Crawler will be restarted in a few seconds...`);
      process.nextTick(this.start.bind(this));
    });
  }

  processBlocks (latestProcessedBlock, callback) {
    const needProcessBlock = latestProcessedBlock + 1;

    async.auto({
      latestOnchainBlock: (next) => {
        if (LATEST_PROCESSED_BLOCK < LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION) {
          return next(null, LATEST_ONCHAIN_BLOCK);
        }

        web3.eth.getBlockNumber(next);
      },
      processBlocksOnce: ['latestOnchainBlock', (ret, next) => {
        LATEST_ONCHAIN_BLOCK = ret.latestOnchainBlock;

        // Crawl the newest block already
        if (needProcessBlock > LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION) {
          return next(FINISHED_CRAWLING_MSG);
        }

        this._processBlocksOnce(needProcessBlock, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      LATEST_PROCESSED_BLOCK = needProcessBlock;
      process.nextTick(this.start.bind(this));
    });
  }

  _processBlocksOnce (blockNumber, callback) {
    logger.info(`_processBlocksOnce: ${blockNumber}`);

    async.waterfall([
      (next) => {
        web3.eth.getBlock(blockNumber, true, next);
      },
      (block, next) => {
        this._saveTransactionsData(block, next);
      },
    ], callback);
  }

  _saveTransactionsData (block, callback) {
    const data = [];
    const txs = block.transactions;
    const now = Date.now();

    async.forEachLimit(txs, 100, (tx, next) => {
      if (!parseInt(tx.value)) {
        return next(null, null);
      }

      web3.eth.getTransactionReceipt(tx.hash, (err, receipt) => {
        if (err) {
          return next(err);
        }

        data.push({
          coin: Const.COIN.ETH,
          contract_address: DUMMY_ETH_CONTRACT_ADDRESS,
          txid: tx.hash,
          nonce: tx.nonce,
          block_number: block.number,
          block_hash: block.hash,
          block_timestamp: block.timestamp,
          transaction_index: tx.transactionIndex,
          from: tx.from,
          to: tx.to,
          value: tx.value,
          gas_price: tx.gasPrice,
          gas_limit: tx.gas,
          gas_used: receipt.gasUsed,
          status: receipt.status,
          log_id: 'eth_transfer',
          created_at: now,
          updated_at: now,
          created_by: 0,
          updated_by: 0,
        });

        return next(null, null);
      });
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const blockData = {
        number: block.number,
        hash: block.hash,
        nonce: block.nonce,
        size: block.size,
        timestamp: block.timestamp,
        created_at: now,
        updated_at: now,
        created_by: 0,
        updated_by: 0,
      };

      knex.transaction((trx) => {
        Promise.all([
          knex.raw(knex(BLOCK_TABLE_NAME).insert(blockData).toString().replace('insert', 'INSERT IGNORE')).transacting(trx),
          knex.raw(knex(TRANSACTION_TABLE_NAME).insert(data).toString().replace('insert', 'INSERT IGNORE')).transacting(trx)
        ]).then(trx.commit)
          .catch(trx.rollback);
      })
      .then((ids) => {
        logger.info(`Inserted ${data.length} transactions.`);
        return callback(null, null);
      })
      .catch((err) => {
        logger.error(err);
        return callback(err);
      });
    });
  }

  _queryLatestBlock (callback) {
    knex(TRANSACTION_TABLE_NAME)
      .max('block_number as max')
      .where('coin', Const.COIN.ETH)
      .then((ret) => {
        let maxBlockNumber = networkConfig.startCrawlBlock;
        try {
          maxBlockNumber = (parseInt(ret[0].max));
        } catch (e) {
          return callback(null, networkConfig.startCrawlBlock);
        }

        if (isNaN(maxBlockNumber) || maxBlockNumber < networkConfig.startCrawlBlock) {
          maxBlockNumber = networkConfig.startCrawlBlock;
        }

        return callback(null, maxBlockNumber - 1);
      })
      .catch((err) => {
        logger.fatal(err);
        return callback(err);
      });
  }

};

module.exports = EthTransactionCrawler;
