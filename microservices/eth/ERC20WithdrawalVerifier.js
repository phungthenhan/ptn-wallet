const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const EthWithdrawalVerifier     = require('./EthWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('ERC20WithdrawalVerifier');
const Utils                     = require('sota-core').load('util/Utils');

class ERC20WithdrawalVerifier extends EthWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return withdrawal.tokenSymbol;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getWithdrawalModelName () {
    return 'Erc20WithdrawalModel';
  }

  static getCoinService (){
    return 'Erc20WalletService'
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {
    return super.getTxInfo(withdrawal, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = ERC20WithdrawalVerifier;