const _                         = require('lodash');
const async                     = require('async');
const ExSession                 = require('sota-core').load('common/ExSession');

module.exports = (tokenConfig, callback) => {
  const exSession = new ExSession();
  const Erc20DepositModel = exSession.getModel('Erc20DepositModel');
  const tokenAddress = tokenConfig.address;

  Erc20DepositModel.findOne({
    where: 'token_address = ?',
    params: [tokenAddress],
    orderBy: 'block_number DESC'
  }, (err, ret) => {
    exSession.destroy();

    if (err) {
      return callback(err);
    }

    if (!ret) {
      return callback(null, 1);
    }

    return callback(null, ret.blockNumber);
  });

};
