const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const BaseDepositCrawler        = require('../foundation/BaseDepositCrawler');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('EthDepositCrawler');
const web3                      = require('../../app/blockchain').EthGateway.getLib();

class EthDepositCrawler extends BaseDepositCrawler {

  static getCoinSymbol () {
    return Const.COIN.ETH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'EthAddressModel';
  }

  static getDepositModelName () {
    return 'EthDepositModel';
  }

  static getBlockNumInOneGo () {
    return 10;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  static getLatestBlockOnNetwork (callback) {
    web3.eth.getBlockNumber(callback);
  }

  extractAddressesFromTx (tx) {
    return [tx.to];
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    const blockNumbers = [];
    for (let num = fromBlockNumber; num <= toBlockNumber; num++) {
      blockNumbers.push(num);
    }

    const txs = [];
    async.each(blockNumbers, (blockNumber, next) => {
      web3.eth.getBlock(blockNumber, true, (err, block) => {
        if (err) {
          return next(null);
        }

        _.each(block.transactions, tx => {
          txs.push(_.assign(tx, {
            txid: tx.hash,
          }));
        });

        return next(null, null);
      });
    }, (err) => {
      if (err) {
        return callback(err);
      }

      return callback(null, txs);
    });
  }

  constructDepositEntitiesFromTx (tx) {
    const outputs = this.extractOutputsFromTx(tx);
    if (!outputs || !outputs.length) {
      return [];
    }

    return _.reduce(outputs, (ret, output) => {
      const address = output.address;
      const amount = output.amount;
      const walletId = this._cachedAddresses[address].walletId;

      return ret.concat([
        {
          block_number: tx.blockNumber,
          block_hash: tx.blockHash,
          block_timestamp: 0,
          tx_index: tx.transactionIndex,
          txid: tx.txid,
          wallet_id: walletId,
          address: address,
          amount: amount,
          from_address: tx.from,
          to_address: tx.to,
        }
      ]);
    }, []);
  }

  extractOutputsFromTx (tx) {
    return [{
      address: tx.to,
      amount: tx.value
    }];
  }
}

module.exports = EthDepositCrawler;
