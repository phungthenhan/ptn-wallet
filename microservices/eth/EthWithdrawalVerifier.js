const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const BaseWithdrawalVerifier      = require('../foundation/BaseWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('EthWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const EthGateway                = require('../../app/blockchain').EthGateway;

class EthWithdrawalVerifier extends BaseWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.ETH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'EthWalletService'
  }

  static getWithdrawalModelName () {
    return 'EthWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {
    if (withdrawal == null)
      return callback(Const.ASYNC_BREAKER);
    return EthGateway.getTransactionInfo(withdrawal.txid, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = EthWithdrawalVerifier;