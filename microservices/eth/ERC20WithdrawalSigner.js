const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const EthWithdrawalSigner       = require('./EthWithdrawalSigner');

class ERC20WithdrawalSigner extends EthWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return withdrawal.tokenSymbol;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'Erc20WalletService'
  }

  static getWithdrawalModelName () {
    return 'Erc20WithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    return super.signWithdrawalTransaction(withdrawal, hotWallets, callback);
  }
}

module.exports = ERC20WithdrawalSigner;