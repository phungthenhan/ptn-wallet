const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/eth');
const EthWithdrawalSender       = require('./EthWithdrawalSender');
const logger                    = require('sota-core').getLogger('ERC20WithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');

class ERC20WithdrawalSender extends EthWithdrawalSender {

  static getNetworkConfig () {
    return networkConfig;
  }

  static getWithdrawalModelName () {
    return 'Erc20WithdrawalModel';
  }

  static getCoinService (){
    return 'Erc20WalletService'
  }

  static getHotWalletModelName () {
    return 'EthHotWalletModel';
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    return super.doSendTransaction(withdrawal, callback);
  }
}

module.exports = ERC20WithdrawalSender;
