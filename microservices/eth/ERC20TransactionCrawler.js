const _                 = require('lodash');
const async             = require('async');
const log4js            = require('log4js');
const BN                = require('bignumber.js');
const Const             = require('../../app/common/Const');
const networkConfig     = require('../../config/eth');
const web3              = require('../../app/blockchain').EthGateway.getLib();

const tokensBySymbol    = _.keyBy(networkConfig.tokens, 'symbol');
const tokensByAddress   = _.keyBy(networkConfig.tokens, (t) => t.address.toLowerCase());

const logger = log4js.getLogger('ERC20TransactionCrawler');
const knex = require('knex')({
  client: 'mysql',
  connection: {
    host : process.env.MYSQL_DB_HOST,
    user : process.env.MYSQL_USERNAME,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DBNAME,
  }
});

let LATEST_PROCESSED_BLOCK = 0;
let LATEST_ONCHAIN_BLOCK = 0;
const BATCH_BLOCK_SIZE = 7;
const REQUIRED_CONFIRMATION = networkConfig.requiredConfirmation;
const FINISHED_CRAWLING_MSG = `Already processed the newest block. Crawler will be restarted in a few seconds...`;
const TRANSACTION_TABLE_NAME = process.env.ETH_NETWORK === 'mainnet' ? `eth_transaction` : `eth_transaction_${process.env.ETH_NETWORK}`;
const CACHED_BLOCKS = {};

class ERC20TransactionCrawler {

  start () {
    async.waterfall([
      (next) => {
        if (LATEST_PROCESSED_BLOCK > 0) {
          return next(null, LATEST_PROCESSED_BLOCK);
        }

        this._queryLatestBlock(next);
      },
      (latestProcessedBlock, next) => {
        this.processBlocks(latestProcessedBlock, next);
      }
    ], (err, ret) => {
      if (err) {
        logger.error(err);
        logger.info(`Crawler will be restarted in a few seconds...`);
      } else {
        logger.info(`Already processed the newest block. Crawler will be restarted in a few seconds...`);
      }

      setTimeout(this.start.bind(this), networkConfig.averageBlockTime);
    });
  }

  processBlocks (latestProcessedBlock, callback) {
    let fromBlockNumber, toBlockNumber;
    async.auto({
      latestOnchainBlock: (next) => {
        if (LATEST_PROCESSED_BLOCK < LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION) {
          return next(null, LATEST_ONCHAIN_BLOCK);
        }

        web3.eth.getBlockNumber(next);
      },
      processBlocksOnce: ['latestOnchainBlock', (ret, next) => {
        LATEST_ONCHAIN_BLOCK = ret.latestOnchainBlock;
        fromBlockNumber = latestProcessedBlock + 1;

        // Crawl the newest block already
        if (fromBlockNumber > LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION) {
          toBlockNumber = latestProcessedBlock;
          return next(null, true);
        }

        toBlockNumber = latestProcessedBlock + BATCH_BLOCK_SIZE;
        if (toBlockNumber > LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION) {
          toBlockNumber = LATEST_ONCHAIN_BLOCK - REQUIRED_CONFIRMATION;
        }

        this._processBlocksOnce(fromBlockNumber, toBlockNumber, next);
      }]
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      if (ret.processBlocksOnce === true) {
        return callback(null, true);
      }

      LATEST_PROCESSED_BLOCK = toBlockNumber;
      process.nextTick(this.start.bind(this));
    });
  }

  _processBlocksOnce (fromBlockNumber, toBlockNumber, callback) {
    logger.info(`_processBlocksOnce: ${fromBlockNumber}→${toBlockNumber}`);
    web3.getLogs({
      fromBlock: web3.utils.toHex(fromBlockNumber),
      toBlock: web3.utils.toHex(toBlockNumber),
      topics: [networkConfig.eventTopics.erc20Transfer]
    }, (err, ret) => {
      if (err) {
        return callback(`Cannot query data from network: ${err.toString()}`);
      }

      this._processLogData(ret, callback);
    });
  }

  _processLogData (arrayData, callback) {
    const now = Date.now();
    const records = [];
    async.forEachLimit(arrayData, 100, (log, next) => {
      if (!log.topics || log.topics.length !== 3 || log.topics[0] !== networkConfig.eventTopics.erc20Transfer) {
        return next(null, null);
      }

      const data = web3.utils.hexToBytes(log.data);
      if (data.length !== 32) {
        return next(null, null);
      }

      const contractAddress = log.address.toLowerCase();
      const tokenDef = tokensByAddress[contractAddress];
      const coin = tokenDef ? tokenDef.symbol : contractAddress;
      const blockNumber = log.blockNumber;
      const fromAddress = web3.eth.abi.decodeParameter('address', log.topics[1]);
      const toAddress = web3.eth.abi.decodeParameter('address', log.topics[2]);
      const amount = web3.eth.abi.decodeParameter('uint256', log.data);

      this._getOneBlock(blockNumber, (err, block) => {
        if (err) {
          return next(err);
        }

        records.push({
          coin: coin,
          contract_address: contractAddress,
          block_number: log.blockNumber,
          block_hash: log.blockHash,
          block_timestamp: block.timestamp,
          txid: log.transactionHash,
          nonce: -1,
          transaction_index: -1,
          from: fromAddress,
          to: toAddress,
          value: amount,
          status: Const.TX_STATUS.SUCCESS,
          log_id: log.id,
          created_at: now,
          updated_at: now,
          created_by: 0,
          updated_by: 0,
        });

        next(null, null);
      });
    }, (err) => {
      if (err) {
        return callback(err);
      }

      knex.raw(knex(TRANSACTION_TABLE_NAME).insert(records).toString().replace('insert', 'INSERT IGNORE'))
          .then((ret) => {
            logger.info(`Inserted ${records.length} transactions.`);
            return callback(null, null);
          })
          .catch((error) => {
            return callback(error);
          });
    });
  }

  _getOneBlock (blockNumber, callback) {
    if (CACHED_BLOCKS[blockNumber]) {
      return callback(null, CACHED_BLOCKS[blockNumber]);
    }

    web3.eth.getBlock(blockNumber, (err, block) => {
      if (err) {
        return callback(err);
      }

      CACHED_BLOCKS[blockNumber] = block;
      return callback(null, block);
    });
  }

  _queryLatestBlock (callback) {
    knex(TRANSACTION_TABLE_NAME)
      .max('block_number as max')
      .whereNot('coin', Const.COIN.ETH)
      .then((ret) => {
        let maxBlockNumber = networkConfig.startCrawlBlock;
        try {
          maxBlockNumber = (parseInt(ret[0].max));
        } catch (e) {
          return callback(null, networkConfig.startCrawlBlock);
        }

        if (isNaN(maxBlockNumber) || maxBlockNumber < networkConfig.startCrawlBlock) {
          maxBlockNumber = networkConfig.startCrawlBlock;
        }

        return callback(null, maxBlockNumber - 1);
      })
      .catch((err) => {
        logger.fatal(err);
        return callback(err);
      });
  }

};

module.exports = ERC20TransactionCrawler;
