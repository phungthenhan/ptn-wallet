const _           = require('lodash');
const async       = require('async');
const Const       = require('../../app/common/Const');
const ExSession   = require('sota-core').load('common/ExSession');
const Utils       = require('sota-core').load('util/Utils');
const logger      = require('sota-core').getLogger('BaseWithdrawalProcessor');

const TIMER = 2000;

class BaseWithdrawalProcessor {

  start () {
    /**
     * Model classname name can be:
     * - EthWithdrawalModel
     * - EtcWithdrawalModel
     * - DashWithdrawalModel
     * - Erc20WithdrawalModel
     * - QtumWithdrawalModel
     */
    if (!this._withdrawalModelClassname) {
      throw new Error(`Could not start due to invalid withdrawal model: ${this._withdrawalModelClassname}`);
    }

    if (!this._requiredConfirmations) {
      this._requiredConfirmations = 32;
    }

    this.findAndSend();
    this.findAndVerify();
  }

  findAndSend () {
    const exSession = new ExSession();
    const now = Utils.now();
    const WithdrawalModel = exSession.getModel(this._withdrawalModelClassname);
    const WalletWebhookService = exSession.getService('WalletWebhookService');
    let withdrawal = null;

    async.waterfall([
      (next) => {
        WithdrawalModel.findOne({
          where: 'status = ?',
          params: [Const.WITHDRAW_STATUS.SENT],
          orderBy: 'tried_at'
        }, (err, ret) => {
          if (err){
            return next(err);
          }
          if (ret){
            return next(`Waiting for sent transaction ...`);
          } else {
            return next(null, null);
          }
        });
      },
      (ret, next) => {
        WithdrawalModel.findOne({
          where: 'status = ?',
          params: [Const.WITHDRAW_STATUS.SIGNED],
          orderBy: 'tried_at',
          lockMode: ' FOR UPDATE',
        }, next);
      },

      (record, next) => {
        if (!record) {
          logger.info(`${this.classname}::findAndSend: no record to process...`);
          return next(null, null);
        }
        this._checkTransactionFee(record, next);
      },

      (record, next) => {
        if (!record) {
          return next(null, null);
        }
        withdrawal = record;
        const prevTxid = `${record.txid}`;
        this._doSendTransaction(record, (err, ret) => {
          // check if Txid was changed, push to trace array
          const currentTxid = `${record.txid}`;
          if (currentTxid !== prevTxid) {
            const txChangedInfo = {
              type: Const.WEBHOOK_TYPE.TXCHANGED,
              from: prevTxid,
              to: currentTxid,
            }
            WalletWebhookService.createTxChangedWebhookProgress(this._getCoinSymbol(record), record.walletId, record.txid, txChangedInfo, next);
          } else {
            return next(err, ret);
          }
        });
      },
      (ret, next) => {
        exSession.commit(next);
      }
    ], (err, record) => {
      exSession.destroy();

      if (err) {
        if (err.toString().includes('Waiting for sent transaction')){
          logger.warn(`${err.toString()}`);
        } else {
          logger.error(`findAndSend something went wrong: ${err.toString()}`);
        }
      }

      setTimeout(() => {
        this.findAndSend();
      }, TIMER);
    });
  }

  _checkTransactionFee (withdrawalRecord, callback) {
    return callback(null, withdrawalRecord);
  }

  _checkRewriteWithdrawal (record, callback) {
    // Default is no rewrite
    // This method should be overriden in derived classes.
    const isRewritten = false;
    return callback(null, isRewritten);
  }

  _updateVerifiedData (tx, block, record, callback) {
    const now = Utils.now();
    let isConfirmed = false;
    isConfirmed = isConfirmed || !!(tx && !block && tx.status === Const.TX_STATUS.FAILED);
    isConfirmed = isConfirmed || !!(tx && block && block.confirmations >= this._requiredConfirmations);

    if (isConfirmed) {
      record.setData({
        block_number: block ? block.number : null,
        block_hash: block ? block.hash : null,
        block_timestamp: block ? block.timestamp : null,
        fee: tx.fee,
        tx_status: tx.status,
        status: tx.status === Const.TX_STATUS.SUCCESS ? Const.WITHDRAW_STATUS.COMPLETED : Const.WITHDRAW_STATUS.FAILED,
      });
    }

    record.setData({
      tried_at: now
    }).save((err, ret) => {
      if (err) {
        return callback(err);
      }

      return callback(null, isConfirmed);
    });
  }

  findAndVerify () {
    const exSession = new ExSession();
    const now = Utils.now();
    const WithdrawalModel = exSession.getModel(this._withdrawalModelClassname);
    const WalletBalanceModel = exSession.getModel('WalletBalanceModel');

    async.auto({
      record: (next) => {
        WithdrawalModel.findOne({
          where: 'status = ?',
          params: [Const.WITHDRAW_STATUS.SENT],
          orderBy: 'tried_at'
        }, next);
      },
      isRewritten: ['record', (ret, next) => {
        if (!ret.record) {
          return next(`${this.classname}::findAndVerify: no record to process...`);
        }

        this._checkRewriteWithdrawal(ret.record, next);
      }],
      tx: ['isRewritten', (ret, next) => {
        if (ret.isRewritten) {
          return next(`Transaction ${ret.record.txid} will be rewritten...`);
        }

        this._getTxInfo(ret.record, next);
      }],
      block: ['tx', (ret, next) => {
        if (!ret.tx || (!ret.tx.blockNumber && !ret.tx.blockHash)) {
          return next(null, null);
        }

        this._getBlockInfo(ret.tx.blockNumber || ret.tx.blockHash, next);
      }],
      verify: ['block', (ret, next) => {
        const tx = ret.tx;
        const block = ret.block;
        const record = ret.record;

        this._updateVerifiedData(tx, block, record, next);
      }],
      subtractPending: ['verify', (ret, next) => {
        if (!ret.verify) {
          return next(null, null);
        }

        const amount = ret.record.amount;
        const walletId = ret.record.walletId;
        const coin = this._getCoinSymbol(ret.record);

        if (ret.tx.status === Const.TX_STATUS.SUCCESS) {
          WalletBalanceModel.verifyWithdrawal(ret.record, walletId, coin, amount, next);
        } else {
          WalletBalanceModel.invalidateWithdrawal(ret.record, walletId, coin, amount, next);
        }
      }],
      commit: ['subtractPending', (ret, next) => {
        exSession.commit(next);
      }],
    }, (err, ret) => {
      exSession.destroy();

      if (err) {
        logger.info(err.toString());
      }

      setTimeout(() => {
        this.findAndVerify();
      }, TIMER);
    });
  }
}

module.exports = BaseWithdrawalProcessor;
