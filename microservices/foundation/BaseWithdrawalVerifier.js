const _               = require('lodash');
const async           = require('async');
const BaseWProcessor   = require('./BaseWProcessor');
const Const           = require('../../app/common/Const');
const ExSession       = require('sota-core').load('common/ExSession');
const Utils           = require('sota-core').load('util/Utils');
const logger          = require('sota-core').getLogger('BaseWithdrawalVerifier');

class BaseWithdrawalVerifier extends BaseWProcessor {

  run (callback) {
    async.auto({
      sentRecord: (next) => {
        this.findOneSentWithdrawal(next);
      },
      txInfo: ['sentRecord', (ret, next) => {
        this.getTxInfo(ret.sentRecord, next);
      }],
      updateConfirmedData: ['sentRecord', 'txInfo', (ret, next) => {
        this.updateConfirmedData(ret.sentRecord, ret.txInfo, next);
      }],
      updateWalletBalance: ['sentRecord', 'txInfo', (ret, next) => {
        this.updateWalletBalance(ret.sentRecord, ret.txInfo, next);
      }],
    }, callback);
  }

  findOneSentWithdrawal (callback) {
    const WithdrawalModel = this.getModel(this.constructor.getWithdrawalModelName());
    WithdrawalModel.findLatestWithdrawalByStatus(Const.WITHDRAW_STATUS.SENT, callback);
  }

  getTxInfo (withdrawal, callback) {
    // TODO
    return callback(null, null);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    const now = Utils.now();
    let isConfirmed = false;
    const tx = txInfo;

    isConfirmed = isConfirmed || !!(tx && tx.status === Const.TX_STATUS.FAILED);
    isConfirmed = isConfirmed || !!(tx && tx.confirmations >= this.constructor.getNetworkConfig().requiredConfirmation);

    if (isConfirmed) {
      withdrawal.setData({
        block_number: tx.blockNumber,
        block_hash: tx.blockHash,
        block_timestamp: tx.timestamp,
        fee: tx.fee,
        tx_status: tx.status,
        status: tx.status === Const.TX_STATUS.SUCCESS ? Const.WITHDRAW_STATUS.COMPLETED : Const.WITHDRAW_STATUS.FAILED,
      });
    }

    withdrawal.setData({
      tried_at: now
    }).save((err, ret) => {
      if (isConfirmed){
        return callback(err, ret);
      } else {
        return callback(null, null);
      }
    });
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    let isConfirmed = false;
    const tx = txInfo;
    isConfirmed = isConfirmed || !!(tx && tx.status === Const.TX_STATUS.FAILED);
    isConfirmed = isConfirmed || !!(tx && tx.confirmations >= this.constructor.getNetworkConfig().requiredConfirmation);

    if (!isConfirmed){
      return callback(null, null);
    }

    const WalletBalanceModel = this.getModel('WalletBalanceModel');
    const amount = withdrawal.amount;
    const walletId = withdrawal.walletId;
    const coin = this.constructor.getCoinSymbol(withdrawal);

    if (txInfo.status === Const.TX_STATUS.SUCCESS) {
      WalletBalanceModel.verifyWithdrawal(withdrawal, walletId, coin, amount, callback);
    } else {
      WalletBalanceModel.invalidateWithdrawal(withdrawal, walletId, coin, amount, callback);
    }
  }
}

module.exports = BaseWithdrawalVerifier;