const _                         = require('lodash');
const Const                     = require('../../app/common/Const');
const BaseDepositCrawler        = require('./BaseDepositCrawler');

class UTXOBasedDepositCrawler extends BaseDepositCrawler {

  extractAddressesFromTx (tx) {
    /**
     * TBD: considering to construct bitcore.Transaction object
     * by the raw transaction (hex data)
     * Or just use json version that is returned from bitcore node
     */
    return _.chain(tx.vout)
            .map(this._extractAddressesFromOutput)
            .compact()
            .uniq()
            .value();
  }

  _extractAddressesFromOutput (output) {
    if (!output.scriptPubKey) {
      return null;
    }

    const scriptPubKey = output.scriptPubKey;

    // Currently only support P2PK and P2PKH transaction
    if (scriptPubKey.type != 'pubkey' && scriptPubKey.type != 'pubkeyhash') {
      return null;
    }

    // And the recipient is a normal address only
    return scriptPubKey.addresses[0];
  }

  /**
   * Outputs have simple format:
   * {
   *   address: int,
   *   amount: int,
   * }
   */
  extractOutputsFromTx (tx) {
    const vouts = tx.vout || [];
    const addressAmounts = {};

    _.forEach(vouts, (vout) => {
      if (!vout.scriptPubKey) {
        return null;
      }

      const scriptPubKey = vout.scriptPubKey;

      // Currently only support P2PK and P2PKH transaction
      // Didn't support multisig & P2SH yet
      if (scriptPubKey.type != 'pubkey' && scriptPubKey.type != 'pubkeyhash') {
        return null;
      }

      if (scriptPubKey.addresses.length > 1) {
        throw new Error('Only support normal P2PKH transaction for now.');
      }

      // Addresses array should have only 1 item, take it
      const address = scriptPubKey.addresses[0];

      // If address is not a watching one, ignore
      if (!this._cachedAddresses[address]) {
        return;
      }

      // Add accumulation output value for address
      if (!addressAmounts[address]) {
        addressAmounts[address] = 0;
      }

      addressAmounts[address] += vout.value * Const.SATOSHI_FACTOR
    });

    return _.map(Object.keys(addressAmounts), (address) => {
      return {
        address: address,
        amount: addressAmounts[address],
      };
    });
  }
}

module.exports = UTXOBasedDepositCrawler;
