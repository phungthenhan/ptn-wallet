const _                     = require('lodash');
const async                 = require('async');
const uuid                  = require('uuid/v1');
const Checkit               = require('cc-checkit');
const latestProcessedBlock  = require('./getLatestProcessedBlockNumber');
const Const                 = require('../../app/common/Const');
const ExSession             = require('sota-core').load('common/ExSession');
const logger                = require('sota-core').getLogger('DepositCrawlerManager');

// Store in-progress block
const LATEST_PROCESSED_BLOCK = {};

class DepositCrawlerManager {

  constructor () {
    // Generate unique id for each process
    this._id = uuid();
  }

  start (CrawlerClass) {
    this._crawlerClass = CrawlerClass;
    this._coinSymbol = CrawlerClass.getCoinSymbol();

    if (!this._coinSymbol) {
      throw new Error(`Could not start due to invalid coin symbol: ${this._coinSymbol}`);
    }

    // Model prefix is first-character capitalized version of coin symbol
    this._modelPrefix = this._coinSymbol.charAt(0).toUpperCase() + this._coinSymbol.slice(1);

    const networkConfig = CrawlerClass.getNetworkConfig();
    const [err, params] = new Checkit({
      startCrawlBlock: ['required', 'naturalNonZero'],
      requiredConfirmation: ['required', 'naturalNonZero'],
      averageBlockTime: ['required', 'naturalNonZero'],
    }).validateSync(networkConfig);

    if (err) {
      logger.error(err.toString());
      throw new Error(`Could not start due to invalid network config: ${JSON.stringify(networkConfig)}`);
    }

    this._startCrawlBlock = networkConfig.startCrawlBlock;
    this._requiredConfirmations = networkConfig.requiredConfirmation;
    this._averageBlockTime = networkConfig.averageBlockTime;

    this._batchBlockSize = CrawlerClass.getBlockNumInOneGo();
    this._batchTxSize = CrawlerClass.getTxNumInOneGo();

    this.doCrawl();
  }

  /**
   * The crawler's main loop
   */
  doCrawl () {
    async.auto({
      latestProcessedBlock: (next) => {
        const cachedLatestProcessedBlock = LATEST_PROCESSED_BLOCK[this._id];
        if (cachedLatestProcessedBlock > 0) {
          return next(null, cachedLatestProcessedBlock);
        }

        latestProcessedBlock(this._coinSymbol, Const.PROCESSED_BLOCK_TYPE.DEPOSIT, this._startCrawlBlock, next);
      },
      latestNetworkBlock: (next) => {
        this._crawlerClass.getLatestBlockOnNetwork(next);
      }
    }, (err, ret) => {
      /**
       * Opps! Bad thing happens
       * Will try to re-crawl in the next tick
       */
      if (err) {
        logger.error(err);
        logger.error('Something went wrong while checking latest blocks. Crawler will be restarted...');
        setTimeout(this.doCrawl.bind(this));
        return;
      }

      /**
       * Everything is ok, just go ahead.
       */
      this.doProcess(ret.latestProcessedBlock, ret.latestNetworkBlock);
    });
  }

  /**
   * The main flow
   */
  doProcess (latestProcessedBlock, latestNetworkBlock) {
    /**
     * Start with the next block of the latest processed one
     */
    const fromBlockNumber = latestProcessedBlock + 1;

    /**
     * The highest block that has enough confirmations to trust
     */
    const latestTrustedBlock = latestNetworkBlock - this._requiredConfirmations;

    /**
     * If crawled the newest block already
     * Wait for a period that is equal to average block time
     * Then try crawl again (hopefully new block will be available then)
     */
    if (fromBlockNumber > latestTrustedBlock) {
      logger.info(`Block <${fromBlockNumber}> is the newest block can processed already. Wait for the next tick...`);
      setTimeout(this.doCrawl.bind(this), this._averageBlockTime);
      return;
    }

    /**
     * Try to process several blocks
     */
    let toBlockNumber = latestProcessedBlock + this._batchBlockSize;

    /**
     * Of course we only select the trusted ones
     */
    if (toBlockNumber > latestTrustedBlock) {
      toBlockNumber = latestTrustedBlock;
    }

    this._processBlocksOnce(fromBlockNumber, toBlockNumber, (err) => {
      /**
       * Opps! Bad thing happens
       * Will try to re-crawl in the next tick
       */
      if (err) {
        logger.error(err);
        logger.error('Something went wrong while checking latest blocks. Crawler will be restarted...');
        setTimeout(this.doCrawl.bind(this));
        return;
      }

      /**
       * Cache the latest processed block number
       * Do the loop again in the next tick
       */
      LATEST_PROCESSED_BLOCK[this._id] = toBlockNumber;
      setTimeout(this.doCrawl.bind(this));
    });
  }

  /**
   * Process several blocks in 1 go
   * Just use single database transaction
   */
  _processBlocksOnce (fromBlockNumber, toBlockNumber, callback) {
    logger.info(`${this._coinSymbol}::_processBlocks: ${fromBlockNumber}→${toBlockNumber}`);
    const exSession = new ExSession();
    const crawler = new this._crawlerClass(exSession);
    async.waterfall([
      (next) => {
        // Get all transactions within the block range
        crawler.getBlockTransactions(fromBlockNumber, toBlockNumber, next);
      },
      (txs, next) => {
        // Get watching addresses
        const addresses = _.chain(txs).reduce((ret, tx) => {
          const addrs = crawler.extractAddressesFromTx(tx);
          crawler.cacheAddressesAndTransactionsAfterExtract(tx, addrs);
          return ret.concat(addrs);
        }, []).uniq().value();
        this._filterWatchingAddresses(exSession, addresses, next);
      },
      (watchingAddresses, next) => {
        crawler.cacheAddresses(watchingAddresses);
        crawler.getWatchingTransactionsFromCache(_.map(watchingAddresses, 'address'), next);
      },
      (txs, next) => {
        async.forEachLimit(txs, this._batchTxSize, (tx, _next) => {
          crawler.processOneTransaction(tx, _next);
        }, next);
      },
      (ret, next) => {
        if (typeof ret === 'function') {
          next = ret;
        }

        const LatestBlockModel = exSession.getModel('LatestBlockModel');
        LatestBlockModel.update({
          set: 'block_number = ? ',
          where: 'coin = ? AND type = ?' ,
          params: [toBlockNumber, this._coinSymbol, Const.PROCESSED_BLOCK_TYPE.DEPOSIT],
        }, next);
      },
      (ret, next) => {
        exSession.commit(next);
      }
    ], (err, ret) => {
      exSession.destroy();
      return callback(err, ret);
    });
  }

  _filterWatchingAddresses (exSession, addresses, callback) {

    if (addresses.length == 0) {
      return callback(null, []);
    }

    const CoinAddressModel = exSession.getModel(this._crawlerClass.getAddresModelName());
    CoinAddressModel.find({
      where: CoinAddressModel.whereIn('address', addresses.length),
      params: addresses
    }, callback);
  }
}

module.exports = DepositCrawlerManager;
