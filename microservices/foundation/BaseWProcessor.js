const _         = require('lodash');
const async     = require('async');
const logger    = require('sota-core').getLogger('BaseWProcessor');

class BaseWProcessor {

  constructor (exSession) {
    this._exSession = exSession;
  }

  static getCoinSymbol () {
    throw new Error('Implement me in derived class.');
  }

  static getNetworkConfig () {
    throw new Error('Implement me in derived class.');
  }

  static getWithdrawalModelName () {
    throw new Error('Implement me in derived class.');
  }

  static getHotWalletModelName () {
    throw new Error('Implement me in derived class.');
  }

  getModel (model) {
    return this._exSession.getModel(model);
  }

  getService (service) {
    return this._exSession.getService(service);
  }

  run (callback) {
    throw new Error('This method should be implemented in derived class.');
  }

  logAction (callback) {
    // TODO: record change log of withdrawal record
  }

}

module.exports = BaseWProcessor;
