const _         = require('lodash');
const async     = require('async');
const logger    = require('sota-core').getLogger('BaseDepositCrawler');

class BaseDepositCrawler {

  constructor (exSession) {
    this._exSession = exSession;
    this._cachedTxsByHash = {};
    this._cachedTxidsByAddress = {};
    this._cachedAddresses = {};
  }

  static getCoinSymbol () {
    throw new Error('Implement me in derived class.');
  }

  static getNetworkConfig () {
    throw new Error('Implement me in derived class.');
  }

  static getAddresModelName () {
    throw new Error('Implement me in derived class.');
  }

  static getDepositModelName () {
    throw new Error('Implement me in derived class.');
  }

  /**
   * Here comes magic numbers
   * Should be overriden in derived classes
   */
  static getBlockNumInOneGo () {
    return 5;
  }

  static getTxNumInOneGo () {
    return 10;
  }

  getModel (model) {
    return this._exSession.getModel(model);
  }

  getService (service) {
    return this._exSession.getService(service);
  }

  cacheAddresses (addressEntities) {
    _.each(addressEntities, entity => {
      this._cachedAddresses[entity.address] = entity;
    });
  }

  cacheAddressesAndTransactionsAfterExtract (tx, addresses) {
    const txid = tx.hash || tx.txid;

    this._cachedTxsByHash[txid] = tx;
    _.each(addresses, address => {
      if (!this._cachedTxidsByAddress[address]) {
        this._cachedTxidsByAddress[address] = [];
      }

      this._cachedTxidsByAddress[address].push(txid);
    });
  }

  getWatchingTransactionsFromCache (watchingAddresses, callback) {
    const txids = _.reduce(watchingAddresses, (ret, address) => {
      return ret.concat(this._cachedTxidsByAddress[address]);
    }, []);

    const txs = _.map(_.uniq(txids), txid => {
      return this._cachedTxsByHash[txid];
    });

    return callback(null, txs);
  }

  processOneTransaction (tx, callback) {
    if (!this._cachedAddresses || !Object.keys(this._cachedAddresses).length) {
      return callback(null, null);
    }

    const coin = this.constructor.getCoinSymbol();
    const CoinDepositModel = this.getModel(this.constructor.getDepositModelName());
    const WalletWebhookService = this.getService('WalletWebhookService');
    const WalletService = this.getService('WalletService');
    const txid = tx.txid;
    let entities = [];

    try {
      entities = this.constructDepositEntitiesFromTx(tx);
    } catch (err) {
      logger.error(`Something went wrong when trying to contruct deposits from tx: ${JSON.stringify(tx)}`);
      return callback(err.toString());
    }

    if (!entities.length) {
      return callback(null, null);
    }

    async.parallel([
      (next) => {
        const options = {};
        async.forEach(entities, (data, _next) => {
          WalletService.handleDeposit(data.wallet_id, coin, CoinDepositModel, data, options, _next);
        }, next);
      },
      (next) => {
        const walletIds = _.map(entities, 'wallet_id');
        WalletWebhookService.createMultiWalletDepositWebhookProgress(coin, walletIds, txid, next);
      }
    ], callback);
  }

  constructDepositEntitiesFromTx (tx) {
    /**
     * Outputs have simple format:
     * {
     *   address: int,
     *   amount: int,
     * }
     */
    const outputs = this.extractOutputsFromTx(tx);
    if (!outputs || !outputs.length) {
      return [];
    }

    logger.info(`Got outputs from <${tx.txid}>: ${JSON.stringify(outputs)}`);
    return _.reduce(outputs, (ret, output) => {
      const address = output.address;
      const amount = output.amount;
      const walletId = this._cachedAddresses[address].walletId;

      return ret.concat([
        {
          block_number: tx.height,
          block_hash: tx.blockhash,
          block_timestamp: tx.time,
          txid: tx.txid,
          wallet_id: walletId,
          address: address,
          amount: amount,
        }
      ]);
    }, []);
  }

}

module.exports = BaseDepositCrawler;
