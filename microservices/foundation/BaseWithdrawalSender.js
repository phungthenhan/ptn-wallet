const _               = require('lodash');
const async           = require('async');
const BaseWProcessor   = require('./BaseWProcessor');
const Const           = require('../../app/common/Const');
const ExSession       = require('sota-core').load('common/ExSession');
const Utils           = require('sota-core').load('util/Utils');
const logger          = require('sota-core').getLogger('BaseWithdrawalSender');

class BaseWithdrawalSender extends BaseWProcessor {

  run (callback) {
    async.auto({
      signedRecord: (next) => {
        this.findOneSignedWithdrawal(next);
      },
      send: ['signedRecord', (ret, next) => {
        this.doSendTransaction(ret.signedRecord, next);
      }]
    }, callback);
  }

  findOneSignedWithdrawal (callback) {
    const WithdrawalModel = this.getModel(this.constructor.getWithdrawalModelName());
    async.auto({
      get: (next) => {
        // get One
        WithdrawalModel.findOne({
          where: 'status = ?',
          params: [Const.WITHDRAW_STATUS.SIGNED],
          orderBy: 'tried_at',
        }, next);
      }

    }, (err, ret) => {
      if (err) return callback(err);
      return callback(null, ret.get);
    })
  }

  doSendTransaction (withdrawal, callback) {
    // TODO
    return callback(null, null);
  }

  afterSendTransaction (err, ret, withdrawal, callback) {
    // Override if needed
    const now = Utils.now();
    if (err) {
      logger.error(`Failed to sent signed transaction ${withdrawal.txid}: ${err.toString()}`);
      withdrawal.setData({
        status: Const.WITHDRAW_STATUS.UNSIGNED
      });
    } else {
      logger.info(`BaseWithdrawalSender::_doSendTransaction: ${withdrawal.txid} Transaction was sent`);
      withdrawal.setData({
        status: Const.WITHDRAW_STATUS.SENT
      });
    }
    withdrawal.setData({
      tried_at: now
    }).save(callback);
  }
}

module.exports = BaseWithdrawalSender;
