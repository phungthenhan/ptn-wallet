const _                   = require('lodash');
const async               = require('async');
const Const               = require('../../app/common/Const');
const ExSession           = require('sota-core').load('common/ExSession');
const logger              = require('sota-core').getLogger('WithdrawalManager');

const TIMER = 10000;

class WithdrawalManager {

  start (SignerClass, SenderClass, VerifierClass) {
    this.runEternalProcessor(SignerClass);
    this.runEternalProcessor(SenderClass);
    this.runEternalProcessor(VerifierClass);
  }

  runEternalProcessor (ProcessorClass) {
    const exSession = new ExSession();
    const processor = new ProcessorClass(exSession);

    async.auto({
      run: (next) => {
        processor.run(next);
      },
      commit: ['run', (ret, next) => {
        exSession.commit(next);
      }]
    }, (err) => {
      exSession.destroy();

      if (err && err !== Const.ASYNC_BREAKER) {
        logger.error(err);
      }

      const eternalTask = this.runEternalProcessor.bind(this, ProcessorClass);
      setTimeout(eternalTask, TIMER);
    });
  }

}

module.exports = WithdrawalManager;
