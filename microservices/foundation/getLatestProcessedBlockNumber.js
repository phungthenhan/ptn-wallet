const async         = require('async');
const ExSession     = require('sota-core').load('common/ExSession');

module.exports = (coin, type, initialBlockNumber, callback) => {
  const exSession = new ExSession();
  const LatestBlockModel = exSession.getModel('LatestBlockModel');
  let result = initialBlockNumber;

  async.waterfall([
    (next) => {
      LatestBlockModel.findOneOrInsert(coin, type, initialBlockNumber, next);
    },
    (record, next) => {
      if (record.blockNumber > 0) {
        result = record.blockNumber;
      }

      exSession.commit(next);
    }
  ], (err, ret) => {
    exSession.destroy();

    if (err) {
      return callback(err);
    }

    // Check the latest block one more time to
    // ensure everything is processed
    return callback(null, result - 1);
  });
}