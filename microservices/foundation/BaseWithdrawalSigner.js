const _                   = require('lodash');
const async               = require('async');
const BaseWProcessor       = require('./BaseWProcessor');
const Const               = require('../../app/common/Const');
const Utils               = require('sota-core').load('util/Utils');
const logger              = require('sota-core').getLogger('BaseWithdrawalSigner');

class BaseWithdrawalSigner extends BaseWProcessor {

  /**
   * Find all unsigned withdrawal records
   * Then build and sign transaction
   * Update raw_tx and txid back to withdrawal
   */
  run (callback) {
    async.auto({
      unsignedRecord: (next) => {
        this.findOneUnsignedWithdrawal(next);
      },
      hotWallets: (next) => {
        this.findAvailableHotWallets(next);
      },
      sign: ['unsignedRecord', 'hotWallets', (ret, next) => {
        this.signWithdrawalTransaction(ret.unsignedRecord, ret.hotWallets, next);
      }],
    }, callback);
  }

  /**
   * Find the oldest unsigned withdrawal record
   * Return an instance of withdrawal entity
   */
  findOneUnsignedWithdrawal (callback) {
    const WithdrawalModel = this.getModel(this.constructor.getWithdrawalModelName());
    WithdrawalModel.findLatestWithdrawalByStatus(Const.WITHDRAW_STATUS.UNSIGNED, callback);
  }

  /**
   * Find the hot wallet that is available to send funds (no pending transaction)
   * Return array of hot wallet entity
   */
  findAvailableHotWallets (callback) {
    const WithdrawalModel = this.getModel(this.constructor.getWithdrawalModelName());
    const HotWalletModel = this.getModel(this.constructor.getHotWalletModelName());
    async.auto({
      walletSignedList: (next) => {
        HotWalletModel.getNonSendableWallets(next);
      },
      walletUnsignedList: (next) => {
        HotWalletModel.all(next);
      },

      filter: ["walletUnsignedList", "walletSignedList", (ret, next) => {
        const filteredAddress = _.differenceBy(ret.walletUnsignedList, ret.walletSignedList, "address");
        return next(null, filteredAddress);
      }],

    }, (err, ret) => {
      return callback(err, ret.filter)
    })
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback) {
    const CoinWalletService = this.getService(this.constructor.getCoinService());
    const withdrawInfo = withdrawal.toJSON();
    async.filter(hotWallets, function (hotWallet, _next) {
      withdrawInfo.fromAddress = hotWallet.address;
      CoinWalletService.checkWithdrawBalanceInfo(withdrawInfo, (err, ret) => {
        if (err)
          return _next(err);
        if (ret == false)
          return _next(false);
        return _next(null, ret);
      })
    }, (err, ret) => {
      if (err) return callback(err);
      if (ret.length == 0){
        return callback(`None hot wallet is avaiable`);
      }
      return callback(null, ret);
    });
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    // TODO: from data is stored in withdrwal record,
    // build and sign the transaction,
    // then update status of withdrawal record to `signed`.

    // If hot wallets ran out of coin already,
    // just return error and wait for next tick
    // Don't need to add complicated logic for now.
    return callback(null, null);
  }

  writeTxChangeLog (withdrawal, newTxid, callback){
    const WalletWebhookService = this.getService('WalletWebhookService');
    const TxChangeLogModel = this.getModel('TxChangeLogModel');
    // check if Txid was changed, push to trace array
    const currentTxid = `${withdrawal.txid}`;
    if (currentTxid !== newTxid) {
      const txChangedInfo = {
        type: Const.WEBHOOK_TYPE.TXCHANGED,
        from: currentTxid,
        to: newTxid,
      }
      async.auto({
        webhook: (_next) => {
          WalletWebhookService.createTxChangedWebhookProgress(this.constructor.getCoinSymbol(withdrawal), withdrawal.walletId, withdrawal.txid, txChangedInfo, _next);
        },
        tx_log: (_next) => {
          TxChangeLogModel.appendLog(txChangedInfo, _next);
        }
      }, callback);
    } else {
      return callback(null, null);
    }
  }


}

module.exports = BaseWithdrawalSigner;
