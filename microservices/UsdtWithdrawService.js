require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const UsdtWithdrawalProcessor = require('./usdt/UsdtWithdrawalProcessor.js');
  const verifier = new UsdtWithdrawalProcessor();
  verifier.start();
});

module.exports = app;
module.exports.SotaCore = SotaCore;