require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {

  const Signer = require('./eth/ERC20WithdrawalSigner');
  const Sender = require('./eth/ERC20WithdrawalSender');
  const Verifier = require('./eth/ERC20WithdrawalVerifier');
  const WithdrawalManager = require('./foundation/WithdrawalManager');
  const manager = new WithdrawalManager();
  manager.start(Signer, Sender, Verifier);

});

module.exports = app;
module.exports.SotaCore = SotaCore;
