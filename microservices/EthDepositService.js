require('dotenv').config();
const path                = require('path');
const SotaCore            = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore             = require('bitcore-lib');
const logger              = SotaCore.getLogger('EthDepositService');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});

process.nextTick(() => {
  const EthDepositCrawler = require('./eth/EthDepositCrawler');
  const DepositCrawlerManager = require('./foundation/DepositCrawlerManager');
  const crawler = new DepositCrawlerManager();
  crawler.start(EthDepositCrawler);
});

app.start();
