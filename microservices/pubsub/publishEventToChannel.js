const redis       = require('redis');
const logger      = require('sota-core').getLogger('publishEventToChannel');

'use strict';

let client;
if (process.env.REDIS_SERVER_ADDRESS) {
  client = redis.createClient({
    host: process.env.REDIS_SERVER_ADDRESS,
    port: process.env.REDIS_SERVER_PORT,
    password: process.env.REDIS_SERVER_PASSWORD || undefined
  });

  client.on('error', function (err) {
    logger.error('On redis error: ' + err);
  });
}

module.exports = (channel, eventType, eventData) => {
  let message;
  try {
    message = JSON.stringify({
      type: eventType,
      data: eventData
    });
  } catch (e) {
    logger.error(`Error when trying to publish event type=${eventType} data=${eventData}: ${e.toString()}`);
    return;
  }

  if (message) {
    logger.debug(`publishEventToChannel: channel=${channel} message=${message}`);
    client.publish(channel, message);
  }
};
