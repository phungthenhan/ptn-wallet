const _         = require('lodash');
const redis     = require('redis');
const Const     = require('../../app/common/Const');
const logger    = require('sota-core').getLogger('addEventSubscriber');

'use strict';

let client;
const handlers = {};
if (process.env.REDIS_SERVER_ADDRESS) {
  client = redis.createClient({
    host: process.env.REDIS_SERVER_ADDRESS,
    port: process.env.REDIS_SERVER_PORT,
    password: process.env.REDIS_SERVER_PASSWORD || undefined
  });

  client.on('error', function (err) {
    logger.error('On redis error: ' + err);
  });
}

client.subscribe(Const.PUBSUB_CHANNEL.APP);
client.on('message', (channel, data) => {
  let event;
  try {
    event = JSON.parse(data);
  } catch (e) {
    logger.error(`Cannot parse event from channel=${channel} data=${data}`);
    return;
  }

  if (!event || !event.type) {
    logger.error(`Invalid event data: ${data}`);
    return;
  }

  _.each(handlers[event.type], (handler) => {
    handler(event.data);
  });

});

module.exports = (eventType, handler) => {
  if (typeof eventType !== 'string') {
    logger.error(`Invalid eventType: ${eventType}`);
    return;
  }

  if (typeof handler !== 'function') {
    logger.error(`Invalid handler`);
    return;
  }

  if (!handlers[eventType]) {
    handlers[eventType] = [];
  }

  handlers[eventType].push(handler);
};
