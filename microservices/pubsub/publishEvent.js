const Const                 = require('../../app/common/Const');
const publishEventToChannel = require('./publishEventToChannel');

module.exports = (eventType, eventData) => {
  const channel = Const.PUBSUB_CHANNEL.APP;
  publishEventToChannel(channel, eventType, eventData);
};
