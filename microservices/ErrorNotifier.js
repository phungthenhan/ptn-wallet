require('dotenv').config();
const _           = require('lodash');
const async       = require('async');
const path        = require('path');
const bitcore     = require('bitcore-lib');
const Const       = require('../app/common/Const');
const SotaCore    = require('sota-core');
const Utils       = SotaCore.load('util/Utils');
const logger      = SotaCore.getLogger('ErrorNotifier');
const redis       = require("redis");
const mailer       = require("nodemailer");
const addEventSubscriber = require('./pubsub/addEventSubscriber');

process.nextTick(() => {
  const errorSubscriber = redis.createClient();
  const botMailer = mailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.ERROR_RECEIVED_EMAIL,
      pass: process.env.ERROR_RECEIVED_PASSWORD
    }
  });
  listenErrors(errorSubscriber, botMailer);
});

function listenErrors(errorSubscriber, botMailer) {
  addEventSubscriber('EVENT_ERROR_RAISED', (message) => {
    logger.info("Error: " + message +  " arrived!")
    const errorOptions = {
      from: process.env.ERROR_RECEIVED_EMAIL,
      to: process.env.ERROR_RECEIVED_EMAIL,
      subject: message.type,
      text: message
    };

    botMailer.sendMail(errorOptions, (err, ret) => {
      if (err) {
        logger.error(err);
      } else {
        logger.info('Email sent: ' + ret.response);
      }
    })
  });
}