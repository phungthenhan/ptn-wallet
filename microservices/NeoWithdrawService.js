require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const NeoWithdrawalProcessor = require('./neo/NeoWithdrawalProcessor.js');
  const verifier = new NeoWithdrawalProcessor();
  verifier.start();
});

module.exports = app;
module.exports.SotaCore = SotaCore;
