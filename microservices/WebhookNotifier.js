require('dotenv').config();
const _           = require('lodash');
const async       = require('async');
const path        = require('path');
const axios       = require('axios');
const Const       = require('../app/common/Const');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');
const ExSession   = SotaCore.load('common/ExSession');
const Utils       = SotaCore.load('util/Utils');
const logger      = SotaCore.getLogger('WebhookNotifier');
const publisher   = require('./pubsub/publishEvent');
const MAX_RETRY_COUNT = 20;
const MAX_PARALLEL_PROCESSES = 5;
const TIMER = 2000;

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  findAndNotify();
});

function findAndNotify() {
  const exSession = new ExSession();
  const WalletWebhookProgressModel = exSession.getModel('WalletWebhookProgressModel');

  async.auto({
    hooks: (next) => {
      WalletWebhookProgressModel.find({
        where: 'is_processed = ? AND retry_count < ?',
        params: [Const.BOOLEAN.FALSE, MAX_RETRY_COUNT],
        orderBy: 'last_try ASC',
        limit: MAX_PARALLEL_PROCESSES,
      }, next);
    },
    notify: ['hooks', (ret, next) => {
      async.each(ret.hooks, _processOneHook, next);
    }],
    commit: ['notify', (ret, next) => {
      exSession.commit(next);
    }],
  }, (err, ret) => {
    exSession.destroy();
    if (err) {
      logger.error(err);
    }

    setTimeout(() => {
      findAndNotify();
    }, TIMER);
  });
}

function _buildTransactionTrace (data) {
  let parsedData = null;
  let stringTraceBuilder = "";
  try {
    parsedData = JSON.parse(data)
  } catch (err){
  }

  if (parsedData!= null){
    stringTraceBuilder += `${parsedData.from}  to ${parsedData.to}`;
  }
  return stringTraceBuilder;
}


function _processOneHook (webhookProgress, callback) {
  const txid = webhookProgress.txid;
  const WalletWebhookModel = webhookProgress.getModel().getModel('WalletWebhookModel');

  async.auto({
    webhook: (next) => {
      WalletWebhookModel.findOne(webhookProgress.webhookId, next);
    },
    notify: ['webhook', (ret, next) => {
      const webhook = ret.webhook;
      if (!webhook) {
        return next(`Cannot find webhook: ${webhookProgress.webhookId}`);
      }

      if (webhookProgress.data){
        publisher('EVENT_ERROR_RAISED', 'Transaction is changed: '+ _buildTransactionTrace(webhookProgress.data))
      }
      const params = {
        coin: webhook.coin,
        type: webhook.type,
        wallet: webhook.walletId,
        hash: webhookProgress.txid,
        txChanged: (webhookProgress.data)?`true`:`false`,
        data: (webhookProgress.data)?null:JSON.parse(webhookProgress.data),
      };
      axios
        .post(webhook.url, params)
        .then((response) => {
          if (response.status === 200) {
            webhookProgress.markDone(next);
          } else {
            const msg = JSON.stringify(response.data);
            webhookProgress.markFailed(msg, next);
          }
        })
        .catch((err) => {
          webhookProgress.markFailed(err.toString(), next);
        })
        .catch((err) => {
          return next(err);
        });
    }],
  }, (err, ret) => {
    if (err) {
      return callback(err);
    }

    return callback(null, null);
  });
}
