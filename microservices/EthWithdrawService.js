require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const Signer = require('./eth/EthWithdrawalSigner');
  const Sender = require('./eth/EthWithdrawalSender');
  const Verifier = require('./eth/EthWithdrawalVerifier');
  const WithdrawalManager = require('./foundation/WithdrawalManager');
  const manager = new WithdrawalManager();
  manager.start(Signer, Sender, Verifier);
});
