const _             = require('lodash');
const async         = require('async');
const OmniClient      = require('./../../test/OmniClient.js').OmniClient;
const util          = require('util');
const bitcore       = require('bitcore-lib');
const logger        = require('sota-core').getLogger('ExternalService.getUsdtRPCNode');

// OmniClient.testNet.exodusAddress
// Testnet: mpexoDuSkGGqvqrkrjiFng38QPkJQVFyqv
// Realnet: 1EXoDusjGwvnjZUyKkxZ4UHEf77z6A5S4P

const client = new OmniClient({
  host: process.env.USDT_WALLET_RPC_HOST,
  port: process.env.USDT_WALLET_RPC_PORT,
  user: process.env.USDT_WALLET_RPC_USER,
  pass: process.env.USDT_WALLET_RPC_PASSWORD});

Object.defineProperty(client, 'getBlockTransactions', {
  writable: false,
  value: getBlockTransactions
});

function getBlockTransactions (blockNumber, callback) {
  async.waterfall([
    (next) => {
      client.callRpc('omni_listblocktransactions', [blockNumber], next)
    },
    (idTxs, next) => {
      _getTxsOfBlock(idTxs, next);
    }
  ], callback);
}


Object.defineProperty(client, 'getBatchTransactions', {
  writable: false,
  value: getBatchTransactions
});

function getBatchTransactions (options, callback) {
  let txs = [];
  const fromBlockNumber = options.fromBlockNumber;
  const toBlockNumber = options.toBlockNumber;

  if (!fromBlockNumber || !toBlockNumber) {
    return callback(`You must specify the block range, currently: start=${fromBlockNumber}, end=${toBlockNumber}`);
  }

  if (fromBlockNumber > toBlockNumber) {
    return callback(`Start block must be less or equal end block, currently start=${fromBlockNumber}, end=${toBlockNumber}`);
  }

  const blockNumbers = [];
  for (let no = fromBlockNumber; no <= toBlockNumber; no++) {
    blockNumbers.push(no);
  }

  async.eachLimit(blockNumbers, 1, (blockNumber, next) => {
    client.getBlockTransactions(blockNumber, (err, ret) => {
      if (err) {
        return next(err);
      }
      txs = txs.concat(ret);
      return next(null, null);
    });
  }, (err) => {
    if (err) {
      return callback(err);
    }


    txs = _.compact(txs);
    return callback(null, txs);
  });
}

function _getTxsOfBlock(idTxs, callback) {
  const txs = idTxs;
  const all_txs = [];
  if (txs.length == 0) {
    return callback(null);
  }
  if (txs != null && txs.length > 0) {
    async.each(txs, (txAddress, _next) => {
      client.callRpc('omni_gettransaction', [txAddress],(err, tx) => {
        if (err || tx == null) {
          return _next(null);
        }

        tx.time = tx.blocktime;
        tx.height = tx.block;
        all_txs.push(tx);
        _next(null);
      });

    },(err) => {
      if (err) {
        return callback(err);
      }
      return callback(null, all_txs);
    });
  }
}
module.exports = () => client