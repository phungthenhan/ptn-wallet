const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const UTXOBasedDepositCrawler   = require('../foundation/UTXOBasedDepositCrawler');
const UsdtGateway                = require('../../app/blockchain').UsdtGateway;
const networkConfig             = require('../../config/usdt');
const getLatestProcessedBlock   = require('./getLatestProcessedBlock');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('UsdtDepositCrawler');


class UsdtDepositCrawler extends UTXOBasedDepositCrawler {

  getLatestBlockOnNetwork (callback) {z
    UsdtGateway.getBlockCount((err, ret) => {
      callback(err, ret);
    });
  }

  static getCoinSymbol () {
    return Const.COIN.USDT;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'UsdtAddressModel';
  }

  static getDepositModelName () {
    return 'UsdtDepositModel';
  }

  static getBlockNumInOneGo () {
    return 1;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  static getLatestBlockOnNetwork (callback) {
    UsdtGateway.getBlockCount((err, ret)=>{
      callback(err,ret)
    });
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    UsdtGateway.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  }

  constructDepositEntitiesFromTx (tx) {
    // Override if needed
    return super.constructDepositEntitiesFromTx(tx);
  }

  extractOutputsFromTx (tx) {
    // Override if needed
    return super.extractOutputsFromTx(tx);
  }

  extractAddressesFromTx (tx) {
    /**
     * TBD: considering to construct bitcore.Transaction object
     * by the raw transaction (hex data)
     * Or just use json version that is returned from bitcore node
     */
    return [tx.referenceaddress];
  }

  // extractAddressesFromTx (tx) {
  //   /**
  //    * TBD: considering to construct bitcore.Transaction object
  //    * by the raw transaction (hex data)
  //    * Or just use json version that is returned from bitcore node
  //    */
  //   // one address one transaction
  //   return [tx.referenceaddress];
  // }

  

  // getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
  //   USDTNode.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  // }

  // constructDepositEntitiesFromTx (tx) {
  //   // Override if needed
  //   return super.constructDepositEntitiesFromTx(tx);
  // }

  // /**
  //  * Outputs have simple format:
  //  * {
  //  *   address: int,
  //  *   amount: int,
  //  * }
  //  */
  // extractOutputsFromTx (tx) {
  //   const vouts = tx.vout || [];
  //   const addressAmounts = {};

  //   addressAmounts[tx.referenceaddress] = tx.amount;

  //   return _.map(Object.keys(addressAmounts), (address) => {
  //     return {
  //       address: address,
  //       amount: addressAmounts[address],
  //     };
  //   });
  // }

  // constructDepositEntitiesFromTx (tx) {
  //   /**
  //    * Outputs have simple format:
  //    * {
  //    *   address: int,
  //    *   amount: int,
  //    * }
  //    */
  //   const outputs = this.extractOutputsFromTx(tx);
  //   if (!outputs || !outputs.length) {
  //     return [];
  //   }

  //   logger.info(`Got outputs from <${tx.txid}>: ${JSON.stringify(outputs)}`);
  //   return _.reduce(outputs, (ret, output) => {
  //     const address = output.address;
  //     const amount = output.amount * Const.SATOSHI_FACTOR;
  //     const walletId = this._cachedAddresses[address].walletId;
  //     return ret.concat([
  //       {
  //         block_number: tx.height,
  //         block_hash: tx.blockhash,
  //         block_timestamp: tx.time,
  //         txid: tx.txid,
  //         wallet_id: walletId,
  //         address: address,
  //         amount: amount,
  //       }
  //     ]);
  //   }, []);
  // }
}


module.exports = UsdtDepositCrawler;