const _                         = require('lodash');
const async                     = require('async');
const networkConfig             = require('../../config/usdt');
const Const                     = require('../../app/common/Const');
const getUsdtRPCNode            = require('./getUsdtRPCNode');
const BaseWithdrawalProcessor   = require('../foundation/BaseWithdrawalProcessor');
const ExSession                 = require('sota-core').load('common/ExSession');
const Utils                     = require('sota-core').load('util/Utils');
const logger                    = require('sota-core').getLogger('DashWithdrawalProcessor');

const USDTNode = getUsdtRPCNode();

class UsdtWithdrawalProcessor extends BaseWithdrawalProcessor {
  constructor () {
    super();
    this.classname = 'UsdtWithdrawalProcessor';
    this._coinSymbol = Const.COIN.USDT;
    this._withdrawalModelClassname = 'UsdtWithdrawalModel';
    this._requiredConfirmations = networkConfig.requiredConfirmation;
    this.TIMER = 2000;
  }

  _doSendTransaction (withdrawalRecord, callback){
    const now = Utils.now();
    USDTNode.sendRawTransaction(withdrawalRecord.txRaw, (err, txid) => {
      if (err) {
        logger.error(`Failed to sent signed transaction ${withdrawalRecord.txid}: ${err.toString()}`);
      } else {
        logger.info(`Sent transaction ${withdrawalRecord.txid}. Result: ${txid}`);
        withdrawalRecord.setData({
          txid: txid,
          status: Const.WITHDRAW_STATUS.SENT
        });
      }

      withdrawalRecord.setData({
        tried_at: now
      }).save(callback);
    });
  }

  // unimplemented method
  _rebuildWithdrawal(){}

  _getBlockInfo (blockNumber, callback){
    async.auto({
      block: (next) => {
        // Compatible with both block number and block hash
        if (typeof blockNumber === 'string' && blockNumber.length === 64) {
          USDTNode.getBlock(blockNumber, next);
          return;
        }

        USDTNode.getBlockHash(blockNumber, (err, blockHash) => {
          if (err) {
            return next(err);
          }
          USDTNode.getBlock(blockHash, next);
        });
      }
    }, (err, ret) => {
      if (err) {
        return callback(err);
      }

      const block = ret.block[0];

      if (!block) {
        logger.error(`Something went wrong. Cannot get block: ${blockNumber}`);
        return callback(`Could not get block ${blockNumber}. Wait for the next tick...`);
      }

      return callback(null, {
        number: block.height,
        hash: block.hash,
        timestamp: block.time,
        confirmations: block.confirmations,
      });
    });
  }

  _getTxInfo(withdrawalRecord, callback) {
    const createdAt = withdrawalRecord.createdAt;
    const txid = withdrawalRecord.txid;
    const now = Utils.now();
    const data = {};
    async.auto({
      tx: (next) => {
        USDTNode.omniGetTransaction(txid, next);
      },
    }, (err, ret) => {
      console.log(err);
      if (err) {
        return callback(err);
      }

      const tx = ret.tx[0];
      if (!tx || !tx.blockhash) {
        logger.info(`Still could not get tx info: ${txid}. Wait for the next tick...`);
        return callback(null, null);
      }

      // TODO: get transaction fee and return here.
      return callback(null, {
        blockHash: tx.blockhash,
        status: Const.TX_STATUS.SUCCESS,
      });
    });
  }

  _getCoinSymbol () {
    return Const.COIN.USDT;
  }
}


module.exports = UsdtWithdrawalProcessor;