const _                         = require('lodash');
const async                     = require('async');
const ExSession                 = require('sota-core').load('common/ExSession');

module.exports = (callback) => {
  const exSession = new ExSession();
  const UsdtDepositModel = exSession.getModel('UsdtDepositModel');

  UsdtDepositModel.findOne({
    where: '1=1',
    orderBy: 'block_number DESC'
  }, (err, ret) => {
    exSession.destroy();

    if (err) {
      return callback(err);
    }

    if (!ret) {
      return callback(null, 1);
    }

    return callback(null, ret.blockNumber);
  });

};
