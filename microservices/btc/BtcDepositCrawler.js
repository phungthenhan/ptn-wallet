const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/btc');
const BtcGateway                = require('../../app/blockchain').BtcGateway;
const UTXOBasedDepositCrawler   = require('../foundation/UTXOBasedDepositCrawler');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('BtcDepositCrawler');

class BtcDepositCrawler extends UTXOBasedDepositCrawler {

  static getCoinSymbol () {
    return Const.COIN.BTC;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'BtcAddressModel';
  }

  static getDepositModelName () {
    return 'BtcDepositModel';
  }

  static getBlockNumInOneGo () {
    return 10;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  static getLatestBlockOnNetwork (callback) {
    BtcGateway.getBlockCount(callback);
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    BtcGateway.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  }

  constructDepositEntitiesFromTx (tx) {
    // Override if needed
    return super.constructDepositEntitiesFromTx(tx);
  }

  extractOutputsFromTx (tx) {
    // Override if needed
    return super.extractOutputsFromTx(tx);
  }
}

module.exports = BtcDepositCrawler;
