const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalVerifier      = require('../foundation/BaseWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('BtcWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const BtcGateway               = require('../../app/blockchain').BtcGateway;
const UtxoBasedWithdrawalVerifier = require('../utxo-based/UtxoBasedWithdrawalVerifier');

class BtcWithdrawalVerifier extends UtxoBasedWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.BTC;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'BtcWalletService'
  }

  static getWithdrawalModelName () {
    return 'BtcWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'BtcHotWalletModel';
  }

  static getGateway(){
    return BtcGateway;
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {
    return super.getTxInfo(withdrawal, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = BtcWithdrawalVerifier;