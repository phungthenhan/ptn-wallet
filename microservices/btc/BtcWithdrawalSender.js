const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSender      = require('../foundation/BaseWithdrawalSender');
const logger                    = require('sota-core').getLogger('BtcWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const BtcGateway               = require('../../app/blockchain').BtcGateway;
const UtxoBasedWithdrawalSender = require('../utxo-based/UtxoBasedWithdrawalSender')

class BtcWithdrawalSender extends UtxoBasedWithdrawalSender {

  static getCoinSymbol () {
    return Const.COIN.BTC;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'BtcWalletService'
  }

  static getWithdrawalModelName () {
    return 'BtcWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'BtcHotWalletModel';
  }

  static getGateway(){
    return BtcGateway;
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    return super.doSendTransaction(withdrawal, callback);
  }
}

module.exports = BtcWithdrawalSender;
