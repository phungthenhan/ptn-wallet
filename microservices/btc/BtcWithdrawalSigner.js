const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSigner      = require('../foundation/BaseWithdrawalSigner');
const logger                    = require('sota-core').getLogger('BtcWithdrawalSigner');
const Utils                     = require('sota-core').load('util/Utils');
const BtcGateway               = require('../../app/blockchain').BtcGateway;
const UtxoBasedWithdrawalSigner = require('../utxo-based/UtxoBasedWithdrawalSigner');

class BtcWithdrawalSigner extends UtxoBasedWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.BTC;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'BtcWalletService'
  }

  static getWithdrawalModelName () {
    return 'BtcWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'BtcHotWalletModel';
  }

  static getGateway(){
    return BtcGateway;
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    return super.signWithdrawalTransaction(withdrawal, hotWallets, callback);
  }

  writeTxChangeLog (withdrawal, newTxid, callback) {
    return super.writeTxChangeLog (withdrawal, newTxid, callback);
  }
}

module.exports = BtcWithdrawalSigner;