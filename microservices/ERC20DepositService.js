require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const ERC20DepositCrawler = require('./eth/ERC20DepositCrawler');
  const crawler = new ERC20DepositCrawler();
  crawler.start(process.env.ERC20_TOKEN);
});

module.exports = app;
module.exports.SotaCore = SotaCore;
