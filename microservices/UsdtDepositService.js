require('dotenv').config();
const path                = require('path');
const SotaCore            = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore             = require('bitcore-lib');
const logger              = SotaCore.getLogger('UsdtDepositService');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const UsdtDepositCrawler = require('./usdt/UsdtDepositCrawler');
  const DepositCrawlerManager = require('./foundation/DepositCrawlerManager');
  const crawler = new DepositCrawlerManager();
  crawler.start(UsdtDepositCrawler);
});
