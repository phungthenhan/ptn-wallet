const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const BaseWithdrawalSender      = require('../foundation/BaseWithdrawalSender');
const logger                    = require('sota-core').getLogger('UtxoBasedWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');

class UtxoBasedWithdrawalSender extends BaseWithdrawalSender {

  static getCoinSymbol () {
    return null;
  }

  static getNetworkConfig () {
    return null;
  }

  static getCoinService (){
    return null
  }

  static getWithdrawalModelName () {
    return null;
  }

  static getHotWalletModelName () {
    return null;
  }

  static getGateway(){
    return null;
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    if (!withdrawal){
      return callback(null, null);
    }
    this.constructor.getGateway().sendRawTransaction(withdrawal.txRaw, (err, ret) => {
      return this.afterSendTransaction(err, ret, withdrawal, callback);
    });
  }
}

module.exports = UtxoBasedWithdrawalSender;
