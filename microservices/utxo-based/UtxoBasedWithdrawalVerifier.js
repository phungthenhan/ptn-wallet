const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const BaseWithdrawalVerifier      = require('../foundation/BaseWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('UtxoBasedWithdrawalVerifier');
const Utils                     = require('sota-core').load('util/Utils');

class UtxoBasedWithdrawalVerifier extends BaseWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return null;
  }

  static getNetworkConfig () {
    return null;
  }

  static getCoinService (){
    return null
  }

  static getWithdrawalModelName () {
    return null;
  }

  static getHotWalletModelName () {
    return null;
  }

  static getGateway(){
    return null;
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {

    if (withdrawal == null)
      return callback(Const.ASYNC_BREAKER);
    return this.constructor.getGateway().getTransactionInfo(withdrawal.txid, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = UtxoBasedWithdrawalVerifier;