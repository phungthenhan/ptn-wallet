const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const BaseWithdrawalSigner      = require('../foundation/BaseWithdrawalSigner');
const logger                    = require('sota-core').getLogger('UtxoBasedWithdrawalSigner');
const Utils                     = require('sota-core').load('util/Utils');

class UtxoBasedWithdrawalSigner extends BaseWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return null;
  }

  static getNetworkConfig () {
    return null;
  }

  static getCoinService (){
    return null
  }

  static getWithdrawalModelName () {
    return null;
  }

  static getHotWalletModelName () {
    return null;
  }

  static getGateway(){
    return null;
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    const Coin = this.constructor.getCoinSymbol().charAt(0).toUpperCase() + this.constructor.getCoinSymbol().slice(1).toLowerCase();
    const CoinWalletService = this.getService(`${Coin}WalletService`);
    if (withdrawal == null) {
      return callback(Const.ASYNC_BREAKER);
    }
    const now = Utils.now();
    async.auto({
      checkBalance: (next) => {
        return this.findAvailableBalanceHotWallet(withdrawal, hotWallets, next);
      },

      build: ["checkBalance", (ret, next) => {
        const randomHotWallet = _.sample(ret.checkBalance);

        CoinWalletService.buildWithdrawalData(
          withdrawal.walletId,
          withdrawal.toAddress,
          withdrawal.amount,
          randomHotWallet, next);
      }],

      tx_changed: ["build", (ret, next) => {
        // check if Txid was changed, push to trace array
        const newTxid = ret.build.txid;
        this.writeTxChangeLog(withdrawal, newTxid, next);
      }],

      record: ["tx_changed", (ret, next) => {
        withdrawal.setData({
          status: Const.WITHDRAW_STATUS.SIGNED,
          fee: ret.build.fee,
          txid: ret.build.txid,
          tx_raw: ret.build.tx_raw,
          tried_at: now
        }).save(next);
      }]
    }, callback)
  }

  writeTxChangeLog (withdrawal, newTxid, callback) {
    return super.writeTxChangeLog (withdrawal, newTxid, callback);
  }
}

module.exports = UtxoBasedWithdrawalSigner;