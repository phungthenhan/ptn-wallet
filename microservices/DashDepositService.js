require('dotenv').config();
const path                = require('path');
const SotaCore            = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore             = require('bitcore-lib');
const dashcore            = require('@dashevo/dashcore-lib');
const logger              = SotaCore.getLogger('DashDepositService');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const DashDepositCrawler = require('./dash/DashDepositCrawler');
  const DepositCrawlerManager = require('./foundation/DepositCrawlerManager');
  const crawler = new DepositCrawlerManager();
  crawler.start(DashDepositCrawler);
});
