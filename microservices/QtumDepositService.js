require('dotenv').config();
const path                = require('path');
const SotaCore            = require('sota-core');
// Require in order to prevent original bitcore lib throwing error
const bitcore             = require('bitcore-lib');
const dashcore            = require('@dashevo/dashcore-lib');
const qtumcore            = require('qtumcore-lib');
const logger              = SotaCore.getLogger('QtumDepositService');

const app = SotaCore.createApp({
  rootDir: path.resolve('.'),
  useSocket: false,
});
app.start();

process.nextTick(() => {
  const QtumDepositCrawler = require('./qtum/QtumDepositCrawler');
  const DepositCrawlerManager = require('./foundation/DepositCrawlerManager');
  const crawler = new DepositCrawlerManager();
  crawler.start(QtumDepositCrawler);
});
