const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSender      = require('../foundation/BaseWithdrawalSender');
const logger                    = require('sota-core').getLogger('DashWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const DashGateway               = require('../../app/blockchain').DashGateway;
const UtxoBasedWithdrawalSender = require('../utxo-based/UtxoBasedWithdrawalSender')

class DashWithdrawalSender extends UtxoBasedWithdrawalSender {

  static getCoinSymbol () {
    return Const.COIN.DASH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'DashWalletService'
  }

  static getWithdrawalModelName () {
    return 'DashWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'DashHotWalletModel';
  }

  static getGateway(){
    return DashGateway;
  }

  findOneSignedWithdrawal (callback) {
    return super.findOneSignedWithdrawal(callback);
  }

  doSendTransaction (withdrawal, callback) {
    return super.doSendTransaction(withdrawal, callback);
  }
}

module.exports = DashWithdrawalSender;
