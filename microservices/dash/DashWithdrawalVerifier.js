const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalVerifier      = require('../foundation/BaseWithdrawalVerifier');
const logger                    = require('sota-core').getLogger('DashWithdrawalSender');
const Utils                     = require('sota-core').load('util/Utils');
const DashGateway               = require('../../app/blockchain').DashGateway;
const UtxoBasedWithdrawalVerifier = require('../utxo-based/UtxoBasedWithdrawalVerifier');

class DashWithdrawalVerifier extends UtxoBasedWithdrawalVerifier {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.DASH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'DashWalletService'
  }

  static getWithdrawalModelName () {
    return 'DashWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'DashHotWalletModel';
  }

  static getGateway(){
    return DashGateway;
  }

  findOneSentWithdrawal (callback) {
    return super.findOneSentWithdrawal(callback);
  }

  getTxInfo (withdrawal, callback) {
    return super.getTxInfo(withdrawal, callback);
  }

  updateConfirmedData (withdrawal, txInfo, callback) {
    return super.updateConfirmedData(withdrawal, txInfo, callback);
  }

  updateWalletBalance (withdrawal, txInfo, callback) {
    return super.updateWalletBalance(withdrawal, txInfo, callback);
  }
}

module.exports = DashWithdrawalVerifier;