const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const BaseWithdrawalSigner      = require('../foundation/BaseWithdrawalSigner');
const logger                    = require('sota-core').getLogger('DashWithdrawalSigner');
const Utils                     = require('sota-core').load('util/Utils');
const DashGateway               = require('../../app/blockchain').DashGateway;
const UtxoBasedWithdrawalSigner = require('../utxo-based/UtxoBasedWithdrawalSigner');

class DashWithdrawalSigner extends UtxoBasedWithdrawalSigner {

  static getCoinSymbol (withdrawal) {
    return Const.COIN.DASH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getCoinService (){
    return 'DashWalletService'
  }

  static getWithdrawalModelName () {
    return 'DashWithdrawalModel';
  }

  static getHotWalletModelName () {
    return 'DashHotWalletModel';
  }

  static getGateway(){
    return DashGateway;
  }

  findOneUnsignedWithdrawal (callback) {
    return super.findOneUnsignedWithdrawal(callback);
  }

  findAvailableHotWallets (callback) {
    return super.findAvailableHotWallets(callback);
  }

  findAvailableBalanceHotWallet (withdrawal, hotWallets, callback){
    return super.findAvailableBalanceHotWallet(withdrawal, hotWallets, callback);
  }

  signWithdrawalTransaction (withdrawal, hotWallets, callback) {
    return super.signWithdrawalTransaction(withdrawal, hotWallets, callback);
  }

  writeTxChangeLog (withdrawal, newTxid, callback) {
    return super.writeTxChangeLog (withdrawal, newTxid, callback);
  }
}

module.exports = DashWithdrawalSigner;