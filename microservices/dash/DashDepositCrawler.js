const _                         = require('lodash');
const async                     = require('async');
const Const                     = require('../../app/common/Const');
const networkConfig             = require('../../config/dash');
const DashGateway               = require('../../app/blockchain').DashGateway;
const UTXOBasedDepositCrawler   = require('../foundation/UTXOBasedDepositCrawler');
const ExSession                 = require('sota-core').load('common/ExSession');
const logger                    = require('sota-core').getLogger('DashDepositCrawler');

class DashDepositCrawler extends UTXOBasedDepositCrawler {

  static getCoinSymbol () {
    return Const.COIN.DASH;
  }

  static getNetworkConfig () {
    return networkConfig;
  }

  static getAddresModelName () {
    return 'DashAddressModel';
  }

  static getDepositModelName () {
    return 'DashDepositModel';
  }

  static getBlockNumInOneGo () {
    return 10;
  }

  static getTxNumInOneGo () {
    return 50;
  }

  static getLatestBlockOnNetwork (callback) {
    DashGateway.getBlockCount(callback);
  }

  getBlockTransactions (fromBlockNumber, toBlockNumber, callback) {
    DashGateway.getBatchTransactions({ fromBlockNumber, toBlockNumber }, callback);
  }

  constructDepositEntitiesFromTx (tx) {
    // Override if needed
    return super.constructDepositEntitiesFromTx(tx);
  }

  extractOutputsFromTx (tx) {
    // Override if needed
    return super.extractOutputsFromTx(tx);
  }
}

module.exports = DashDepositCrawler;
