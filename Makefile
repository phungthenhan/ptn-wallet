init-db:
	node bin/init_db.js

schema:
	node bin/generate_schema.js

connect-server-dash:
	ssh ec2-user@13.125.146.174

connect-server-qtum:
	ssh ubuntu@13.209.21.69

connect-gw:
	ssh root@13.125.15.151

deploy-202:
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--exclude .idea \
				--exclude .env \
				--exclude .logs \
				--exclude node_modules \
				. sotatek@192.168.1.202:/home/sotatek/bitkoex-wallet
	ssh sotatek@192.168.1.202 "pm2 restart all -a"

deploy-prod:
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--exclude .idea \
				--exclude .env \
				--exclude .logs \
				--exclude node_modules \
				. ec2-user@wallet:/home/ec2-user/wallet
	ssh ec2-user@wallet "pm2 restart all -a"

deploy-staging:
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--exclude .idea \
				--exclude .env \
				--exclude .logs \
				--exclude node_modules \
				. ec2-user@wallet-stg:/home/ec2-user/wallet
	ssh ec2-user@wallet-stg "pm2 restart all -a"

test-connect-dash:
	curl --user $(u):$(p) --data-binary '{"jsonrpc":"1.0","id":"curltext","method":"getinfo","params":[]}' -H 'content-type:text/plain;' wallet-dash-node:9998

test-connect-qtum:
	curl --user $(u):$(p) --data-binary '{"jsonrpc":"1.0","id":"curltext","method":"getinfo","params":[]}' -H 'content-type:text/plain;' wallet-qtum-node:9998