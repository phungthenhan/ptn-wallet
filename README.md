# BITKOEX WALLET MODULE

## Overview logic architecture
![Overview architecture](docs/Overview.png)

### The actors/components
#### User

#### DASH wallet application:
- The application that users usually use to store and manage their DASH coin. For example Dash Core, Jaxx, Exodus, ...
- See the recommended wallets here: https://www.dash.org/wallets/

#### Webserver:
- Hosts the website that users see and interact with. It's the place to watch the prices, make orders, request deposit/withdraw, ...
- Webserver also contains many microservices like bot, crawler, order processor,...

#### DASH APIs (BitGo-like APIs) server:
- It's a set of wrapper of DASH APIs, which have same format and parameter with BitGo's. Webserver will send all crypto-related business here via http request.
- Documentation for APIs is updated here: https://documenter.getpostman.com/view/1227218/bitkoex/7TGiDnx

#### DASH full node server:
- It's a computer that run a program to connect to the DASH network and fully enforce all of its rules. It's the bridge between Bitkoex system and DASH blockchain network. DASH API server will contact with DASH node server via RPC-JSON protocol.
- DASH full node also hosts the wallet of exchange
- Documentation about contact with a DASH node can be found here: https://dash-docs.github.io/en/developer-reference#dash-core-apis

#### DASH network:
- It's combination of all nodes that is running DASH program.

### Typical use-cases
#### User make a deposit
![Deposit flow](docs/Deposit.png)
- Step 1: User get address to deposit coin from webserver.
- Step 2: After getting deposit address, user use wallet application to send coins to that address.
- Step 3: Wallet app connects to the network and makes the transfer.
- Step 4: After the transaction is confirmed on the network, the full node will recognize and know there's coins were sent to one deposit address.
- Step 5: Full node fires an event to notify API server about the new deposit.
- Step 6: API server call webserver to update the user's balance on the exchange.

#### User make a withdrawal
![Withdrawal flow](docs/Withdrawal.png)
- Step 1: User requests a withdrawal to webserver
- Step 2: Webserver tells API server that there's an user wants to withdraw
- Step 3: API server invokes method to send coins on the DASH node
- Step 4: After verifying authorization and some other information, DASH node makes the transfer
- Step 5: User receives his coins after the transaction is confirmed

### Security measures
- All servers are on the same LAN network, no access from outside is allowed.
- All sensitive APIs, RPCs are protected by authorization.
- All http requests are encrypted.
- Webserver implementation can resist all common security holes: SQL injection, CSRF, XSS, ...

### Fault tolerance
- API server and full node are both setup in multiple physical instances. It guarantees the system is not down if there's failure with one machine.

## Microservices
- TBD: Use multi-sig wallet to store coins/tokens.
- TBD: Check confirmations number before calling webhooks.
- TBD: Use a message broker to pass and handle event immediately.

### Dash
- Dash Deposit Crawler
- Dash Withdraw Verifier

### Ethereum Classic
- ETC Deposit Crawler
- ETC Withdraw Verifier

### Ethereum & ERC20 tokens
- Ethereum Deposit Crawler
- Ethereum Withdraw Verifier

### Webhook
- Webhook Notifier Service
