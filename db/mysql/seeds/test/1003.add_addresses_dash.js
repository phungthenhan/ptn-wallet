const _ = require('lodash');
const seeds = [
  {
    id              : 1001,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : '761c26dd35df310949d540e47d7db785051b6b0d6fafd038babdb6a166f0df63',
    wif             : 'cRYHtVJHqJNUtYP8pXbJKdEpNacFtQvf6s5V8XWMcERGUA9wLM2e',
    address         : 'yNi5kbW5prg5aKSS9hBsi5PkNWsiJERtJp',
    network         : 'testnet',
  },
  {
    id              : 1002,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : 'f0610c238ddcea74aa5b3826c92cc4ae9d04eb1e7be5817cdcb7faaaa6cf32a1',
    wif             : 'cVdy3reA7HRrZWQNgRQ9jg7VmdeufTLhzEtzVpjAhDvXBTHer3BL',
    address         : DASH_RECEIVE_ADDRESS,
    network         : 'testnet',
  },
  {
    id              : 1003,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : '5a398eac70bf63f4569bf847d46ee3e6ada43208515654cab39239e7293dd7fe',
    wif             : 'cQc5zg1TxEoNuGZJnxugQB4cQru1ahTuEywfQrcFCTfEz2nX4AXi',
    address         : 'yNXYAUPGssmCrXH37uTECseXazUycvUjxd',
    network         : 'testnet',
  },
  {
    id              : 1004,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : 'ff444c6c8912abefdd34b26a6f4381b25181f28c89438172497ddc7db82dc117',
    wif             : 'cW8uZ9iiDUFkmZFk6KHvFeL4NP2Fuk55oJsWPWEfdKvuvNHQNbVR',
    address         : 'yUZ3kBvELLLyK9T1QgcrnmS3ysgnYxJBKo',
    network         : 'testnet',
  },
  {
    id              : 1005,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : '6a11e6424949bd439057981b066d6faeaebfce0f6ed00631f475cc9a18c95718',
    wif             : 'cR8tSVJuXsSEBA92TBgmH2wmoA88Nq4BuQDWcvGdz8TcWXVhnFGz',
    address         : 'yWLdKE2SoyC7EqvyA3SMxypv41JEX4WoXr',
    network         : 'testnet',
  },
  {
    id              : 1006,
    wallet_id       : DASH_DEFAULT_WALLET_ID,
    private_key     : '500b36d5951885b41323a739ec4960eb15d4bc683e0d474032e89ceac56a453d',
    wif             : 'cQGJ98s5jektM25H4NG2vzMyVR9Lj1Ld8ncR8XjSEoZSyqLswDdN',
    address         : 'yXACvJPSreVVFFHm9eh4JCq7ouMEqa7qze',
    network         : 'testnet',
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('dash_address').insert(data),
    knex.raw('ALTER TABLE dash_address AUTO_INCREMENT=1000001'),
  )
};
