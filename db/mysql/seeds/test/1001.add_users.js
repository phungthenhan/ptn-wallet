var bcrypt = require('bcryptjs');

const seeds = [
  {
    id: 1, username: DEFAULT_USERNAME,
    email: 'sotatek.test@gmail.com',
    avatar_url: 'images/avatar.jpg',
    password: bcrypt.hashSync(DEFAULT_PASSWORD, bcrypt.genSaltSync(8)),
    full_name: 'Super User',
    api_key: process.env.TEST_API_KEY,
    api_secret: process.env.TEST_API_SECRET,
  }
];

exports.seed = function (knex, Promise) {
  return knex('user').insert(seeds);
};
