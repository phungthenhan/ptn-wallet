const _ = require('lodash');
const seeds = [
  {
    id        : DASH_DEFAULT_WALLET_ID,
    coin      : 'dash',
    label     : 'DASH seed wallet 1',
    user_id   : 1,
    delegate_address: DASH_SEND_ADDRESS,
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : 1002,
    coin      : 'dash',
    label     : 'DASH seed wallet 2',
    user_id   : 1,
    delegate_address: DASH_RECEIVE_ADDRESS,
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : 1003,
    coin      : 'dash',
    label     : 'DASH seed wallet 3',
    user_id   : 1,
    delegate_address: 'DASH_ADDRESS_TEST_003',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : ETH_DEFAULT_WALLET_ID,
    coin      : 'eth',
    label     : 'ETH seed wallet',
    user_id   : 1,
    delegate_address: ETH_SEND_ADDRESS,
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : 3001,
    coin      : 'etc',
    label     : 'ETC seed wallet',
    user_id   : 1,
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : 4001,
    coin      : 'qtum',
    label     : 'QTUM seed wallet',
    user_id   : 1,
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id        : BTC_DEFAULT_WALLET_ID,
    coin      : 'btc',
    label     : 'BTC seed wallet 1',
    user_id   : 1,
    delegate_address: BTC_SEND_ADDRESS,
    change_address: '32NFcoB96ANoPnqycLH9WUd94o7bwRyq66',
    change_private_key  : 'TBD',
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  const balance_data = _.map(seeds, (seed) => {
    const row  = {
      wallet_id: seed.id,
      coin: seed.coin,
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    };

    switch (seed.coin) {
      case 'eth':
        row.balance = '10000000000000000000';
        break;
      case 'btc':
        row.balance = '20000000000000000000';
        break;
      default:
        row.balance = '10000000000000000000';
    }

    return row;
  });

  return Promise.join(
    knex('wallet').insert(data),
    knex('wallet_balance').insert(balance_data),
    knex.raw('ALTER TABLE wallet AUTO_INCREMENT=1000001'),
  )
};
