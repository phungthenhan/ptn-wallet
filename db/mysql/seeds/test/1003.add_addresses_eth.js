const _ = require('lodash');
const seeds = [
  {
    id              : 1,
    wallet_id       : ETH_DEFAULT_WALLET_ID,
    private_key     : '0xc2d2bbf1f36aab47a6acf67f11aee2307e75d039a9ce6abfb11abbcc937e7521',
    address         : ETH_SEND_ADDRESS,
  },
  {
    id              : 1001,
    wallet_id       : ETH_DEFAULT_WALLET_ID,
    private_key     : '0xfad673a3ed7d096279ab33fcad89f911c36285764a368ed10b6c0d299b109742',
    address         : '0xdcF632bF2408b341E0D7782fa48D1A29cB5EB35c',
  },
  {
    id              : 1002,
    wallet_id       : ETH_DEFAULT_WALLET_ID,
    private_key     : '0xdf9553b655939e0b1106bc995b6dfbfeecb4072bcfc081b1fb782b95bd8780b2',
    address         : '0x7FaCdA3F9D69b2b6E02578995f98319409f83446',
  },
  {
    id              : 1003,
    wallet_id       : ETH_DEFAULT_WALLET_ID,
    private_key     : '0x0ca1fcc01033a27989fcaeb8dc08d6b37be60a7cd2ceb8f9db259ea677367882',
    address         : '0xE401117EAd2b4a8Abf4eB99950CC350ce1e0457A',
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('eth_address').insert(data),
    knex.raw('ALTER TABLE eth_address AUTO_INCREMENT=1000001'),
  )
};
