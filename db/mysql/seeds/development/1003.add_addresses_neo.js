const _ = require('lodash');
const seeds = [
  {
    id              : 1,
    wallet_id       : NEO_DEFAULT_WALLET_ID,
    private_key     : '80c81f6c74077ece99674f1ef8ac91a59baba8e80175c6707517eb6ecffb85b2',
    wif             : 'L1Y3c76FYo2UDYkhvsiYgwVv2MFHAJhYRWC8tokWmj3zmBjpLewF',
    address         : 'AY27T4DehowuzxjidL7iL3xjAe5oPVQ5Kx',
  }
];

const hot_wallets = [
  {
    private_key     : "L5hm5ajW8EaAwBMZVHNF1T43Rv7GhPzG34foAnvNk5m3BnwXRxMg",
    address         : "ATMMPS2FVu9mYAZD2Pghqb6GKonegWmgVY",
    balance         : 0,
    low_threshold   : 0,
  },
];


exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('neo_address').truncate(),
    knex('neo_address').insert(data),
    knex.raw('ALTER TABLE neo_address AUTO_INCREMENT=1000001'),
  )
};
