const _ = require('lodash');
const BTC_DEFAULT_WALLET_ID = 5001;
const seeds = [
  {
    id              : 1001,
    wallet_id       : BTC_DEFAULT_WALLET_ID,
    private_key     : '3031ddc8f57b06312139c470194aef1b36a9f0002bc9bc3d6c2150391eb00663',
    wif             : 'cPCPLorCRiTR1ir17BcPmK2sdBFfBA2YdW6mGp3tJeNS1sRNrMfF',
    address         : 'mmJzRfnQHyxPhfgbK2q8pC8dZuqMgHUUtN',
    network         : 'testnet',
  },
  {
    id              : 1002,
    wallet_id       : BTC_DEFAULT_WALLET_ID,
    private_key     : '0a396ca30951ac468f381f4ad8406cf923c02271071544f145c9f5aeafc3817b',
    wif             : 'cMvaPAUoTJgacwYw3yuRFFjVeZsL9AFkyTw8t9W53TGR8JmqV38c',
    address         : 'mrpTHkE9CsdmNRRQ9JPKH8NJTJAbgi3Jc4',
    network         : 'testnet',
  },
  {
    id              : 1003,
    wallet_id       : BTC_DEFAULT_WALLET_ID,
    private_key     : 'f0a1ef73adcce027074e26b1474cbb0e4da59d496f8764858b90555a13baee5a',
    wif             : 'cVeTdLDK7QSi569mD4gBSxqS6129ELj5UCAxQuDzNGwnNC5SKkxC',
    address         : 'mr7u9PhJBbYFiBsKQ2pHD1SdGRT6xUBXeg',
    network         : 'testnet',
  },
  {
    id              : 1004,
    wallet_id       : BTC_DEFAULT_WALLET_ID,
    private_key     : '77300b86b335fd3b2420f3f8b69fc1406033e89b1604f225f59f45dc7ac5c377',
    wif             : 'cRaPPoxoPoQvVdZbyUh4RdoCerig6oj6YmJ6fjYyfnUDqnug9Hg6',
    address         : 'mg4qCZ2x1x7MQ7GRC1C1JURQeKjwT6dMz1',
    network         : 'testnet',
  },
];

const btc_hot_wallets = [
  {
    private_key     : "3e51915a4482ce275f613343e883314f3a5b915ffa21ecbbe68fe33d614ea8fc",
    address         : "min8wk3oLmoY4AcHr1UMEoKtaToNpVtVMd",
    balance         : 0,
    low_threshold   : 0,
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('btc_address').truncate(),
    knex('btc_address').insert(data),
    knex('btc_hot_wallet').truncate(),
    knex('btc_hot_wallet').insert(btc_hot_wallets),
    knex.raw('ALTER TABLE btc_address AUTO_INCREMENT=1000001'),
  )
};
