global.DASH_DEFAULT_WALLET_ID = 1001;
global.ETH_DEFAULT_WALLET_ID = 2001;
global.ETC_DEFAULT_WALLET_ID = 3001;
global.QTUM_DEFAULT_WALLET_ID = 4001;
global.USDT_DEFAULT_WALLET_ID = 5001;
global.NEO_DEFAULT_WALLET_ID = 6001;
global.BTC_DEFAULT_WALLET_ID = 7001;
const _ = require('lodash');
const seeds = [
  {
    id                  : DASH_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'dash',
    label               : 'DASH seed wallet',
    delegate_address    : 'yNi5kbW5prg5aKSS9hBsi5PkNWsiJERtJp',
    change_address      : 'ydfTHU6jguMkbhgABmvmb1pnrzCKxGZPXA',
    change_private_key  : '87cf8e850f60d7e51958c64e0e2b6ca85ad867b1024d1ed08a56fd04761612dc',
  },
  {
    id                  : ETH_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'eth',
    label               : 'ETH seed wallet',
    delegate_address    : '0xA0631a5beFf3509A7dFDfD09caDcC836bb09B483',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id                  : ETC_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'etc',
    label               : 'ETC seed wallet',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id                  : QTUM_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'qtum',
    label               : 'QTUM seed wallet',
    delegate_address    : 'qN1sQD1EdP7J24pvgFXyiVRS5952TTWHCk',
    change_address      : 'qSKXKQHNpNdViu22U3idhE3VnhSjrR6Qpz',
    change_private_key  : '832bfeb83353b9bb032f5aa7369f9742470ce34a1b8c6431ee7c4d7efb6f91cb',
  },
  {
    id                  : USDT_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'usdt',
    label               : 'USDT seed wallet',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id                  : NEO_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'neo',
    label               : 'NEO seed wallet',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
  {
    id                  : BTC_DEFAULT_WALLET_ID,
    user_id             : 1,
    coin                : 'btc',
    label               : 'BTC seed wallet',
    change_address      : 'TBD',
    change_private_key  : 'TBD',
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  const balance_data = _.map(seeds, (seed) => {
    return {
      wallet_id: seed.id,
      coin: seed.coin,
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    };
  });

  return Promise.join(
    knex('wallet').truncate(),
    knex('wallet').insert(data),
    knex('wallet_balance').truncate(),
    knex('wallet_balance').insert(balance_data),
    knex.raw('ALTER TABLE wallet AUTO_INCREMENT=1000001'),
  )
};
