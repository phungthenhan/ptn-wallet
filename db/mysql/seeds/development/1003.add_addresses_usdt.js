const _ = require('lodash');
const seeds = [
  {
    id              : 1001,
    wallet_id       : USDT_DEFAULT_WALLET_ID,
    private_key     : 'aa34e21f6bcb569a101e47724217bd6f676e79a526be8f2b82c6de313674591a',
    address         : 'mhJEnsPjbS5ENc3KZD52v9YDss8fgD5gd8',
  },
];

const hot_wallets = [
  {
    private_key     : "aa34e21f6bcb569a101e47724217bd6f676e79a526be8f2b82c6de313674591a",
    address         : "mhJEnsPjbS5ENc3KZD52v9YDss8fgD5gd8",
    balance         : 0,
    low_threshold   : 0,
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('usdt_address').truncate(),
    knex('usdt_address').insert(data),
    knex.raw('ALTER TABLE usdt_address AUTO_INCREMENT=1000001'),
  )
};
