const _ = require('lodash');
const seeds = [
  {
    id              : 1001,
    wallet_id       : QTUM_DEFAULT_WALLET_ID,
    private_key     : 'ac7e05cd072c6a4d3327e12cf3695d17d56ff295d5c7e5eb6ae81404145ad8bf',
    wif             : 'cTN1BzdKFCCAEYh4xbw9S62hdDn2k6dg3yNenuk98zG59vjYEbwW',
    address         : 'qN1sQD1EdP7J24pvgFXyiVRS5952TTWHCk',
    network         : 'testnet',
  }
];

const hot_wallets = [
  {
    private_key     : "cPxTw46tP1WopaNZoMviESMhbxRzfEDUy9egSL9Qvwt5tE8FrcnB",
    address         : "qe73MdBX8ANRnKG4CJ4Ckkfz6GszC2udER",
    balance         : 0,
    low_threshold   : 0,
  },
  {
    private_key     : "cNQZMy8ypcMTYdPsYz7nEGvf2XhUx4Lu89VL61fjrb5swEvBzw1U",
    address         : "qML8kCa9wKXQBrXGjZFQoeA9qXoAGfjBr7",
    balance         : 0,
    low_threshold   : 0,
  }
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('qtum_address').truncate(),
    knex('qtum_address').insert(data),
    knex('qtum_hot_wallet').truncate(),
    knex('qtum_hot_wallet').insert(hot_wallets),
    knex.raw('ALTER TABLE qtum_address AUTO_INCREMENT=1000001'),
  )
};
