var bcrypt = require('bcryptjs');

const seeds = [
  {
    id: 1, username: 'admin',
    email: 'sotatek.test@gmail.com',
    avatar_url: 'images/avatar.jpg',
    password: bcrypt.hashSync('1', bcrypt.genSaltSync(8)),
    full_name: 'Super User',
    api_key: process.env.TEST_API_KEY,
    api_secret: process.env.TEST_API_SECRET,
  }
];

exports.seed = function (knex, Promise) {
  return Promise.join(
    knex('user').truncate(),
    knex('user').insert(seeds)
  )
};
