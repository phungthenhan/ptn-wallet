const _ = require('lodash');
const seeds = [
  {
    label             : 'Test transfer wallet webhook',
    wallet_id         : 1001,
    coin              : 'dash',
    type              : 'transfer',
    num_confirmations : 0,
    url               : 'http://httpbin.org/post'
  },
  {
    label             : 'Test transfer wallet webhook 2',
    wallet_id         : 1001,
    coin              : 'dash',
    type              : 'transfer',
    num_confirmations : 10,
    url               : 'http://httpbin.org/post'
  },
  {
    label             : 'Test ETH deposit wallet webhook',
    wallet_id         : ETH_DEFAULT_WALLET_ID,
    coin              : 'eth',
    type              : 'transfer',
    num_confirmations : 0,
    url               : 'http://httpbin.org/post'
  },
  {
    label             : 'Test ETH deposit wallet webhook 2',
    wallet_id         : ETH_DEFAULT_WALLET_ID,
    coin              : 'eth',
    type              : 'transfer',
    num_confirmations : 0,
    url               : 'http://httpbin.org/post'
  },
];

exports.seed = function (knex, Promise) {
  const data = _.map(seeds, (seed) => {
    return _.assign(seed, {
      created_at: Date.now(),
      updated_at: Date.now(),
      created_by: 1,
      updated_by: 1,
    });
  });

  return Promise.join(
    knex('wallet_webhook').truncate(),
    knex('wallet_webhook').insert(data),
    knex.raw('ALTER TABLE wallet_webhook AUTO_INCREMENT=1000001'),
  )
};
