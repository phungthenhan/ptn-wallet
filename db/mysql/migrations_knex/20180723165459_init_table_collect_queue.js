
exports.up = (knex, Promise) => {
  return knex.schema.createTable('collect_queue', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.integer('wallet_id').notNullable().index();
    t.string('address', 100).notNullable().index();
    t.string('coin', 10).notNullable().index();
    t.tinyint('is_fee_seeded').default(0);
    t.bigint('tried_at');
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
    t.unique(['address', 'coin']);
  });
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('collect_queue');
}
