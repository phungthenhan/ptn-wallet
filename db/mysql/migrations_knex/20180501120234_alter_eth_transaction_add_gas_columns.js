
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('eth_transaction', 'gas_price').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('eth_transaction', (t) => {
      t.decimal('gas_used', 32, 0).after('value');
      t.decimal('gas_limit', 32, 0).after('value');
      t.decimal('gas_price', 32, 0).after('value');
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
