exports.up = (knex, Promise) => {

  return knex.schema.hasTable('collect_transaction').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('collect_transaction', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('coin',10).notNullable().index();
      t.integer('wallet_id').notNullable().index();
      t.string('txid',100).notNullable().unique().index();
      t.bigint('block_time');
      t.integer('block_number').default(0);
      t.string('block_hash', 100);
      t.string('from_address',100).notNullable();
      t.string('to_address',100).notNullable();
      t.decimal('fee',32,0).notNullable();
      t.decimal('amount',32,0).notNullable();
      t.string('status', 20).default('sent');
      t.bigint('checked_at');
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}