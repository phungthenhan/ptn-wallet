
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('usdt_deposit', (t) =>{
      t.bigIncrements('id').primary().unsigned();
      t.integer('block_number').notNullable().index();
      t.string('block_hash', 100).notNullable().index();
      t.bigint('block_timestamp').notNullable().index();
      t.string('txid', 100).notNullable();
      t.integer('wallet_id').notNullable().index();
      t.string('address', 40).notNullable().index();
      t.decimal('amount', 32, 0).notNullable();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
      t.unique(['txid', 'address']);
    })
  ])
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('usdt_deposit');
};
