
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('dash_address', 'wif').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('dash_address', (t) => {
      t.string('wif', 100).notNullable().after('private_key');
      t.string('network', 10).notNullable().after('address');
    });
  });

};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
