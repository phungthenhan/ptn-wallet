
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('wallet', 'delegate_address').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('wallet', (t) => {
      t.string('delegate_address', 100).after('coin').notNullable().default('SET_VALID_ADDRESS_HERE');
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
