
exports.up = (knex, Promise) => {
  return knex.schema.table('dash_withdrawal', (t) => {
    t.bigint('tried_at').index().default(0).after('tx_raw');
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
