
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('network_status', (t) => {
      t.string('key').notNullable().index();
      t.string('type').notNullable();
      t.string('value').notNullable();
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('network_status');
}
