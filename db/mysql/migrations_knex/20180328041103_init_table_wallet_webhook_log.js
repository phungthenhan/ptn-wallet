
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('wallet_webhook_log', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.integer('webhook_progress_id').notNullable();
      t.string('msg', 1000).notNullable();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('wallet_webhook_log');
}
