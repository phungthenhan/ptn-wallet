
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('wallet_webhook_progress', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.integer('webhook_id').notNullable();
      t.string('txid', 100).notNullable();
      t.tinyint('is_processed').notNullable().index().default(0);
      t.integer('retry_count').notNullable().default(0);
      t.bigint('last_try').notNullable();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
      t.unique(['webhook_id', 'txid']);
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('wallet_webhook_progress');
}
