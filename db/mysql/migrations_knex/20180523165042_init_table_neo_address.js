exports.up = function(knex, Promise) {
  return knex.schema.hasTable('neo_address').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }
    
    return knex.schema.createTable('neo_address', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.integer('wallet_id').notNullable().index();
      t.string('private_key', 100).notNullable().index();
      t.string('address', 100).notNullable().unique();
      t.string('wif', 100);
      t.string('publickey', 100);
      t.string('scripthash', 100);
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
      t.bigint('check_balance_at');
      t.decimal('balance',32,0);
    });
  });
};

exports.down = function(knex, Promise) {
  return Promise.all([]);
};
