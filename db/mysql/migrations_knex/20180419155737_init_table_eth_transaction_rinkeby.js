
exports.up = (knex, Promise) => {
  return knex.schema.hasTable('eth_transaction_rinkeby').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('eth_transaction_rinkeby', (t) => {
      t.string('coin', 100).notNullable();
      t.string('contract_address', 100).notNullable().index();
      t.string('txid', 100).notNullable().index();
      t.integer('nonce').notNullable();
      t.string('block_hash', 100).notNullable().index();
      t.integer('block_number').notNullable().index();
      t.integer('block_timestamp').notNullable().index();
      t.integer('transaction_index').notNullable();
      t.string('from', 100).notNullable().index();
      t.string('to', 100).notNullable().index();
      t.decimal('value', 32, 0).notNullable();
      t.string('status', 10);
      t.string('log_id').index();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');

      t.unique(['txid', 'log_id']);
    });
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}
