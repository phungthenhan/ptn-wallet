
exports.up = (knex, Promise) => {
  return knex.schema.hasTable('qtum_hot_wallet').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('qtum_hot_wallet', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('private_key', 100);
      t.string('address', 100).notNullable().unique();
      t.decimal('balance', 32, 0).unsigned().notNullable().default(0);
      t.decimal('low_threshold', 32, 0).unsigned().notNullable().default(0);
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    });
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}