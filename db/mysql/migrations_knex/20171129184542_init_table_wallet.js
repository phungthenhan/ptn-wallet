
exports.up = (knex, Promise) => {
  return knex.schema.createTable('wallet', (t) => {
    t.increments('id').primary().unsigned();
    t.integer('user_id').notNullable().index();
    t.string('label', 255).notNullable();
    t.string('coin', 10).notNullable();
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
  });
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('wallet');
}
