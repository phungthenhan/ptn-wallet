
exports.up = (knex, Promise) => {
  return knex.schema.createTable('address_balance', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.integer('wallet_id').notNullable().index();
    t.string('address', 100).notNullable().index();
    t.string('coin', 10).notNullable().index();
    t.decimal('balance', 32, 0).notNullable();
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
    t.unique(['address', 'coin']);
  });
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('address_balance');
}
