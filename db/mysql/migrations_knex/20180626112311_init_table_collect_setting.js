exports.up = (knex, Promise) => {

  return knex.schema.hasTable('collect_setting').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('collect_setting', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('coin',10).notNullable().index();
      t.decimal('minimum_amount', 32,0).notNullable();
      t.decimal('seeding_fee',32,0).notNullable();
      t.decimal('gas_limit',32,0).notNullable();
      t.decimal('gas_price', 32,0).notNullable();
      t.tinyint('auto_collector').notNullable().default(0);
      t.integer('daily_at').default(0);
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}