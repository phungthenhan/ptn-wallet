
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('erc20_withdrawal', 'tx_raw').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('erc20_withdrawal', (t) => {
      t.string('tx_raw', 2000).notNullable().after('tx_status');
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
