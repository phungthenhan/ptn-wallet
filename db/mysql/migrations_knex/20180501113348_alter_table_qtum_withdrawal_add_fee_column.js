
exports.up = (knex, Promise) => {
  return knex.schema.table('qtum_withdrawal', (t) => {
    t.decimal('fee', 32, 0).default(0).after('amount');
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
