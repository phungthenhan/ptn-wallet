
exports.up = (knex, Promise) => {
  return knex.schema.table('wallet_webhook', (t) => {
    t.integer('num_confirmations').notNullable().default(0).after('type');
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
