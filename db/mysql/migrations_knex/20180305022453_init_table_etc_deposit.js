
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('etc_deposit', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('txid', 100).notNullable().unique();
      t.integer('wallet_id').notNullable().index();
      t.string('address', 255).notNullable().index();
      t.decimal('amount', 32, 0).notNullable();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('etc_deposit');
}
