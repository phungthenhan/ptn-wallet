
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('usdt_address', (t) =>{
      t.bigIncrements('id').primary().unsigned();
      t.integer('wallet_id').notNullable().index();
      t.string('private_key', 100);
      t.string('address', 100).notNullable().unique();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  ])
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('usdt_address');
};
