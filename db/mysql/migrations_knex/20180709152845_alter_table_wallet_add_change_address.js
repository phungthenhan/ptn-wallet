
exports.up = function(knex, Promise) {
  return knex.schema.hasColumn('wallet', 'change_address').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('wallet', (t) => {
      t.string('change_private_key', 100).notNullable().after('delegate_address').default("");
      t.string('change_address', 100).notNullable().after('delegate_address').default("");
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
