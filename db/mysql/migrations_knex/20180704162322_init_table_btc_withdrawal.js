
exports.up = (knex, Promise) => {
  return knex.schema.createTable('btc_withdrawal', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.integer('block_number').index();
    t.string('block_hash', 100).index();
    t.integer('block_timestamp').index();
    t.string('txid', 100).notNullable().unique();
    t.integer('wallet_id').notNullable().index();
    t.string('from_address', 100).notNullable().index();
    t.string('to_address', 100).notNullable().index();
    t.decimal('amount', 32, 0).notNullable();
    t.decimal('fee', 32, 0);
    t.string('status', 20).notNullable();
    t.string('tx_status', 10);
    t.string('tx_raw', 2000).notNullable();
    t.bigint('tried_at').index().default(0);
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
  });
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('btc_withdrawal');
}
