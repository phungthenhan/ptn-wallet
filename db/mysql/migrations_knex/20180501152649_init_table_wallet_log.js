
exports.up = function (knex, Promise) {
  return knex.schema.createTable('wallet_log', function (t) {
    t.bigIncrements('id').primary().unsigned();
    t.integer('wallet_id').notNullable().index();
    t.string('coin', 10).notNullable();
    t.string('event', 30).notNullable();
    t.decimal('balance_change', 32, 0).notNullable();
    t.bigint('ref_id').notNullable().index();
    t.unique(['ref_id', 'wallet_id', 'coin', 'event']);

    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
  });
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('wallet_log');
}
