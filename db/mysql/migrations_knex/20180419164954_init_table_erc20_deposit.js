
exports.up = (knex, Promise) => {
  return knex.schema.createTable('erc20_deposit', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.integer('block_number').notNullable().index();
    t.string('block_hash', 100).notNullable().index();
    t.bigint('block_timestamp').notNullable().index();
    t.integer('tx_index').notNullable();
    t.string('txid', 100).notNullable().unique();
    t.string('token_address', 100).notNullable().index();
    t.string('token_symbol', 7).notNullable().index();
    t.integer('wallet_id').notNullable().index();
    t.string('from_address', 100).notNullable().index();
    t.string('to_address', 100).notNullable().index();
    t.decimal('amount', 32, 0).notNullable();
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
  });
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('erc20_deposit');
}
