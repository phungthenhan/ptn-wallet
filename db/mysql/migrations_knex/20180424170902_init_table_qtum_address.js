
exports.up = (knex, Promise) => {
  return knex.schema.hasTable('qtum_address').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('qtum_address', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.integer('wallet_id').notNullable().index();
      t.string('private_key', 100).notNullable();
      t.string('wif', 100).notNullable();
      t.string('address', 100).notNullable().unique();
      t.string('network', 10).notNullable();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    });
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}
