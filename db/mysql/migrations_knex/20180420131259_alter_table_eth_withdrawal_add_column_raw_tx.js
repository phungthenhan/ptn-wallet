
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('eth_withdrawal', 'tx_raw').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('eth_withdrawal', (t) => {
      t.string('tx_raw', 5000).notNullable().after('tx_status');
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
