
exports.up = (knex, Promise) => {
  return knex.schema.hasTable('eth_block').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('eth_block', (t) => {
      t.integer('number').primary().unsigned();
      t.string('hash', 100).notNullable().unique();
      t.string('nonce', 100).notNullable().index();
      t.bigint('size').notNullable();
      t.bigint('timestamp').notNullable().unsigned().index();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    });
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}
