
exports.up = function(knex, Promise) {
  return knex.schema.hasColumn('eth_address', 'balance').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('eth_address', (t) => {
      t.decimal('balance', 32,0).after('address');
      t.bigint('balance_updated_at').after('address').default(0);
      t.bigint('collect_enqueued_at').after('address').default(0);
    });
  });
};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
