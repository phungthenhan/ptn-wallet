
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('dash_withdraw', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.integer('block_number').index();
      t.string('block_hash', 100).index();
      t.integer('block_timestamp').index();
      t.string('txid', 100).notNullable().unique();
      t.integer('wallet_id').notNullable().index();
      t.string('address', 100).notNullable().index();
      t.decimal('amount', 32, 0).notNullable();
      t.string('status', 20).notNullable();
      t.string('tx_status', 10);
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('dash_withdraw');
}
