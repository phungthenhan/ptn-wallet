
exports.up = (knex, Promise) => {
  return knex.schema.hasTable('tx_change_log').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.createTable('tx_change_log', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('coin', 100).notNullable();
      t.string('tx_old', 100).notNullable().unique().index();
      t.string('tx_new', 100);
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    });
  });
}

exports.down = (knex, Promise) => {
  return Promise.all([]);
}