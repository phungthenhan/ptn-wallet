
exports.up = (knex, Promise) => {
  return knex.schema.hasColumn('wallet_webhook_progress', 'data').then((exists) => {
    if (exists) {
      return Promise.all([]);
    }

    return knex.schema.table('wallet_webhook_progress', (t) => {
      t.string('data', 9999);
    });
  });

};

exports.down = (knex, Promise) => {
  return Promise.all([]);
};
