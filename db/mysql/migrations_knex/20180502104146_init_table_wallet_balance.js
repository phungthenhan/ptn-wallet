
exports.up = (knex, Promise) => {
  return knex.schema.createTable('wallet_balance', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.integer('wallet_id').notNullable().index();
    t.string('coin', 20).notNullable();
    t.decimal('balance', 32, 0).unsigned().notNullable().default(0);
    t.decimal('withdrawal_pending', 32, 0).unsigned().notNullable().default(0);
    t.decimal('withdrawal_total', 32, 0).unsigned().notNullable().default(0);
    t.decimal('deposit_total', 32, 0).unsigned().notNullable().default(0);
    t.unique(['wallet_id', 'coin']);
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
  });
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('wallet_balance');
}
