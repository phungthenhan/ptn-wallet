
exports.up = (knex, Promise) => {
  return Promise.all([
    knex.schema.createTable('wallet_webhook', (t) => {
      t.bigIncrements('id').primary().unsigned();
      t.string('label', 255).notNullable();
      t.integer('wallet_id').notNullable().index();
      t.string('coin', 10).notNullable();
      t.string('type', 50).notNullable();
      t.string('url', 255).notNullable().index();
      t.bigint('created_at');
      t.bigint('updated_at');
      t.integer('created_by');
      t.integer('updated_by');
    })
  ]);
}

exports.down = (knex, Promise) => {
  return knex.schema
    .dropTableIfExists('wallet_webhook');
}
