
exports.up = function(knex, Promise) {
  return knex.schema.createTable('latest_block', (t) => {
    t.bigIncrements('id').primary().unsigned();
    t.string('coin', 10).notNullable().index();
    t.string('type', 50).notNullable().index();
    t.integer('block_number').notNullable().index();
    t.bigint('created_at');
    t.bigint('updated_at');
    t.integer('created_by');
    t.integer('updated_by');
    t.unique(['coin', 'type']);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('latest_block');
};
