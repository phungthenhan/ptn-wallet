const _           = require('lodash');
const async       = require('async');
const path        = require('path');
const mongoose    = require('mongoose');
const FileUtils   = require('sota-core').load('util/FileUtils');
const logger      = require('sota-core').getLogger('MongoDBSeeds::clearOldData');

module.exports = function () {
  const modelDir = path.resolve(__dirname, '../../../app/models');
  const modelFiles = FileUtils.listFiles(modelDir, {
    regex: /.js$/i,
    isRecursive: false
  });

  const tasks = [];
  _.forEach(modelFiles, (modelFile) => {
    const model = require(modelFile);
    if (model._mongooseSchema) {
      try {
        tasks.push((next) => {
          mongoose.model(model.tableName).remove({}, next);
        });
      } catch (e) {
        logger.error(e.toString());
      }
    }
  });

  return new Promise((resolve, reject) => {
    async.parallel(tasks, (err) => {
      if (err) {
        return reject(err);
      }

      return resolve(null, true);
    });
  });
}
