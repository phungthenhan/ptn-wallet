const _           = require('lodash');
const async       = require('async');
const path        = require('path');
const mongoose    = require('mongoose');
const FileUtils   = require('sota-core').load('util/FileUtils');
const logger      = require('sota-core').getLogger('MongoDBSeeds::insertNewData');

module.exports = function () {
  const seedDir = path.resolve(__dirname, `./${process.env.NODE_ENV}`);
  const seedFiles = FileUtils.listFiles(seedDir, {
    regex: /.js$/i,
    isRecursive: true
  });

  const tasks = [];
  _.each(seedFiles, (filePath) => {
    const tableName = path.basename(filePath, `.js`);

      const seeds = require(filePath);
      tasks.push(function (next) {
        try {
          mongoose.model(tableName).insertMany(seeds, next);
        } catch (e) {
          logger.error(e.toString());
          next(null, null);
        }
      });
  });

  return new Promise((resolve, reject) => {
    async.parallel(tasks, (err) => {
      if (err) {
        return reject(err);
      }

      return resolve(null, true);
    });
  });
}
