const _           = require('lodash');
const async       = require('async');
const path        = require('path');
const mongoose    = require('mongoose');
const FileUtils   = require('sota-core').load('util/FileUtils');

module.exports = function () {
  initMongoose();

  const modelDir = path.resolve(__dirname, '../../app/models');
  const modelFiles = FileUtils.listFiles(modelDir, {
    regex: /.js$/i,
    isRecursive: false
  });

  _.forEach(modelFiles, (modelFile) => {
    const model = require(modelFile);
    if (model._mongooseSchema) {
      const Schema = new mongoose.Schema(model._mongooseSchema);
      mongoose.model(model.tableName, Schema);
    }
  });

  return new Promise((resolve, reject) => {
    mongoose.connection.on('open', (err) => {
      if (err) {
        reject(new Error(`MongoAdapter connection on open with error: ${err}`));
      }

      resolve(null, true);
    });
  });
}

function initMongoose() {
  const e = process.env;
  if (!e.MONGODB_HOST || !e.MONGODB_DBNAME) {
    return;
  }

  let url = 'mongodb://';
  if (e.MONGODB_USERNAME && e.MONGODB_PASSWORD) {
    url += `${e.MONGODB_USERNAME}:${e.MONGODB_PASSWORD}@`;
  }
  url += e.MONGODB_HOST;
  if (process.env.MONGODB_PORT) {
    url += `:${e.MONGODB_PORT}`;
  }
  url += `/${e.MONGODB_DBNAME}`;

  mongoose.connect(url);
  mongoose.Promise = global.Promise;
  mongoose.connection.on('error', (err) => {
    throw new Error(`MongoAdapter connection on error: ${err}`);
  });
};
