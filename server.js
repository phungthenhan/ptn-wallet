require('dotenv').config();
const path        = require('path');
const SotaCore    = require('sota-core');

// Require in order to prevent original bitcore lib throwing error
const bitcore     = require('bitcore-lib');
const dashcore    = require('@dashevo/dashcore-lib');
const qtumcore    = require('qtumcore-lib');

const app = SotaCore.createServer({
  rootDir: path.resolve('.')
});
app.start();

module.exports = app;
module.exports.SotaCore = SotaCore;
